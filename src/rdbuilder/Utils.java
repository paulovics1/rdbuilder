package rdbuilder;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import rdbuilder.control.TaskExecutionPopupController;
import rdbuilder.dbconnector.DBConnector;
import rdbuilder.dbobject.DBObject;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

/**
 *  A static class containing helper methods.
 */
@SuppressWarnings("WeakerAccess")
public final class Utils {
    public static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    public static final int DEFAULT_TAB_SIZE = 4;

    /**
     * Used by {@link #forEachRow(ResultSet, ResultSetRowOperator)}
     */
    public interface ResultSetRowOperator {
        void execute(ResultSet resultSet) throws SQLException;
    }

    private Utils() {}

    public static String colorToHex(Color color){
        return String.format( "#%02X%02X%02X",
                (int)(color.getRed() * 255),
                (int)(color.getGreen() * 255),
                (int)(color.getBlue() * 255));
    }

    /**
     * Tries to establish connection with the database system.
     * @param dbConnector database connector which specifies the connection
     * @param owner parent window of the potential alert.
     * @return success of connection
     */
    public static boolean connectDBConnectorTask(DBConnector dbConnector, Window owner) {
        try {
            DBConnector result = TaskExecutionPopupController.newCancellableTaskExecution(() -> {
                        dbConnector.connect();
                        return dbConnector;
                    },
                    "Connecting to the database server",
                    owner);
            return result != null;
        } catch (Exception exception) {
            Utils.showError(exception, "Database connection exception", "An error has occured during establishing the connection.");
        }
        return false;
    }

    public static void createFileIfNotExists(String path) throws IOException {
        File file = new File(path);
        //noinspection ResultOfMethodCallIgnored
        file.createNewFile();
    }

    /**
     * Calls the action on every remaining row of this result set.
     */
    public static void forEachRow(ResultSet resultSet, ResultSetRowOperator action) throws SQLException {
        while (resultSet.next()) {
            action.execute(resultSet);
        }
    }

    public static String getEnglishPosition(int pos){
        if (pos == 1)
            return "1st";
        if (pos == 2)
            return "2nd";
        if (pos == 3)
            return "3rd";
        return pos+"th";
    }

    public static String getIndentationString(int depth, int tabSize) {
        char[] indentArray = new char[depth * tabSize];
        Arrays.fill(indentArray, ' ');
        return new String(indentArray);
    }

    public static Collection<DBObject> getSortedDBObjectsCollection(Collection<? extends DBObject> collection){
        final ArrayList<DBObject> sorted = new ArrayList<>(collection);
        sorted.sort(DBObject.getComparator());
        return sorted;
    }

    /**
     * Loads an XML file returning it's content except the header line.
     */
    public static String loadAsXML(File file) throws IOException {
        Scanner scanner = new Scanner(file);
        scanner.nextLine();
        scanner.useDelimiter("\\Z");
        String result = scanner.next();
        scanner.close();
        return result;
    }

    /**
     * Calls and waits for the execution of {@link Platform#runLater(Runnable)}.
     */
    public static void platformRunAndWait(Runnable runnable) {
        if (Platform.isFxApplicationThread()) {
            runnable.run();
            return;
        }

        final CountDownLatch doneLatch = new CountDownLatch(1);
        Platform.runLater(() -> {
            try {
                runnable.run();
            } finally {
                doneLatch.countDown();
            }
        });

        try {
            doneLatch.await();
        } catch (InterruptedException e) {
            // ignore exception
        }
    }

    public static String readFileContent(File file) throws IOException {
        final BufferedReader bufferedFileReader = new BufferedReader(new FileReader(file));
        final StringBuilder fileContentBuilder = new StringBuilder();
        while (bufferedFileReader.ready()) {
            fileContentBuilder.append(bufferedFileReader.readLine());
        }

        bufferedFileReader.close();
        return fileContentBuilder.toString();
    }

    /**
     * Saves a string to a file adding an XML header.
     */
    public static void saveAsXML(String content, File file) throws IOException {
        Utils.createFileIfNotExists(file.getPath());
        FileWriter fileWriter = new FileWriter(file, false);
        BufferedWriter writer = new BufferedWriter(fileWriter);
        writer.write(XML_HEADER);
        writer.newLine();
        writer.write(content);
        writer.close();
        fileWriter.close();
    }

    /**
     * Shows a custom error dialog.
     * @param exception error that occured
     * @param title title of the dialog
     * @param header header of the dialog
     */
    public static void showError(Exception exception, String title, String header) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.initStyle(StageStyle.UTILITY);
        alert.setHeaderText(header);
        alert.setContentText(exception.getLocalizedMessage());
        exception.printStackTrace();

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        String exceptionText = sw.toString();

        Pane pane = new GridPane();
        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);

        pane.setMaxWidth(Double.MAX_VALUE);
        pane.setMaxHeight(Double.MAX_VALUE);
        GridPane.setHgrow(textArea, Priority.ALWAYS);
        GridPane.setVgrow(textArea, Priority.ALWAYS);

        pane.getChildren().add(textArea);
        alert.getDialogPane().setExpandableContent(pane);
        alert.showAndWait();
    }
}
