package rdbuilder;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import rdbuilder.control.ControllerMediator;
import rdbuilder.control.RootController;
import rdbuilder.dbconnector.DBConnectorFactory;
import rdbuilder.dbconnector.SQLGenerator;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

@SuppressWarnings("ConstantConditions")
public class Main extends Application {
    private static final double minHeight = 96;
    private static final double minWidth = 320;
    private final static String DB_CONNECTORS_JAVA_PATH = String.format("src%1$srdbuilder%1$sdbconnector%1$sexternal%1$s", File.separator);
    private final static String CLASSES_ROOT_PATH = String.format("classes%s", File.separator);
    private final static String DB_CONNECTORS_CLASSES_PATH = String.format("%2$srdbuilder%1$sdbconnector%1$sexternal%1$s", File.separator, CLASSES_ROOT_PATH);
    private final static String JDBC_DRIVERS_PATH = String.format("lib%1$sjdbc_drivers%1$s", File.separator);
    public static List<DBConnectorFactory> dbConnectorFactories;
    public static TreeSet<SQLGenerator> sqlGenerators;
    private static URLClassLoader loader;

    @Override
    public void start(Stage primaryStage) throws Exception {
        clearExternalClasses();
        loadJDBCDrivers();
        compileAndLoadDBConnectors();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view/main.fxml"));
        RootController.primaryStage = primaryStage;
        Parent root = fxmlLoader.load();
        Scene mainScene = new Scene(root);
        primaryStage.setTitle("Relational Database Builder");
        primaryStage.setScene(mainScene);
        primaryStage.show();

        final RootController rootController = ControllerMediator.getRootController();
        primaryStage.setMinHeight(primaryStage.getHeight() - mainScene.getHeight() + minHeight);
        primaryStage.setMinWidth(primaryStage.getWidth() - mainScene.getWidth() + minWidth);
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Clears the dynamically compiled DBConnector classpath. Creates necessary
     * directories if they don't exist.
     */
    private void clearExternalClasses() {
        File dbConnectorsClassesDirectory = new File(DB_CONNECTORS_CLASSES_PATH);
        dbConnectorsClassesDirectory.mkdirs();
        for (File file : dbConnectorsClassesDirectory.listFiles()) {
            file.delete();
        }
        dbConnectorFactories = new ArrayList<>();
        sqlGenerators = new TreeSet<>((o1, o2) -> o1.getSQLSyntaxName().toLowerCase().compareTo(o2.getSQLSyntaxName().toLowerCase()));
    }

    /**
     * Loads classes from java files in a directory specified in the
     * {@link #DB_CONNECTORS_JAVA_PATH} constant.
     */
    private void compileAndLoadDBConnectors() throws IOException, ClassNotFoundException {
        final JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();
        if (javaCompiler == null) {
            Utils.showError(new Exception(), "Java system compiler error", "It was not possible to load the system java compiler. " +
                    System.lineSeparator() + "You need to use Java Developement Kit (JDK) to run this application.");
            Platform.exit();
        }
        final File dbConnectorsJavaDirectory = new File(DB_CONNECTORS_JAVA_PATH);
        final File dbConnectorsClassesDirectory = new File(DB_CONNECTORS_CLASSES_PATH);
        final File classesRootDirectory = new File(CLASSES_ROOT_PATH);
        dbConnectorsJavaDirectory.mkdirs();
        dbConnectorsClassesDirectory.mkdirs();

        loader = new URLClassLoader(new URL[]{classesRootDirectory.toURI().toURL()});
        Thread.currentThread().setContextClassLoader(loader);

        //noinspection ConstantConditions
        ArrayList<File> files = new ArrayList<>(Arrays.asList(dbConnectorsJavaDirectory.listFiles()));
        tryToCompileAndLoad(javaCompiler, files);
    }

    /**
     * Adds files from a directory specified in the {@link #JDBC_DRIVERS_PATH} constant
     * to the classpath.
     */
    private void loadJDBCDrivers() throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        final Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
        method.setAccessible(true);
        final File jdbcRootDirectory = new File(JDBC_DRIVERS_PATH);
        //noinspection ConstantConditions
        for (File driverFile : jdbcRootDirectory.listFiles()) {
            method.invoke(systemClassLoader, driverFile.toURI().toURL());
        }
    }

    /**
     * This metod is being called from {@link #compileAndLoadDBConnectors()} method.
     */
    private void tryToCompileAndLoad(JavaCompiler javaCompiler, ArrayList<File> files) {
        ArrayList<String> classesToLoad = new ArrayList<>();
        if (files != null) {
            Iterator<File> iterator = files.iterator();
            while (iterator.hasNext()) {
                File file = iterator.next();
                String[] filenameDotSplit = file.getName().split("\\.");
                if (filenameDotSplit[filenameDotSplit.length - 1].toLowerCase().equals("java")) {
                    String className = String.join(".", (CharSequence[]) Arrays.copyOfRange(filenameDotSplit, 0, filenameDotSplit.length - 1));
                    classesToLoad.add(className);
                }
                else {
                    iterator.remove();
                }
            }

            String[] compilerArgs = new String[files.size()+4];
            compilerArgs[0] = "-classpath";
            compilerArgs[1] = System.getProperty("java.class.path") + ";." + File.separator + "classes";
            compilerArgs[2] = "-d";
            compilerArgs[3] = "classes";


            for (int i = 0; i < files.size(); i++){
                compilerArgs[i+4] = String.format("%1$s%2$s", DB_CONNECTORS_JAVA_PATH, files.get(i).getName());
            }

            javaCompiler.run(null,
                    null,
                    null,
                    compilerArgs
                    );
        }

        //noinspection ConstantConditions
        for (String className : classesToLoad) {
            try {
                Class<?> clazz = loader.loadClass("rdbuilder.dbconnector.external." + className);
                if (DBConnectorFactory.class.isAssignableFrom(clazz)) {
                    dbConnectorFactories.add((DBConnectorFactory) clazz.getMethod("getInstance").invoke(null));
                }
                else if (SQLGenerator.class.isAssignableFrom(clazz)) {
                    sqlGenerators.add((SQLGenerator) clazz.getConstructor().newInstance());
                }
            } catch (Exception ignore) {
            }
        }
    }


}
