package rdbuilder.control;

/**
 * A static class providing global access to static controllers.
 */
public class ControllerMediator {
    private static RootController rootController;
    private static DBTreeController dbTreeController;
    private static ModelPaneController modelPaneController;

    private ControllerMediator() {}

    public static DBTreeController getDBTreeController() {
        return dbTreeController;
    }

    public static ModelPaneController getModelPaneController() {
        return modelPaneController;
    }

    public static RootController getRootController() {
        return rootController;
    }

    public static void registerDBTreeController(DBTreeController dbTreeController) {
        ControllerMediator.dbTreeController = dbTreeController;
    }

    public static void registerModelPaneController(ModelPaneController modelPaneController) {
        ControllerMediator.modelPaneController = modelPaneController;
    }

    public static void registerRootController(RootController rootController) {
        ControllerMediator.rootController = rootController;
    }
}
