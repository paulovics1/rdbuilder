package rdbuilder.control;

import com.thoughtworks.xstream.XStream;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import rdbuilder.Utils;
import rdbuilder.dbconnector.DBConnector;
import rdbuilder.dbconnector.DBConnectorConverter;
import rdbuilder.dbobject.DBContainer;
import rdbuilder.dbobject.DBObject;
import rdbuilder.view.TreeCell;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.stream.Stream;

/**
 * The database tree controller, contains collection of methods related to database tree.
 */
public class DBTreeController {
    private static final javafx.scene.image.Image NEW_CONNECTION_RES = new javafx.scene.image.Image("file:../../res/new_connection.png", 20, 20, true, false);
    private static final String SAVED_CONNECTIONS_PATH = "connections.rdc";
    private final ContextMenu contextMenu;
    @FXML private TreeView<DBObject> dbTreeView;

    @SuppressWarnings("unchecked")
    public DBTreeController() {
        ControllerMediator.registerDBTreeController(this);

        final MenuItem newConnectionMI = new MenuItem("New connection");
        newConnectionMI.setGraphic(new ImageView(NEW_CONNECTION_RES));
        newConnectionMI.setOnAction(event -> {
            ControllerMediator.getRootController().newConnectionDialog();
        });
        contextMenu = new ContextMenu();
        contextMenu.getItems().add(newConnectionMI);
    }

    /**
     * Adds a new database connector to the database tree.
     * Also sorts the database connectors alphabetically.
     * @param dbConnector database connector to be added
     */
    public void add(DBConnector dbConnector) {
        TreeItem<DBObject> dbRootItem = newDBRootItem(dbConnector);
        dbTreeView.getSelectionModel().select(dbRootItem);
    }

    private void addTreeItemExpandListener(TreeItem<DBObject> treeItem) {
        treeItem.expandedProperty().addListener((observable, oldValue, newValue) -> {
            ObservableList<TreeItem<DBObject>> viewChildren = treeItem.getChildren();
            if (newValue && viewChildren.size() == 1 && viewChildren.get(0) == null) {
                loadContent(treeItem);
            }
        });
    }

    /**
     * Hangs up all database connections, which have their database connector in the
     * database tree. Is called on application exit.
     */
    public void closeAllConnections() {
        ObservableList<TreeItem<DBObject>> dbRootsTreeItems = dbTreeView.getRoot().getChildren();
        for (TreeItem<DBObject> dbObjectTreeItem : dbRootsTreeItems){
            dbObjectTreeItem.getValue().getDBConnector().disconnect();
        }
    }

    /**
     * @return the currently selected database tree item.
     */
    public TreeItem<DBObject> getSelection() {
        return dbTreeView.getSelectionModel().getSelectedItem();
    }

    /**
     * This method is automatically called upon construction of this controller. Database
     * tree is initialized, as it was not available at the construction time.
     */
    public void initialize(){
        dbTreeView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        dbTreeView.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) ->
                Platform.runLater(() -> dbTreeView.getFocusModel().focus((Integer) newValue)));
        dbTreeView.setCellFactory(param -> new TreeCell());
        dbTreeView.setRoot(new TreeItem<>());
        dbTreeView.setShowRoot(false);

        try {
            loadSavedConnections();
        } catch (IOException |ClassNotFoundException e) {
            Utils.showError(e, "An error has occured", "It was not possible to load the database connections.");
        }

        dbTreeView.getSelectionModel().clearSelection();
        dbTreeView.setContextMenu(contextMenu);
    }

    /**
     * Loads the content of a DBObject contained in the specified database tree item.
     * Uses the {@link DBObject#loadContent(boolean)} method. Closes the database tree
     * item upon failure.
     * @param treeItem database tree item of which DBObject's content is to be loaded
     */
    public void loadContent(TreeItem<DBObject> treeItem) {
        DBObject dbObject = treeItem.getValue();
        ObservableList<TreeItem<DBObject>> viewChildren = treeItem.getChildren();
        DBContainer dbContainer = (DBContainer) dbObject;
        try {
            Boolean result = TaskExecutionPopupController.newCancellableTaskExecution(() -> {
                dbContainer.loadContent(true);
                return true;
            }, "Fetching database information", dbTreeView.getScene().getWindow());
            if (result == null){
                if (treeItem.isExpanded() &&
                        viewChildren.size() == 1 &&
                        viewChildren.get(0) == null)
                treeItem.setExpanded(false);
                return;
            }
            viewChildren.clear();
            @SuppressWarnings("unchecked")
            Stream<? extends DBObject> dbObjectChildren = dbContainer.getSortedSubTreeStream();
            dbObjectChildren.forEach(child -> {
                TreeItem<DBObject> treeItemChild = newTreeItem(child);
                viewChildren.add(treeItemChild);
            });
        } catch (Exception e) {
            Utils.showError(e, "Database communication exception", "It was not possible to read database object content from the database server.");
            if (treeItem.isExpanded() &&
                    viewChildren.size() == 1 &&
                    viewChildren.get(0) == null)
            treeItem.setExpanded(false);
        }
    }

    /**
     * Uses {@link XStream} to load saved database connections from a file. The file path
     * is specified in {@link #SAVED_CONNECTIONS_PATH} constant. Creates the file if non-existent.
     */
    void loadSavedConnections() throws IOException, ClassNotFoundException {
        XStream xStream = new XStream();
        xStream.registerConverter(new DBConnectorConverter());
        final File file = new File(SAVED_CONNECTIONS_PATH);
        try{
            if (file.createNewFile()) {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                xStream.toXML(new DBConnector[]{}, fileOutputStream);
                fileOutputStream.close();
            }
        } catch (IOException e) {
            Utils.showError(e, "Output error", "An error has occured during creating database connections file.");
        }

        try {
            DBConnector[] dbConnectors = (DBConnector[]) xStream.fromXML(Utils.loadAsXML(file));

            dbTreeView.getRoot().getChildren().clear();
            for (DBConnector dbConnector : dbConnectors) {
                add(dbConnector);
            }
        }
        catch (Exception e){
            Utils.showError(e, "File read error", "An error has occured during loading of database connections.");
        }
    }

    /**
     * @return a new database tree item to be the holder of DBConnector
     */
    TreeItem<DBObject> newDBRootItem(DBConnector dbConnector) {
        TreeItem<DBObject> dbRootItem = new TreeItem<>(dbConnector.getDBObjectsRoot());
        dbRootItem.getChildren().add(null);
        dbRootItem.setExpanded(false);
        addTreeItemExpandListener(dbRootItem);
        dbTreeView.getRoot().getChildren().add(dbRootItem);
        sortDBConnectors();
        return dbRootItem;
    }

    private TreeItem<DBObject> newTreeItem(DBObject dbObject){
        TreeItem<DBObject> treeItemChild = new TreeItem<>(dbObject);
        if (dbObject instanceof DBContainer) {
            treeItemChild.getChildren().add(null);
            treeItemChild.setExpanded(false);

        }

        addTreeItemExpandListener(treeItemChild);
        return treeItemChild;
    }

    /**
     * Permanently removes the database connection of which branch is selected in the
     * database tree.
     */
    void removeSelectionRoot() {
        TreeItem<DBObject> selection = getSelection();
        while (selection.getParent() != dbTreeView.getRoot())
            selection = selection.getParent();
        dbTreeView.getRoot().getChildren().remove(selection);
        saveConnections();
    }

    /**
     * Saves the database connections present in the database tree to a file specified
     * in the {@link #SAVED_CONNECTIONS_PATH} constant.
     */
    public void saveConnections() {
        XStream xStream = new XStream();
        xStream.registerConverter(new DBConnectorConverter());
        ObservableList<TreeItem<DBObject>> dbRootsTreeItems = dbTreeView.getRoot().getChildren();
        DBConnector[] dbConnectors = new DBConnector[dbRootsTreeItems.size()];
        for (int i = 0; i < dbRootsTreeItems.size(); i++) {
            dbConnectors[i] = dbRootsTreeItems.get(i).getValue().getDBConnector();
        }
        try {
            Utils.saveAsXML(xStream.toXML(dbConnectors), new File(SAVED_CONNECTIONS_PATH));
        } catch (IOException e) {
            Utils.showError(e, "Output error", "An error has occured during saving database connections.");
        }
    }

    private void sortDBConnectors() {
        dbTreeView.getRoot().getChildren().sort((o1, o2) -> o1.getValue().getName().toLowerCase().compareTo(o2.getValue().getName().toLowerCase()));
    }
}
