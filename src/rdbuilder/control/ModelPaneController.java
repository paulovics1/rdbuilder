package rdbuilder.control;

import edu.uci.ics.jung.algorithms.layout.FRLayout2;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import rdbuilder.Utils;
import rdbuilder.control.TableDialogController.TableDialogResult;
import rdbuilder.dbobject.*;
import rdbuilder.view.ForeignKeyDialog;
import rdbuilder.view.TableDialog;
import rdbuilder.view.modeling.Canvas;
import rdbuilder.view.modeling.*;

import java.awt.*;
import java.sql.SQLException;
import java.util.*;

/**
 * This controller controls the modeling pane and it's children.
 */
public class ModelPaneController {
    private static final javafx.scene.image.Image NEW_TABLE_RES = new javafx.scene.image.Image("file:../../res/new_table.png", 20, 20, true, false);
    private static final javafx.scene.image.Image NEW_FK_RES = new javafx.scene.image.Image("file:../../res/new_fk.png", 20, 20, true, false);
    private static final javafx.scene.image.Image AUTO_RES = new javafx.scene.image.Image("file:../../res/auto.png", 20, 20, true, true);
    private static final int OFFSET = 20;
    private final HashMap<DBObject, DBObjectView> dbObjectViewMap = new HashMap<>();
    private final HashSet<DBTableView> dbTableViews = new HashSet<>();
    private final HashSet<DBObjectView> notPermanentlySelectedDBObjectViews = new HashSet<>();
    private final HashSet<DBObjectView> selectedDBObjectViews = new HashSet<>();
    private final HashSet<DBObjectView> temporarilySelectedDBObjectViews = new HashSet<>();
    private final ContextMenu contextMenu;
    private final BooleanProperty schemaDrawnProperty = new SimpleBooleanProperty(false);
    @FXML public ScrollPane dbModelScrollPane;
    @FXML public Pane dbModelPane;
    @FXML public Canvas dbModelCanvas;
    @FXML public Pane dbModelParentPane;
    private Schema currentSchema;
    private SelectionRectangle selectionRectangle;
    private Point2D selectionOrigin = new Point2D(0, 0);
    private boolean mouseDragged = false;
    private DBForeignKeyView draggedForeignKeyView;

    public ModelPaneController() {
        ControllerMediator.registerModelPaneController(this);
        final RootController rootController = ControllerMediator.getRootController();
        contextMenu = new ContextMenu();
        initializeContextMenu();
    }

    /**
     * Adds the foreign key to the current drawn schema and associates
     * it with a new {@link DBForeignKeyView}.
     */
    public void addNewForeignKey(ForeignKey foreignKey){
        if (foreignKey != null && !foreignKey.getColumns().isEmpty()){
            DBForeignKeyView foreignKeyView = new DBForeignKeyView(foreignKey, this);
            currentSchema.registerForeignKey(foreignKey);
            dbObjectViewMap.put(foreignKey, foreignKeyView);
        }
    }

    /**
     * Automatically layouts the drawn tables using the Fruchterman-Reingold graph algoritm
     * ({@link FRLayout2}).
     */
    public void automaticLayout() {
        Graph<DBTableView, DBForeignKeyView> graph = new UndirectedSparseMultigraph<>();

        for (DBTableView tableView : dbTableViews){
            graph.addVertex(tableView);
        }

        dbObjectViewMap.values().stream()
                .filter(dbObjectView -> dbObjectView instanceof DBForeignKeyView).forEach(dbObjectView -> {
            DBForeignKeyView foreignKeyView = (DBForeignKeyView) dbObjectView;
            graph.addEdge(foreignKeyView,
                    (DBTableView) getDBObjectView(foreignKeyView.getDBObject().getTable()),
                    (DBTableView) getDBObjectView(foreignKeyView.getDBObject().getReferencedTable()));
        });

        FRLayout2<DBTableView, DBForeignKeyView> layout = new FRLayout2<>(graph);
        layout.setAttractionMultiplier(1);
        layout.setRepulsionMultiplier(1.5);
        layout.setMaxIterations(10000);

        layout.reset();
        dbModelPane.layout();
        Platform.runLater(() -> {
            layout.setSize(getComputedDimension());
            layout.initialize();

            while (!layout.done()){
                layout.step();
            }
            for (DBTableView tableView : dbTableViews) {
                tableView.setPosition(layout.transform(tableView));
            }
            ArrayList<DBTableView> coordsSortedList = new ArrayList<>();
            coordsSortedList.addAll(dbTableViews);
            coordsSortedList.sort((v1, v2) -> Integer.compare(v1.getLeft(), v2.getLeft()));
            for (int i = 0; i < coordsSortedList.size(); i++) {
                for (int j = i+1; j < coordsSortedList.size(); j++){
                    DBTableView v1 = coordsSortedList.get(i), v2 = coordsSortedList.get(j);
                    if (v1.intersects(v2)){
                        v2.setPosition(new Point(v1.getRight()+20, v2.getTop()));
                    }
                }
            }
            updateBounds();
        });
    }

    /**
     * Warning dialog shown upon object deletion request.
     * @return true if the user confirms the deletion, false otherwise.
     */
    private boolean checkDeletion() {
        if (!selectedDBObjectViews.isEmpty()){
            final Alert dialog = new Alert(Alert.AlertType.WARNING,
                    "This action can not be undone.",
                    ButtonType.NO, ButtonType.YES);
            ((Button)dialog.getDialogPane().lookupButton(ButtonType.YES)).setDefaultButton(false);
            ((Button)dialog.getDialogPane().lookupButton(ButtonType.NO)).setDefaultButton(true);

            ButtonBar buttonBar = (ButtonBar) dialog.getDialogPane().lookup(".button-bar");
            buttonBar.setButtonOrder(ButtonBar.BUTTON_ORDER_NONE);

            dialog.setTitle("Confirm deletion");
            dialog.setHeaderText("Are you sure you want to delete the selected objects?");
            dialog.initOwner(dbModelPane.getScene().getWindow());


            final Optional<ButtonType> answer = dialog.showAndWait();
            return answer.isPresent() && answer.get().equals(ButtonType.YES);
        }
        return true;
    }

    /**
     * Removes all drawn object from the modeling pane.
     */
    public void clear(){
        @SuppressWarnings("unchecked")
        HashMap<DBObject, DBObjectView> dbObjectViewMapClone = (HashMap<DBObject, DBObjectView>) dbObjectViewMap.clone();
        for (DBObjectView dbObjectView: dbObjectViewMapClone.values()){
            dbObjectView.destroy();
        }
    }

    /**
     * @return current drawn schema converted to XML using {@link DBObjectConverter}
     */
    public String currentSchemaToXML() {
        for (DBTableView dbTableView : dbTableViews) {
            DBObjectConverter.coordinateMap.put(dbTableView.getDBObject(), dbTableView.getPosition());
        }
        return DBObjectConverter.toXML(currentSchema);
    }

    /**
     * Deletes the selected objects and other objects dependent on them.
     */
    private void deleteSelection() {
        if (!checkDeletion())
            return;

        @SuppressWarnings("unchecked")
        HashSet<DBObjectView> selectionHashSetClone = (HashSet<DBObjectView>) selectedDBObjectViews.clone();
        for (DBObjectView dbObjectView : selectionHashSetClone) {
            dbObjectView.getDBObject().destroy();
        }
    }

    public void deselectAll(){
        for (DBObjectView dbObjectView : selectedDBObjectViews){
            dbObjectView.setSelected(false);
        }
        selectedDBObjectViews.clear();
    }

    private void destroySelectionRectangle() {
        if (selectionRectangle != null){
            Utils.platformRunAndWait(() ->
                dbModelPane.getChildren().remove(selectionRectangle)
            );
            selectionRectangle = null;
        }
    }

    /**
     * Draws the schema in modeling pane in 3 steps:
     * <ol>
     *     <li>Clears current drawn objects</li>
     *     <li>Reloads the schema content, if connected to database</li>
     *     <li>Creates graphical representation of schema objects and layouts them</li>
     * </ol>
     * @return success of the whole process
     */
    public boolean drawSchema(Schema schema) throws SQLException {
        try {
            TaskExecutionPopupController.newTaskExecution(() -> {
                clear();
                return true;
            }, "Clearing unused objects.", dbModelPane.getScene().getWindow());

            currentSchema = null;

            TaskExecutionPopupController.newTaskExecution(() -> {
                schema.loadContent(true);
                return true;
            }, "Fetching database objects.", dbModelPane.getScene().getWindow());

            TaskExecutionPopupController.newTaskExecution(() -> {
                loadSchemaObjectViews(schema);
                automaticLayout();
                ControllerMediator.getRootController().onSchemaDrawn();
                dbModelPane.setDisable(false);
                return true;
            }, "Drawing and layouting objects.", dbModelPane.getScene().getWindow());

            currentSchema = schema;
            schemaDrawnProperty.setValue(true);
            return true;
        } catch (Exception e) {
            Utils.showError(e, "Schema draw error", "An error has occured during drawing the schema.");
        }
        return false;
    }

    /**
     * Computes the dimension of area across which to automatically layout the tables.
     */
    private Dimension getComputedDimension(){
        int minimalVolume = 0;
        for (DBTableView tableView : dbTableViews){
            minimalVolume += tableView.getWidth()*tableView.getHeight();
        }
        int size = (int) (Math.sqrt(minimalVolume)) + 200;
        return new Dimension((int) (size*1.6), (int) (size*1.1));
    }

    public Schema getCurrentSchema() {
        return currentSchema;
    }

    /**
     * @return the graphical representation of a {@link Table} or a {@link ForeignKey}.
     */
    public DBObjectView getDBObjectView(DBObject dbObject){
        return dbObjectViewMap.get(dbObject);
    }

    public HashMap<DBObject, DBObjectView> getDbObjectViewMap() {
        return dbObjectViewMap;
    }

    /**
     * @return a {@link BooleanProperty} that informs if any schema is drawn in the modeling pane
     */
    public BooleanProperty getSchemaDrawnProperty() {
        return schemaDrawnProperty;
    }

    /**
     * This method is automatically called upon construction of this controller.
     * Mostly objects that were not available during the constructor call are being initialized.
     */
    public void initialize() {

        Rectangle clip = new Rectangle();
        dbModelScrollPane.viewportBoundsProperty().addListener((observable, oldValue, newValue) -> {
            clip.setWidth(newValue.getWidth());
            clip.setHeight(newValue.getHeight());
        });
        dbModelScrollPane.prefHeightProperty().bind(dbModelParentPane.heightProperty());
        dbModelScrollPane.prefWidthProperty().bind(dbModelParentPane.widthProperty());

        dbModelScrollPane.hvalueProperty().addListener((observable, oldValue, newValue) -> {
            int offsetX = (int) (Math.max(0, dbModelPane.getLayoutBounds().getWidth()
                    - dbModelScrollPane.getViewportBounds().getWidth()) *
                    (newValue.doubleValue()));
            dbModelCanvas.setScrollOffset(offsetX, null);
        });

        dbModelScrollPane.vvalueProperty().addListener((observable, oldValue, newValue) -> {
            int offsetY = (int) (Math.max(0, dbModelPane.getLayoutBounds().getHeight()
                    - dbModelScrollPane.getViewportBounds().getHeight()) *
                    (newValue.doubleValue()));
            dbModelCanvas.setScrollOffset(null, offsetY);
        });

        dbModelCanvas.widthProperty().bind(dbModelParentPane.widthProperty());
        dbModelCanvas.heightProperty().bind(dbModelParentPane.heightProperty());

        dbModelScrollPane.viewportBoundsProperty().addListener((observable, oldValue, newValue) -> {
            onDBModelScrollPaneViewportChange(newValue);
        });
        onDBModelScrollPaneViewportChange(dbModelScrollPane.getViewportBounds());

        dbModelScrollPane.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                dbModelPane.requestFocus();
            }
        });

        dbModelParentPane.disableProperty().bind(dbModelPane.disableProperty());

        dbModelPane.addEventFilter(MouseEvent.DRAG_DETECTED, mouseEvent ->
                dbModelPane.startFullDrag()
        );

        dbModelPane.setOnMousePressed(event -> {
            selectionOrigin = new Point2D(event.getX(), event.getY());
            contextMenu.hide();
            if (event.getButton() == MouseButton.PRIMARY)
                if (!event.isControlDown()) {
                    deselectAll();
                }
        });

        dbModelPane.setOnMouseReleased(event -> {
            switch (event.getButton()) {
                case PRIMARY:
                    destroySelectionRectangle();
                    selectedDBObjectViews.addAll(temporarilySelectedDBObjectViews);
                    temporarilySelectedDBObjectViews.clear();
                    event.consume();
                    break;
                case SECONDARY:
                    if (draggedForeignKeyView != null) {
                        draggedForeignKeyView.destroy();
                        draggedForeignKeyView = null;
                    } else {
                        contextMenu.show(dbModelPane, event.getScreenX(), event.getScreenY());
                    }
                    event.consume();
                    break;
            }
            setMouseDragged(false);
        });

        dbModelPane.setOnMouseDragged(event -> {
            if (selectionRectangle == null) {
                newSelection();
                notPermanentlySelectedDBObjectViews.clear();
                notPermanentlySelectedDBObjectViews.addAll(dbObjectViewMap.values());
                notPermanentlySelectedDBObjectViews.removeAll(selectedDBObjectViews);
            }

            if (event.getButton() == MouseButton.PRIMARY) {
                final double width = event.getX() - selectionOrigin.getX();
                final double height = event.getY() - selectionOrigin.getY();
                if (width > 0) {
                    selectionRectangle.setX(selectionOrigin.getX());
                    selectionRectangle.setWidth(width);
                } else {
                    selectionRectangle.setX(selectionOrigin.getX() + width);
                    selectionRectangle.setWidth(-width);
                }
                if (height > 0) {
                    selectionRectangle.setY(selectionOrigin.getY());
                    selectionRectangle.setHeight(height);
                } else {
                    selectionRectangle.setY(selectionOrigin.getY() + height);
                    selectionRectangle.setHeight(-height);
                }

                for (DBObjectView dbObjectView : notPermanentlySelectedDBObjectViews) {
                    final boolean intersects = dbObjectView.selectionIntersects(selectionRectangle);
                    if (intersects && !dbObjectView.isSelected()) {
                        temporarilySelectedDBObjectViews.add(dbObjectView);
                    } else if (!intersects && dbObjectView.isSelected()) {
                        temporarilySelectedDBObjectViews.remove(dbObjectView);
                    }
                    dbObjectView.setSelected(intersects);
                }

                if (!temporarilySelectedDBObjectViews.isEmpty() && isColumnSelection()) {
                    selectedColumnsToTables();
                }
            }
        });

        dbModelPane.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.DELETE || event.getCode() == KeyCode.BACK_SPACE) {
                deleteSelection();
                event.consume();
            }
        });
    }

    private void initializeContextMenu(){
        MenuItem newTableMI = new MenuItem("New table");
        newTableMI.setGraphic(new ImageView(NEW_TABLE_RES));
        newTableMI.setOnAction(event -> {
            TableDialogResult result = new TableDialog(dbModelPane.getScene().getWindow(), null).showAndGetResult();
            if (result != null){
                DBTableView tableView = new DBTableView(result.table, this);
                currentSchema.addChild(result.table);
                dbObjectViewMap.put(result.table, tableView);
                dbTableViews.add(tableView);
                tableView.setPosition(selectionOrigin);
            }
        });

        MenuItem newForeignKeyMI = new MenuItem("New foreign key");
        newForeignKeyMI.setGraphic(new ImageView(NEW_FK_RES));
        newForeignKeyMI.setOnAction(event -> {
            ForeignKey result = new ForeignKeyDialog(dbModelPane.getScene().getWindow(), null).showAndGetResult();
            addNewForeignKey(result);
        });

        MenuItem autoLayoutMI = new MenuItem("Automatic layout");
        autoLayoutMI.setGraphic(new ImageView(AUTO_RES));
        autoLayoutMI.setOnAction(event -> automaticLayout());
        contextMenu.getItems().addAll(newTableMI, newForeignKeyMI, new SeparatorMenuItem(), autoLayoutMI);
    }

    /**
     * @return true if column selection mode is activated, false otherwise
     */
    private boolean isColumnSelection(){
        Iterator<DBObjectView> iterator = selectedDBObjectViews.iterator();
        return iterator.hasNext() && iterator.next() instanceof DBColumnView;
    }

    public boolean isMouseDragged(){
        return mouseDragged;
    }

    /**
     * Sets a helper variable value to true if the mouse is being dragged, false otherwise.
     */
    public void setMouseDragged(boolean mouseDragged) {
        this.mouseDragged = mouseDragged;
        if (!mouseDragged){
            setTableViewsColumnsMouseTransparent(false);
            for (DBTableView tableView : dbTableViews){
                tableView.setForeignKeyDraggedOver(false);
            }
        }
    }

    /**
     * Loads the saved position of graphical objects.
     * @param coordinateMap map of coordinates, obtained from {@link DBObjectConverter}
     */
    public void loadDBObjectsPositions(HashMap<DBObject, Point2D> coordinateMap) {
        for (Map.Entry<DBObject, Point2D> entry : coordinateMap.entrySet()){
            ((DBTableView) dbObjectViewMap.get(entry.getKey())).setPosition(entry.getValue());
        }
    }

    /**
     * Associates every objects of schema with a new graphical representation.
     */
    public void loadSchemaObjectViews(Schema schema) throws SQLException {
        Collection<Table> children = schema.getSubTree();
        for (Table table : children){
            DBTableView tableView = new DBTableView(table, this);
            dbObjectViewMap.put(table, tableView);
            dbTableViews.add(tableView);
        }

        Collection<ForeignKey> foreignKeys = schema.getForeignKeys();
        for (ForeignKey foreignKey : foreignKeys){
            DBForeignKeyView foreignKeyView = new DBForeignKeyView(foreignKey, this);
            dbObjectViewMap.put(foreignKey, foreignKeyView);
        }
    }

    /**
     * Creates a new {@link SelectionRectangle}.
     */
    private void newSelection() {
        destroySelectionRectangle();
        selectionRectangle = new SelectionRectangle(selectionOrigin.getX(), selectionOrigin.getY());
        Utils.platformRunAndWait(() ->
            dbModelPane.getChildren().add(selectionRectangle)
        );
    }

    private void onDBModelScrollPaneViewportChange(Bounds bounds){
        dbModelPane.setMinWidth(bounds.getWidth());
        dbModelPane.setMinHeight(bounds.getHeight());
    }

    private void primaryMouseDrag(DBObjectView dbObjectView, MouseEvent event) {
        if (!mouseDragged) {
            boolean columnSelection = isColumnSelection();
            if (!dbObjectView.isSelected() &&
                    !(dbObjectView instanceof DBColumnView &&
                      ((DBColumnView) dbObjectView).getParentTableView().isSelected())) {
                if (!event.isControlDown()) {
                    deselectAll();
                }
                if (!columnSelection && dbObjectView instanceof DBColumnView){
                    dbObjectView = ((DBColumnView) dbObjectView).getParentTableView();
                }
                setSelected(dbObjectView, true);
            }

            Iterator<DBObjectView> iterator = selectedDBObjectViews.iterator();
            if (dbObjectView instanceof DBColumnView && dbObjectView.isSelected()) {
                while (iterator.hasNext()) {
                    DBObjectView view = iterator.next();
                    if (!(view instanceof DBColumnView)){
                        view.setSelected(false);
                        iterator.remove();
                    }
                }
            }
            else {
                final HashSet<DBTableView> tableViewsToSelect = new HashSet<>();
                while (iterator.hasNext()) {
                    DBObjectView view = iterator.next();
                    if (view instanceof DBForeignKeyView){
                        view.setSelected(false);
                        iterator.remove();
                    }
                    else if (view instanceof DBColumnView){
                        view.setSelected(false);
                        iterator.remove();
                        DBTableView tableView = ((DBColumnView) view).getParentTableView();
                        tableViewsToSelect.add(tableView);
                    }
                }

                for (DBTableView dbTableView : tableViewsToSelect){
                    setSelected(dbTableView, true);
                }
            }
            for (DBObjectView view : selectedDBObjectViews) {
                view.initializeDeltaPosition(event);
            }
            setMouseDragged(true);
        }
        for (DBObjectView view : selectedDBObjectViews){
            view.drag(event);
        }
        updateBounds();
    }

    private void secondaryMouseDrag(DBObjectView dbObjectView, MouseEvent event) {
        if (!mouseDragged) {
            DBTableView tableView = null;
            if (dbObjectView instanceof DBColumnView){
                tableView = ((DBColumnView) dbObjectView).getParentTableView();
            }
            else if (dbObjectView instanceof DBTableView) {
                tableView = (DBTableView) dbObjectView;
            }
            if (tableView != null) {
                if (draggedForeignKeyView != null) {
                    draggedForeignKeyView.getDBObject().destroy();
                }
                draggedForeignKeyView = new DBForeignKeyView(new ForeignKey(currentSchema, "", (Table) tableView.getDBObject(), null), this);
            }
            setMouseDragged(true);
            setTableViewsColumnsMouseTransparent(true);
        }

        draggedForeignKeyView.pointTo(dbObjectView.getParentX(event), dbObjectView.getParentY(event));
        draggedForeignKeyView.getTableView().updatePinSides();
    }

    /**
     * Selects all tables of which at least one column is selected. Columns are deselected afterwards.
     */
    private void selectedColumnsToTables(){
        Iterator<DBObjectView> iterator = selectedDBObjectViews.iterator();
        HashSet<DBTableView> toSelect = new HashSet<>();
        while (iterator.hasNext()) {
            DBObjectView view = iterator.next();
            if (view instanceof DBColumnView) {
                view.setSelected(false);
                iterator.remove();
                toSelect.add(((DBColumnView) view).getParentTableView());
            }
        }

        for (DBTableView tableView : toSelect){
            setSelected(tableView, true);
        }
    }

    /**
     * Sets the selection state of a object.
     */
    public void setSelected(DBObjectView dbObjectView, boolean selected){
        dbObjectView.setSelected(selected);
        if (selected){
            selectedDBObjectViews.add(dbObjectView);
        }
        else {
            selectedDBObjectViews.remove(dbObjectView);
        }

    }

    /**
     * Makes all table columns ignore mouse events, if parameter is true. Reverts
     * if false.
     */
    public void setTableViewsColumnsMouseTransparent(boolean bool){
        for (DBTableView tableView : dbTableViews){
            tableView.getColumnViewsHolder().setMouseTransparent(bool);
        }
    }

    /**
     * Removes an object from the modeling pane.
     */
    public void unregisterDBObjectView(DBObjectView dbObjectView) {
        selectedDBObjectViews.remove(dbObjectView);
        dbObjectViewMap.remove(dbObjectView.getDBObject());
        temporarilySelectedDBObjectViews.remove(dbObjectView);
        notPermanentlySelectedDBObjectViews.remove(dbObjectView);

        if (dbObjectView instanceof DBTableView)
            dbTableViews.remove(dbObjectView);
    }

    /**
     * Updates the bounds of the modeling pane according to the edge objects.
     */
    public void updateBounds() {
        int minY;
        int minX;
        minX = Integer.MAX_VALUE;
        minY = Integer.MAX_VALUE;

        for (DBTableView tableView : dbTableViews) {
            int left, top;
            left = tableView.getLeft();
            top = tableView.getTop();

            if (left < minX){
                minX = left;
            }
            if (top < minY){
                minY = top;
            }
        }

        minX -= OFFSET;
        minY -= OFFSET;

        for (DBTableView tableView : dbTableViews) {
            tableView.move(-minX, -minY);
        }
        dbModelCanvas.setGridOffset(minX, minY);
    }

    public void viewMouseClick(DBObjectView dbObjectView, MouseEvent event){
        dbModelPane.requestFocus();
        contextMenu.hide();
        if (event.getButton() != MouseButton.PRIMARY ||
                (event.getEventType() == MouseEvent.MOUSE_CLICKED && !event.isStillSincePress())){
            return;
        }

        if (dbObjectView instanceof DBColumnView) {
            DBTableView dbTableView = ((DBColumnView) dbObjectView).getParentTableView();
            if (isColumnSelection() || (dbTableView.isSelected() && !event.isControlDown())){
                if (!event.isControlDown()) {
                    deselectAll();
                }
                else {
                    setSelected(dbObjectView, !dbObjectView.isSelected());
                    return;
                }
                setSelected(dbTableView, false);
                setSelected(dbObjectView, true);
                return;
            }
            else {
                dbObjectView = dbTableView;
            }
        }

        if (!event.isControlDown()) {
            deselectAll();
        }

        if (!dbObjectView.isSelected()) {
            setSelected(dbObjectView, true);
        }
        else if (event.isControlDown()) {
            setSelected(dbObjectView, false);
        }
        selectedColumnsToTables();
    }

    public void viewMouseDrag(DBObjectView dbObjectView, MouseEvent event) {
        dbModelPane.requestFocus();
        switch (event.getButton()){
            case PRIMARY:
                primaryMouseDrag(dbObjectView, event);
                break;
            case SECONDARY:
                secondaryMouseDrag(dbObjectView, event);
                break;
        }

    }

    public void viewMouseDragEntered(DBObjectView dbObjectView, MouseDragEvent event) {
        if (event.getButton() == MouseButton.SECONDARY && mouseDragged &&
                draggedForeignKeyView != null && dbObjectView instanceof DBTableView){
            ((DBTableView)dbObjectView).setForeignKeyDraggedOver(true);
        }
    }

    public void viewMouseDragExited(DBObjectView dbObjectView, MouseDragEvent event) {
        if (event.getButton() == MouseButton.SECONDARY && mouseDragged &&
                draggedForeignKeyView != null && dbObjectView instanceof DBTableView){
            ((DBTableView)dbObjectView).setForeignKeyDraggedOver(false);
        }
    }

    public void viewMouseDragRelease(DBTableView dbTableView, MouseDragEvent event) {
        if (event.getButton() == MouseButton.SECONDARY && draggedForeignKeyView != null){
            final ForeignKey foreignKey = draggedForeignKeyView.getDBObject();
            foreignKey.setReferencedTable((Table) dbTableView.getDBObject());
            ForeignKey result = new ForeignKeyDialog(dbModelPane.getScene().getWindow(), foreignKey).showAndGetResult();
            addNewForeignKey(result);
            draggedForeignKeyView.destroy();
            draggedForeignKeyView = null;
        }
        setMouseDragged(false);
    }

    public void viewMouseRelease(DBObjectView dbObjectView, MouseEvent event){
        dbModelPane.requestFocus();
        setMouseDragged(false);
        if (draggedForeignKeyView != null) {
            draggedForeignKeyView.getDBObject().destroy();
            draggedForeignKeyView = null;
        }
    }
}
