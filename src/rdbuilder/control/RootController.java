package rdbuilder.control;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import rdbuilder.Utils;
import rdbuilder.dbconnector.DBConnector;
import rdbuilder.dbconnector.GenericDBConnector;
import rdbuilder.dbconnector.SQLGenerator;
import rdbuilder.dbobject.*;
import rdbuilder.view.ConnectionDialog;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import static rdbuilder.Main.sqlGenerators;

/**
 * This is the main application controller.
 */
public class RootController {
    public static Stage primaryStage;
    private static ButtonType BUTTONTYPE_CONTINUE = new ButtonType("Continue", ButtonType.OK.getButtonData());
    @FXML public MenuItem exportSQLMenuItem;
    @FXML public MenuItem saveSchemaMenuItem;
    @FXML public MenuItem configureConnectionMenuItem;
    @FXML public MenuItem disconnectMenuItem;
    @FXML public MenuItem forgetMenuItem;
    @FXML public Label currentPathLabel;
    @FXML public Pane dbModelParentPane;
    @FXML public TreeView<DBObject> dbTreeView;
    @FXML public VBox root;
    @FXML public MenuBar menuBar;
    @FXML public Button drawButton;
    @FXML public Button uploadButon;

    public RootController() {
        ControllerMediator.registerRootController(this);
    }

    /**
     * Uploads the schema model drawn in modeling pane to a schema selected in database tree.
     */
    public void applySchema() {
        DBObject selectedDBObject = dbTreeView.getSelectionModel().getSelectedItem().getValue();
        Schema remoteSchema;
        if (selectedDBObject instanceof Schema)
            remoteSchema = (Schema) selectedDBObject;
        else if (selectedDBObject instanceof Table)
            remoteSchema = ((Table) selectedDBObject).getParent();
        else return;

        Alert confirmationAlert = new Alert(Alert.AlertType.CONFIRMATION, null, BUTTONTYPE_CONTINUE ,ButtonType.CANCEL);
        confirmationAlert.setTitle("Confirm changes");
        confirmationAlert.setHeaderText(
                String.format("You are about to merge the current working schema to:%s%s",
                        System.lineSeparator(),
                        remoteSchema.getPath()
                        ));
        confirmationAlert.setContentText("All conflicting database objects will be dropped. This action cannot be undone.");

        Optional<ButtonType> result = confirmationAlert.showAndWait();
        if (result.isPresent() && result.get() == BUTTONTYPE_CONTINUE){
            try {
                TaskExecutionPopupController.newCancellableTaskExecution(() -> {
                    final DBConnector dbConnector = remoteSchema.getDBConnector();
                    final SQLGenerator sqlGenerator = dbConnector.getSQLGenerator();
                    sqlGenerator.setSchema(ControllerMediator.getModelPaneController().getCurrentSchema());
                    dbConnector.executeSQLScript(sqlGenerator.generateSQL(), remoteSchema);
                    return null;
                }, "Executing SQL query", root.getScene().getWindow());
            } catch (Exception e) {
                Utils.showError(e, "SQL script execution exception", "It was not possible to execute all SQL commands.");
            }
        }
    }

    /**
     * A confirmation dialog shown upon schema draw request,
     * if there already is a schema drawn in the modeling pane.
     * @return confirmation state
     */
    private boolean checkSchemaOverwrite(){
        final ModelPaneController modelPaneController = ControllerMediator.getModelPaneController();
        if (modelPaneController.getCurrentSchema() != null){
            final Alert dialog = new Alert(Alert.AlertType.WARNING,
                    "Are you sure you want to continue?",
                    ButtonType.NO, ButtonType.YES);
            ((Button)dialog.getDialogPane().lookupButton(ButtonType.YES)).setDefaultButton(false);
            ((Button)dialog.getDialogPane().lookupButton(ButtonType.NO)).setDefaultButton(true);

            ButtonBar buttonBar = (ButtonBar) dialog.getDialogPane().lookup(".button-bar");
            buttonBar.setButtonOrder(ButtonBar.BUTTON_ORDER_NONE);

            dialog.setTitle("Confirm changes");
            dialog.setHeaderText("You already have a schema model drawn in the modeling pane." +
                    System.lineSeparator() +
                    "All unsaved changes will be lost by performing this action.");
            dialog.initOwner(root.getScene().getWindow());


            final Optional<ButtonType> answer = dialog.showAndWait();
            return answer.isPresent() && answer.get().equals(ButtonType.YES);
        }
        return true;
    }

    /**
     * Calls {@link DBTreeController#closeAllConnections()}.
     */
    public void closeAllConnections() {
        ControllerMediator.getDBTreeController().closeAllConnections();
    }

    /**
     * Creates a database connection configuration dialog, configuring the connection currently
     * selected in the database tree.
     */
    public void configureConnectionDialog() {
        TreeItem<DBObject> selectedItem = dbTreeView.getSelectionModel().getSelectedItem();
        GenericDBConnector dbConnector = (GenericDBConnector) selectedItem.getValue().getRoot().getDBConnector();
        DBConnector newDBConnector = new ConnectionDialog(primaryStage,
                dbConnector).showAndGetDBConnector();
        if (newDBConnector != null) {
            dbConnector.disconnect();
            final DBTreeController dbTreeController = ControllerMediator.getDBTreeController();
            dbTreeController.removeSelectionRoot();
            dbTreeController.add(newDBConnector);
            dbTreeController.saveConnections();
        }
    }

    /**
     * Calls {@link DBTreeController#removeSelectionRoot()} after a confirmed confirmation dialog.
     */
    public void deleteConnection() {
        final DBTreeController dbTreeController = ControllerMediator.getDBTreeController();
        final Alert dialog = new Alert(Alert.AlertType.WARNING,
                "This action can not be undone.",
                ButtonType.NO, ButtonType.YES);
        ((Button)dialog.getDialogPane().lookupButton(ButtonType.YES)).setDefaultButton(false);
        ((Button)dialog.getDialogPane().lookupButton(ButtonType.NO)).setDefaultButton(true);

        ButtonBar buttonBar = (ButtonBar) dialog.getDialogPane().lookup(".button-bar");
        buttonBar.setButtonOrder(ButtonBar.BUTTON_ORDER_NONE);

        dialog.setTitle("Confirm connection deletion");
        dialog.setHeaderText(String.format("Are you sure you want to forget \"%s\" connection?",
                dbTreeController.getSelection().getValue().getRoot().getName()));
        dialog.initOwner(root.getScene().getWindow());


        final Optional<ButtonType> answer = dialog.showAndWait();
        if (answer.isPresent() && answer.get().equals(ButtonType.YES))
            dbTreeController.removeSelectionRoot();
    }

    /**
     * Disconnects the database connector selected in database tree and closes the
     * respective tree item.
     */
    public void disconnect() {
        TreeItem<DBObject> treeItem = dbTreeView.getSelectionModel().getSelectedItem();
        while (treeItem.getParent() != dbTreeView.getRoot()){
            treeItem = treeItem.getParent();
        }
        treeItem.getChildren().clear();
        treeItem.getChildren().add(null);
        treeItem.setExpanded(false);
        final DBRoot root = treeItem.getValue().getRoot();
        root.clearChildren();
        root.getDBConnector().disconnect();
        dbTreeView.getSelectionModel().select(treeItem);
    }

    /**
     * Calls {@link #checkSchemaOverwrite()} and {@link ModelPaneController#drawSchema(Schema)}
     * @return schema drawing success
     */
    public boolean drawSchema(Schema schema) {
        if (!checkSchemaOverwrite())
            return false;
        try {
            return ControllerMediator.getModelPaneController().drawSchema(schema);
        } catch (SQLException e) {
            Utils.showError(e, "Database connection exception", "An error has occured during the communication with the database.");
        }
        return false;
    }

    public void exitApplication() {
        Platform.exit();
    }

    /**
     * Exports SQL script with DDL queries of the currently drawn schema in the modeling pane
     * to a file choosen via a {@link FileChooser}. Also shows a dialog to choose SQL generator
     * to be used.
     */
    public void exportSQLdump() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save SQL dump file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("SQL file", "*.sql"));
        File file = fileChooser.showSaveDialog(primaryStage);
        if (file != null){
            ChoiceDialog<SQLGenerator> sqlGeneratorChoiceDialog = new ChoiceDialog<>(null, sqlGenerators);
            sqlGeneratorChoiceDialog.setTitle("Choose SQL syntax");
            sqlGeneratorChoiceDialog.setHeaderText("Please choose SQL generator to use.");
            sqlGeneratorChoiceDialog.setContentText("");
            sqlGeneratorChoiceDialog.initOwner(primaryStage);

            final TreeItem<DBObject> selectedItem = dbTreeView.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                final DBObject selectedDBObject = selectedItem.getValue();
                if (selectedDBObject != null) {
                    final Class<? extends SQLGenerator> sqlGeneratorClass = selectedDBObject.getDBConnector().getSQLGenerator().getClass();
                    for (SQLGenerator sqlGenerator : sqlGenerators) {
                        if (sqlGenerator.getClass() == sqlGeneratorClass) {
                            sqlGeneratorChoiceDialog.setSelectedItem(sqlGenerator);
                            break;
                        }
                    }
                }
            }

            Optional<SQLGenerator> sqlGenerator = sqlGeneratorChoiceDialog.showAndWait();
            if (sqlGenerator.isPresent()){
                sqlGenerator.get().setSchema(ControllerMediator.getModelPaneController().getCurrentSchema());
                sqlGenerator.get().saveSQLDumpToFile(file);
            }
        }
    }

    /**
     * This method is automatically called upon construction of this controller.
     * Mostly objects that were not available during the constructor call are being initialized.
     */
    public void initialize() {
        final ModelPaneController modelPaneController = ControllerMediator.getModelPaneController();

        primaryStage.setOnCloseRequest(event -> {
            if (ControllerMediator.getModelPaneController().getCurrentSchema() != null){

            }
            closeAllConnections();
        });

        dbTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            final boolean is_null = newValue == null;
            final boolean isSchemaSuccSelected = !is_null && (newValue.getValue() instanceof Schema ||
                    newValue.getValue() instanceof Table);
            final boolean modelSchemaNotNull = modelPaneController.getCurrentSchema() != null;
            drawButton.setDisable(is_null || !isSchemaSuccSelected);
            uploadButon.setDisable(is_null || !(isSchemaSuccSelected && modelSchemaNotNull));


            final RootController rootController = ControllerMediator.getRootController();
            rootController.configureConnectionMenuItem.setDisable(is_null);
            rootController.forgetMenuItem.setDisable(is_null);
            try {
                rootController.disconnectMenuItem.setDisable(is_null || !(newValue.getValue().getRoot().getDBConnector().isConnected()));
            } catch (SQLException e) {
                rootController.disconnectMenuItem.setDisable(true);
            }
        });
    }

    /**
     * Draws the schema model of a schema or table's parent selected in the database tree.
     */
    public void loadSchemaModelFromDatabase() {
        final DBObject dbObject = ControllerMediator.getDBTreeController().getSelection().getValue();
        Schema schema;
        if (dbObject instanceof Schema) {
            schema = (Schema) dbObject;
        }
        else
            schema = ((Table) dbObject).getParent();

        if (schema != null) {
            if (drawSchema(schema)) {
                currentPathLabel.setText(schema.getPath());
            }
        }
    }

    /**
     * Draws a schema model loaded from a file choosen via a {@link FileChooser}.
     */
    public void loadSchemaModelFromFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load schema from file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Schema file", "*.schema"));
        File file = fileChooser.showOpenDialog(primaryStage);
        if (file != null) {
            try {
                final Schema schema = (Schema) DBObjectConverter.fromXML(Utils.loadAsXML(file));
                if (drawSchema(schema)) {
                    final ModelPaneController modelPaneController = ControllerMediator.getModelPaneController();
                    modelPaneController.loadDBObjectsPositions(DBObjectConverter.coordinateMap);

                    currentPathLabel.setText(file.getPath());
                }
            } catch (IOException e) {
                Utils.showError(e, "I/O error", "It was not possible to load schema model from the specified file.");
            }
        }
    }

    /**
     * Shows a databae connection configuration dialog to configure a new connection.
     */
    public void newConnectionDialog() {
        DBConnector dbConnector = new ConnectionDialog(primaryStage).showAndGetDBConnector();
        if (dbConnector != null) {
            final DBTreeController dbTreeController = ControllerMediator.getDBTreeController();
            dbTreeController.add(dbConnector);
            dbTreeController.saveConnections();
        }
    }

    /**
     * Draws a brand new empty schema in the modeling pane.
     */
    public void newSchema() {
        if (drawSchema(new Schema())) {
            currentPathLabel.setText("New schema");
        }
    }

    public void onSchemaDrawn() {
        final TreeItem<DBObject> treeSelection = dbTreeView.getSelectionModel().getSelectedItem();
        boolean isSchemaSuccSelected = treeSelection != null &&
                (treeSelection.getValue() instanceof Schema
                        || treeSelection.getValue() instanceof Table);

        drawButton.setDisable(!isSchemaSuccSelected);
        uploadButon.setDisable(!(isSchemaSuccSelected));

        saveSchemaMenuItem.setDisable(false);
        exportSQLMenuItem.setDisable(false);
    }

    public void saveSchemaModelToFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save schema to file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Schema file", "*.schema"));
        File file = fileChooser.showSaveDialog(primaryStage);
        if (file != null) {
            try {
                final ModelPaneController modelPaneController = ControllerMediator.getModelPaneController();
                modelPaneController.getCurrentSchema().setName(file.getName().replaceFirst("\\.[^\\.]*$", ""));
                Utils.saveAsXML(modelPaneController.currentSchemaToXML(), file);
                currentPathLabel.setText(file.getPath());
            } catch (IOException e) {
                Utils.showError(e, "I/O error", "It was not possible to save schema model to the specified file.");
            }
        }
    }
}
