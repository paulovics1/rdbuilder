package rdbuilder.control;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.Window;
import rdbuilder.view.TaskExecutionPopup;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * This controller controls the popup indicating long-running task process. It's
 * associated with the {@link TaskExecutionPopup} class.
 */
public class TaskExecutionPopupController {
    @FXML public Label descriptionLabel;
    @FXML public VBox root;
    private ExecutorService executor;

    public static <V> V newCancellableTaskExecution(Callable<V> task, String description, Window owner) throws Exception {
        final TaskExecutionPopup taskExecutionPopup = new TaskExecutionPopup(owner, description, true);
        taskExecutionPopup.setOnCloseRequest((event) -> taskExecutionPopup.getController().close());
        return taskExecutionPopup.getController().execute(task);
    }

    @SuppressWarnings("WeakerAccess")
    public static <V> V  newTaskExecution(Callable<V> task, String description, Window owner) throws Exception {
        final TaskExecutionPopup taskExecutionPopup = new TaskExecutionPopup(owner, description, false);
        taskExecutionPopup.setOnCloseRequest(Event::consume);
        return taskExecutionPopup.getController().execute(task);
    }

    public void close() {
        executor.shutdownNow();
        ((Stage) root.getScene().getWindow()).close();
    }

    private <V> V execute(Callable<V> task) throws Exception {
        final Future<V> future = executor.submit(task);
        final Object[] result = {null};
        final Exception[] exception = {null};
        final TaskExecutionPopup taskExecutionPopup = (TaskExecutionPopup) root.getScene().getWindow();

        taskExecutionPopup.showingProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                executor.submit(() -> {
                    try {
                        result[0] = future.get();
                    } catch (Exception e) {
                        exception[0] = e;
                    }
                    taskExecutionPopup.setOnCloseRequest(null);
                    Platform.runLater(taskExecutionPopup::close);
                });
            }
        });
        taskExecutionPopup.showAndWait();
        if (exception[0] != null)
            throw exception[0];

        //noinspection unchecked
        return (V) result[0];
    }

    public void initialize() {
        executor = Executors.newSingleThreadExecutor();
    }
}
