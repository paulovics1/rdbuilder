package rdbuilder.control;

import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.util.Callback;
import javafx.util.StringConverter;
import rdbuilder.Utils;
import rdbuilder.dbobject.*;
import rdbuilder.view.TableDialog;

import java.util.*;

/**
 * This controller controls the table configuration dialog. It's
 * associated with the {@link TableDialog} class.
 */
public class TableDialogController {
    private final static int ICON_SIZE = 35;
    private final static Image PLUS_SIGN_RES = new Image("file:../../res/plus_sign.png");
    private final static Image MINUS_SIGN_RES = new Image("file:../../res/minus_sign.png");
    private final static Image UP_ARROW_RES = new Image("file:../../res/up_arrow.png");
    private final static Image DOWN_ARROW_RES = new Image("file:../../res/down_arrow.png");
    private final static int ROW_HEIGHT = 30;
    private final Table resultTable;
    private final HashMap<Column, Column> columnMap = new HashMap<>();
    private final HashMap<UniqueConstraint, UniqueConstraint> uniqueConstraintsMap = new HashMap<>();
    private final TableDialog tableDialog;
    private final ArrayList<UniqueConstraint> uniqueConstraints = new ArrayList<>();
    private final ArrayList<ArrayList<Node>> columnsRows = new ArrayList<>();
    private final ArrayList<ArrayList<Node>> uniqueConstraintsRows = new ArrayList<>();
    private final Table initialTable;
    private final StringConverter<Column> columnStringConverter = new StringConverter<Column>() {
        @Override
        public String toString(Column object) {
            if (object == null)
                return "";
            return object.toString();
        }

        @Override
        public Column fromString(String string) {
            return resultTable.getColumnByName(string);
        }
    };
    @FXML public ImageView addColumnImageView;
    @FXML public GridPane columnGridPane;
    @FXML public GridPane uniqueConstraintGridPane;
    @FXML public TabPane tabPane;
    @FXML public AnchorPane tab1AnchorPane;
    @FXML public TextField tableNameTextField;
    @FXML public VBox root;
    @FXML public Tab tabColumns;
    public TextField primaryKeyNameTextField;
    public ImageView addUniqueConstraintImageView;
    public Tab tabUniqueConstraints;
    public Label labelConstraintName;
    public boolean uniqueConstraintColumnChangeHandlerRunning = false;
    private TableDialogResult result;

    public class TableDialogResult {
        public final Table table;
        public final HashMap<Column, Column> columnMap;
        public final HashMap<UniqueConstraint, UniqueConstraint> uniqueConstraintsMap;

        TableDialogResult(Table table, HashMap<Column, Column> columnMap, HashMap<UniqueConstraint, UniqueConstraint> uniqueConstraintsMap) {
            this.table = table;
            this.columnMap = columnMap;
            this.uniqueConstraintsMap = uniqueConstraintsMap;
        }
    }

    public TableDialogController(TableDialog tableDialog, Table initialTable) {
        this.initialTable = initialTable;
        this.tableDialog = tableDialog;
        if (initialTable == null){
            resultTable = new Table();
            resultTable.addColumn();
        }
        else {
            resultTable = initialTable.getClone();
            final ArrayList<Column> iColumns = initialTable.getColumns(), rColumns = resultTable.getColumns();
            for (int i = 0; i < iColumns.size(); i++){
                columnMap.put(rColumns.get(i), iColumns.get(i));
            }

            final HashSet<UniqueConstraint> iUniqueConstraints = initialTable.getUniqueConstraints(),
                    rUniqueConstraints = resultTable.getUniqueConstraints();
            for (UniqueConstraint iUniqueConstraint : iUniqueConstraints){
                findConstraintPair:
                for (UniqueConstraint rUniqueConstraint : rUniqueConstraints){
                    if (iUniqueConstraint.getName().equals(rUniqueConstraint.getName())){
                        for (Column rColumn : rUniqueConstraint.getColumns()){
                            if (!iUniqueConstraint.getColumns().contains(columnMap.get(rColumn))){
                                continue findConstraintPair;
                            }
                        }
                        uniqueConstraintsMap.put(rUniqueConstraint, iUniqueConstraint);
                    }
                }
            }
        }
    }

    public void addColumn() {
        loadColumn(resultTable.addColumn());
    }

    private void addUniqueConstraint() {
        UniqueConstraint newConstraint = new UniqueConstraint();
        newConstraint.addColumn(resultTable.getColumns().get(0));
        resultTable.registerUniqueConstraint(newConstraint);
        loadUniqueConstraint(newConstraint);
    }

    private void addUniqueConstraintEmptyColumn(UniqueConstraint constraint) {
        ArrayList<Node> row = uniqueConstraintsRows.get(uniqueConstraints.indexOf(constraint));

        ChoiceBox<Column> nChoiceBox = newUniqueConstraintColumnChoiceBox(constraint);
        row.add(row.size()-1, nChoiceBox);

        nChoiceBox.valueProperty().addListener(getUniqueConstraintColumnChangeListener(constraint));

        uniqueConstraintGridPane.getChildren().add(nChoiceBox);
        GridPane.setRowIndex(nChoiceBox, uniqueConstraintsRows.indexOf(row)+1);
        GridPane.setColumnIndex(nChoiceBox, row.size()-2);
        GridPane.setColumnIndex(row.get(row.size()-1), row.size()-1);
    }

    public void apply() {
        if (!validate())
            return;

        result = new TableDialogResult(resultTable, columnMap, uniqueConstraintsMap);
        Platform.runLater(tableDialog::close);
    }

    public void cancel() {
        result = null;
        Platform.runLater(tableDialog::close);
    }

    private void checkImagesVisibility() {
        ObservableList<Node> children = columnGridPane.getChildren();
        children.stream().filter(node -> node instanceof ImageView).forEach(node -> {
            ImageView imageView = (ImageView) node;
            if (imageView.getImage() == MINUS_SIGN_RES) {
                imageView.setVisible(columnsRows.size() > 1);
            } else if (imageView.getImage() == UP_ARROW_RES) {
                imageView.setVisible(GridPane.getRowIndex(imageView) > 1);
            } else if (imageView.getImage() == DOWN_ARROW_RES) {
                imageView.setVisible(GridPane.getRowIndex(imageView) < columnsRows.size());
            }
        });
    }

    public TableDialogResult getResult() {
        return result;

    }

    private ChangeListener<Column> getUniqueConstraintColumnChangeListener(UniqueConstraint constraint) {
        ArrayList<Node> row = uniqueConstraintsRows.get(uniqueConstraints.indexOf(constraint));
        return (observable, oldValue, newValue) -> {
            if (uniqueConstraintColumnChangeHandlerRunning)
                return;

            uniqueConstraintColumnChangeHandlerRunning = true;

            constraint.getColumns().remove(oldValue);
            if (newValue != null)
                constraint.addColumn(newValue);

            ChoiceBox<Column> choiceBoxToRemove = null;
            for (int i = 1; i < row.size()-1; i++) {
                @SuppressWarnings("unchecked")
                ChoiceBox<Column> choiceBox = (ChoiceBox<Column>) row.get(i);
                Column originalValue = choiceBox.getValue();
                if (originalValue == null && i != row.size()-2){
                    choiceBoxToRemove = choiceBox;
                }
            }

            if (((ChoiceBox) row.get(row.size()-2)).getValue() != null) {
                addUniqueConstraintEmptyColumn(constraint);
            }

            if (choiceBoxToRemove != null){
                int index = row.indexOf(choiceBoxToRemove);
                row.remove(choiceBoxToRemove);
                uniqueConstraintGridPane.getChildren().remove(choiceBoxToRemove);
                for (int i = index; i < row.size(); i++){
                    Node child = row.get(i);
                    GridPane.setColumnIndex(child, GridPane.getColumnIndex(child)-1);
                }
            }

            if (constraint.getColumns().isEmpty()){
                removeUniqueConstraint(constraint);
            }
            uniqueConstraintColumnChangeHandlerRunning = false;
        };
    }

    /**
     * This method is automatically called upon construction of this controller.
     * Mostly objects that were not available during the constructor call are being initialized.
     */
    public void initialize() {
        tableNameTextField.setText(resultTable.getName());
        primaryKeyNameTextField.setText(resultTable.getPrimaryKey().getName());

        setUpImageView(addColumnImageView, PLUS_SIGN_RES);
        setUpImageView(addUniqueConstraintImageView, PLUS_SIGN_RES);

        ArrayList<Column> columns = resultTable.getColumns();
        columns.forEach(this::loadColumn);

        HashSet<UniqueConstraint> uniqueConstraints = resultTable.getUniqueConstraints();
        uniqueConstraints.forEach(this::loadUniqueConstraint);

        columnGridPane.heightProperty().addListener((observable, oldValue, newValue) -> {
            tabPane.setPrefHeight(tabPane.getHeight() - oldValue.doubleValue() + newValue.doubleValue());
            root.getScene().getWindow().sizeToScene();
        });

        tableNameTextField.textProperty().addListener((observable, oldValue, newValue) ->
                resultTable.setName(newValue));

        primaryKeyNameTextField.textProperty().addListener((observable, oldValue, newValue) ->
                resultTable.getPrimaryKey().setName(newValue));

        updateUniqueConstraintsColumns();
    }

    public void loadColumn(Column column) {
        GridPane.setRowIndex(addColumnImageView, columnsRows.size()+2);
        TextField nameTextField = new TextField(column.getName());
        TextField dataTypeTextField = new TextField(column.getDataType());

        HBox defaultHBox = new HBox();
        defaultHBox.setSpacing(5);
        defaultHBox.setAlignment(Pos.CENTER);
        TextField defaultTextField = new TextField(column.getDefaultValue());
        CheckBox defaultCheckBox = new CheckBox();
        defaultCheckBox.setSelected(!column.getDefaultValue().toUpperCase().equals("NULL"));

        CheckBox notNullCheckBox = new CheckBox();
        notNullCheckBox.setSelected(column.isNotNull());
        CheckBox primaryKeyCheckBox = new CheckBox();
        primaryKeyCheckBox.setSelected(column.isInPrimaryKey());
        ImageView upArrowImageView = new ImageView();
        ImageView downArrowImageView = new ImageView();
        ImageView deleteImageView = new ImageView();

        final ArrayList<Node> row = new ArrayList<>();
        row.add(nameTextField);
        row.add(dataTypeTextField);
        row.add(defaultHBox);
        row.add(notNullCheckBox);
        row.add(primaryKeyCheckBox);
        row.add(upArrowImageView);
        row.add(downArrowImageView);
        row.add(deleteImageView);

        setUpImageView(deleteImageView, MINUS_SIGN_RES);
        setUpImageView(upArrowImageView, UP_ARROW_RES);
        setUpImageView(downArrowImageView, DOWN_ARROW_RES);

        nameTextField.textProperty().addListener((observable, oldValue, newValue) ->
                column.setName(newValue)
        );

        dataTypeTextField.textProperty().addListener((observable, oldValue, newValue) ->
                column.setDataType(newValue)
        );

        defaultCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                column.setDefaultValue(defaultTextField.getText());
            }
            else {
                column.setDefaultValue("NULL");
            }
        });

        defaultTextField.disableProperty().bind(defaultCheckBox.selectedProperty().not());

        defaultTextField.textProperty().addListener((observable, oldValue, newValue) ->
                column.setDefaultValue(newValue)
        );

        notNullCheckBox.disableProperty().bind(primaryKeyCheckBox.selectedProperty());
        notNullCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> column.setNotNull(newValue));

        deleteImageView.setOnMouseClicked(event -> {
            removeColumn(column);
        });

        primaryKeyCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue){
                resultTable.getPrimaryKey().addColumn(column);
            }
            else {
                resultTable.getPrimaryKey().removeColumn(column);
            }
        });

        upArrowImageView.setOnMouseClicked(event -> {
            final int upShiftedRowIndex = GridPane.getRowIndex(upArrowImageView);
            final int upShiftedTableColumnIndex = rowToTableColumnIndex(upShiftedRowIndex);
            final Column upShiftedColumn = resultTable.getColumns().remove(upShiftedTableColumnIndex);

            resultTable.getColumns().add(upShiftedTableColumnIndex-1, upShiftedColumn);

            for (Node child : columnsRows.get(upShiftedRowIndex - 1)){
                GridPane.setRowIndex(child, upShiftedRowIndex - 1);
            }

            for (Node child : columnsRows.get(upShiftedRowIndex - 2)){
                GridPane.setRowIndex(child, upShiftedRowIndex);
            }
            Collections.swap(columnsRows, upShiftedRowIndex - 1, upShiftedRowIndex - 2);
            checkImagesVisibility();
        });

        downArrowImageView.setOnMouseClicked(event -> {
            final int downShiftedRowIndex = GridPane.getRowIndex(downArrowImageView); // GridPane index
            final int downShiftedTableColumnIndex = rowToTableColumnIndex(downShiftedRowIndex);
            final Column downShiftedColumn = resultTable.getColumns().remove(downShiftedTableColumnIndex);

            resultTable.getColumns().add(downShiftedTableColumnIndex+1, downShiftedColumn);

            for (Node child : columnsRows.get(downShiftedRowIndex - 1)){
                GridPane.setRowIndex(child, downShiftedRowIndex + 1);
            }

            for (Node child : columnsRows.get(downShiftedRowIndex)){
                GridPane.setRowIndex(child, downShiftedRowIndex);
            }
            Collections.swap(columnsRows, downShiftedRowIndex - 1, downShiftedRowIndex);
            checkImagesVisibility();
        });

        defaultHBox.getChildren().addAll(defaultTextField, defaultCheckBox);
        columnGridPane.addRow(columnsRows.size()+1, row.toArray(new Node[]{}));
        columnsRows.add(row);
        RowConstraints rowConstraints = new RowConstraints();
        setUpRowConstraints(rowConstraints);
        columnGridPane.getRowConstraints().add(new RowConstraints());

        checkImagesVisibility();
    }

    private void loadUniqueConstraint(UniqueConstraint constraint) {
        GridPane.setRowIndex(addUniqueConstraintImageView, uniqueConstraintsRows.size()+2);

        final ArrayList<Node> row = new ArrayList<>();
        uniqueConstraints.add(constraint);
        uniqueConstraintsRows.add(row);

        TextField nameTextField = new TextField(constraint.getName());

        ChangeListener<Column> columnChangeListener = getUniqueConstraintColumnChangeListener(constraint);

        @SuppressWarnings("unchecked")
        ChoiceBox<Column>[] columnNamesBoxes = new ChoiceBox[constraint.getColumns().size()];
        for (int i = 0; i < columnNamesBoxes.length; i++) {
            columnNamesBoxes[i] = newUniqueConstraintColumnChoiceBox(constraint);
            Column column = constraint.getColumn(i);
            columnNamesBoxes[i].getItems().add(column);
            columnNamesBoxes[i].setValue(column);
            columnNamesBoxes[i].valueProperty().addListener(columnChangeListener);
        }

        ImageView deleteImageView = new ImageView();
        setUpImageView(deleteImageView, MINUS_SIGN_RES);

        row.add(nameTextField);
        row.addAll(Arrays.asList(columnNamesBoxes));
        row.add(deleteImageView);

        nameTextField.textProperty().addListener((observable, oldValue, newValue) ->
                    constraint.setName(newValue)
        );

        deleteImageView.setOnMouseClicked(event -> {
            removeUniqueConstraint(constraint);
        });

        uniqueConstraintGridPane.addRow(uniqueConstraintsRows.size(), row.toArray(new Node[]{}));

        RowConstraints rowConstraints = new RowConstraints();
        setUpRowConstraints(rowConstraints);
        uniqueConstraintGridPane.getRowConstraints().add(new RowConstraints());

        addUniqueConstraintEmptyColumn(constraint);
    }

    private ChoiceBox<Column> newUniqueConstraintColumnChoiceBox(UniqueConstraint constraint){
        ChoiceBox<Column> nChoiceBox = new ChoiceBox<Column>(FXCollections.observableArrayList(new Callback<Column, Observable[]>() {
            @Override
            public Observable[] call(Column param) {
                return new Observable[] {param.nameProperty()};
            }
        }));
        nChoiceBox.setConverter(columnStringConverter);
        nChoiceBox.showingProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue){
                if (uniqueConstraintColumnChangeHandlerRunning)
                    return;
                uniqueConstraintColumnChangeHandlerRunning = true;
                ArrayList<Column> choices = new ArrayList<>();
                choices.addAll(resultTable.getColumns());
                choices.removeAll(constraint.getColumns());
                Column columnChosen = nChoiceBox.getValue();
                if (columnChosen != null) {
                    choices.add(columnChosen);
                }
                Collections.sort(choices, DBObject.getComparator());

                nChoiceBox.getItems().clear();
                nChoiceBox.getItems().add(null);
                nChoiceBox.getItems().addAll(choices);
                nChoiceBox.setValue(columnChosen);
                nChoiceBox.show();
                uniqueConstraintColumnChangeHandlerRunning = false;
            }
        });
        return nChoiceBox;
    }

    public void onAddColumnClicked(Event event){
        addColumn();
    }

    public void onAddUniqueConstraintClicked(Event event){
        addUniqueConstraint();
    }

    private void removeColumn(Column column) {
        final int deletedColumnIndex = resultTable.getColumns().indexOf(column);
        final int deletedRowIndex = deletedColumnIndex+1;

        resultTable.getColumns().remove(column);
        columnMap.remove(column);
        ArrayList<Node> deletedRow = columnsRows.remove(deletedColumnIndex);
        columnGridPane.getChildren().removeAll(deletedRow);

        for (int i = deletedColumnIndex; i < columnsRows.size(); i++){
            for (Node child : columnsRows.get(i))
                GridPane.setRowIndex(child, i+1);
        }
        GridPane.setRowIndex(addColumnImageView, columnsRows.size()+1);

        columnGridPane.getRowConstraints().remove(columnGridPane.getRowConstraints().size()-1);
        columnGridPane.setMaxHeight(ROW_HEIGHT * (columnsRows.size()+2));
        checkImagesVisibility();

        for (int i = 0; i < uniqueConstraints.size(); i++) {
            UniqueConstraint uniqueConstraint = uniqueConstraints.get(i);
            if (uniqueConstraint.getColumns().remove(column)){
                ArrayList<Node> nodes = uniqueConstraintsRows.get(i);
                for (int j = 1; j < nodes.size()-1; j++){
                    @SuppressWarnings("unchecked")
                    ChoiceBox<Column> choiceBox = (ChoiceBox<Column>) nodes.get(j);
                    if (choiceBox.getValue() == column){
                        choiceBox.setValue(null);
                    }
                }
            }
        }
    }

    private void removeUniqueConstraint(UniqueConstraint constraint) {
        final int deletedUniqueConstraintIndex = uniqueConstraints.indexOf(constraint);
        ArrayList<Node> row = uniqueConstraintsRows.get(deletedUniqueConstraintIndex);

        final int deletedRowIndex = GridPane.getRowIndex(row.get(0));

        resultTable.getUniqueConstraints().remove(constraint);
        uniqueConstraintsMap.remove(constraint);
        uniqueConstraints.remove(constraint);
        uniqueConstraintsRows.remove(row);

        uniqueConstraintGridPane.getChildren().removeAll(row);
        for (Node child : uniqueConstraintGridPane.getChildren()) {
            Integer rowIndex = GridPane.getRowIndex(child);
            if (child.isManaged() && rowIndex != null && rowIndex > deletedRowIndex) {
                GridPane.setRowIndex(child, rowIndex-1);
            }
        }

        uniqueConstraintGridPane.getRowConstraints().remove(uniqueConstraintGridPane.getRowConstraints().size()-1);
        uniqueConstraintGridPane.setMaxHeight(ROW_HEIGHT * (uniqueConstraintsRows.size()+2));

        updateUniqueConstraintsTabSize();
    }

    private int rowToTableColumnIndex(int rowIndex){
        return rowIndex-1;
    }

    private int rowToUniqueConstraintIndex(int rowIndex){
        return rowIndex-1;
    }

    private void setUpImageView(ImageView imageView, Image imageToSet){
        imageView.setPreserveRatio(true);
        imageView.setFitHeight(ICON_SIZE);
        imageView.setFitWidth(ICON_SIZE);
        if (imageToSet != null) {
            imageView.setImage(imageToSet);
        }
        imageView.setCursor(Cursor.HAND);
    }

    private void setUpRowConstraints(RowConstraints rowConstraints){
        rowConstraints.setPrefHeight(ROW_HEIGHT);
        rowConstraints.setMinHeight(ROW_HEIGHT);
        rowConstraints.setMaxHeight(ROW_HEIGHT);
        rowConstraints.setValignment(VPos.CENTER);
        rowConstraints.setVgrow(Priority.NEVER);
        rowConstraints.setFillHeight(false);
    }

    private void updateUniqueConstraintsColumns() {

    }

    private void updateUniqueConstraintsTabSize() {

    }

    public boolean validate(){
        String errorMessage = null;

        final Schema schema = ControllerMediator.getModelPaneController().getCurrentSchema();
        final Table tableWithThisName = schema.getChildByName(resultTable.getName());

        if (resultTable.getName().equals("")){
            errorMessage = "The table must have a non-empty name!";
        }
        else if (tableWithThisName != null && tableWithThisName != initialTable){
            errorMessage = String.format("Another table named \"%s\" already exists!", resultTable.getName());
        }
        else if (!resultTable.getPrimaryKey().getColumns().isEmpty() && resultTable.getPrimaryKey().getName().equals("")){
            errorMessage = "The primary key must have a non-empty name!";
        }
        else {
            {   HashSet<String> columnNames = new HashSet<>();
                final ArrayList<Column> columns = resultTable.getColumns();
                final int size = columns.size();
                for (int i = 0; i < size; i++) {
                    final Column column = columns.get(i);
                    final String name = column.getName();
                    if (name.equals("")) {
                        errorMessage = String.format("The %s column has an empty name!", Utils.getEnglishPosition(i+1));
                        break;
                    }
                    if (!columnNames.add(name.toLowerCase())) {
                        errorMessage = String.format("Two or more columns are named \"%s\", column names must be unique!", name);
                        break;
                    }
                    if (column.getDataType().equals("")) {
                        errorMessage = String.format("The column named \"%s\" has an empty data type!", name);
                        break;
                    }
                }
            }

            if (errorMessage == null){
                HashSet<String> uniqueConstraintNames = new HashSet<>();
                for (UniqueConstraint constraint : resultTable.getUniqueConstraints()){
                    final String name = constraint.getName();
                    if (name.equals("")){
                        errorMessage = "All unique constraints must have a non-empty name!";
                        break;
                    }
                    if (!uniqueConstraintNames.add(name.toLowerCase())){
                        errorMessage = String.format("Two or more unique constraints are named \"%s\", unique constraint names must be unique!", name);
                        break;
                    }
                }
            }
        }

        final boolean error = errorMessage != null;

        if (error){
            Alert alert = new Alert(Alert.AlertType.WARNING, "", ButtonType.OK);
            alert.setHeaderText("Table could not be saved");
            alert.setHeaderText("One or more fields are not filled correctly.");
            alert.setContentText(errorMessage);
            alert.show();
        }

        return !error;
    }
}
