package rdbuilder.control;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.StageStyle;
import rdbuilder.Utils;
import rdbuilder.dbconnector.DBConnector;
import rdbuilder.dbconnector.DBConnectorFactory;
import rdbuilder.dbconnector.GenericDBConnector;
import rdbuilder.view.ConnectionDialog;

import static rdbuilder.Main.dbConnectorFactories;

/**
 * This controller controls the database connection configuration dialog. It's
 * associated with the {@link ConnectionDialog} class.
 */
public class ConnectionDialogController {
    private static DBConnectorFactoryHolder[] dbConnectorFactoryHolders;
    @FXML private ChoiceBox<DBConnectorFactoryHolder> dbConnectorChoiceBox;
    @FXML private CheckBox storePasswordCheckBox;
    @FXML private TextField nameTextField;
    @FXML private TextField hostTextField;
    @FXML private TextField portTextField;
    @FXML private ComboBox<String> databaseComboBox;
    @FXML private TextField usernameTextField;
    @FXML private PasswordField passwordField;
    private ConnectionDialog connectionDialog;
    private Node saveButton;
    private Node testButton;

    /**
     * This holder of {@link DBConnectorFactory} class returns the database system
     * name in it's {@link #toString()} method.
     */
    private static class DBConnectorFactoryHolder {
        DBConnectorFactory dbConnectorFactory;

        private DBConnectorFactoryHolder(DBConnectorFactory dbConnectorFactory) {
            this.dbConnectorFactory = dbConnectorFactory;
        }

        @Override
        public String toString() {
            return dbConnectorFactory.DBMS_PROPERTIES.NAME;
        }
    }

    public ConnectionDialogController(ConnectionDialog connectionDialog) {
        this.connectionDialog = connectionDialog;
    }

    /**
     * Create a database connector using {@link #prepareNewDBConnector()} and invoke
     * it's {@link DBConnector#connect()} method.
     * @return the original database, if the connection was successful. Null otherwise.
     */
    private DBConnector createConnectedDBConnector() {
        DBConnector dbConnector = prepareNewDBConnector();
        boolean connectionSuccesfull = Utils.connectDBConnectorTask(dbConnector,
                connectionDialog.getDialogPane().getScene().getWindow());
        if (connectionSuccesfull) {
            return dbConnector;
        }
        return null;
    }

    /**
     * This method is automatically called upon construction of this controller.
     * Mostly objects that were not available during the constructor call are being initialized.
     */
    public void initialize() {
        dbConnectorFactoryHolders = new DBConnectorFactoryHolder[dbConnectorFactories.size()];
        for (int i = 0; i < dbConnectorFactories.size(); i++) {
            dbConnectorFactoryHolders[i] = new DBConnectorFactoryHolder(dbConnectorFactories.get(i));
        }

        testButton = connectionDialog.getDialogPane().lookupButton(ConnectionDialog.TEST_BTN_TYPE);
        saveButton = connectionDialog.getDialogPane().lookupButton(ConnectionDialog.SAVE_BTN_TYPE);
        testButton.disableProperty().bind(dbConnectorChoiceBox.valueProperty().isNull());
        saveButton.disableProperty().bind(dbConnectorChoiceBox.valueProperty().isNull());

        testButton.setOnMouseClicked(event -> {
            if (createConnectedDBConnector() != null) {
                Alert informationDialog = new Alert(Alert.AlertType.INFORMATION);
                informationDialog.initStyle(StageStyle.UTILITY);
                informationDialog.setTitle("Success");
                informationDialog.setHeaderText("Connection has been succesfully established.");
                informationDialog.setContentText("Now you can save this connection. It will be automatically loaded every time you launch this application.");
                informationDialog.show();
            }
        });

        dbConnectorChoiceBox.getItems().addAll(dbConnectorFactoryHolders);
        dbConnectorChoiceBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            databaseComboBox.getItems().clear();
            String[] defaultDatabases = newValue.dbConnectorFactory.getDefaultDatabases();
            databaseComboBox.getItems().addAll(defaultDatabases);
            if (portTextField.getText().isEmpty() ||
                    (oldValue != null &&
                    portTextField.getText().equals(oldValue.dbConnectorFactory.getDefaultPort())))
                portTextField.setText(newValue.dbConnectorFactory.getDefaultPort());
        });

        connectionDialog.setResultConverter(button -> {
            if (button == ConnectionDialog.SAVE_BTN_TYPE) {
                return prepareNewDBConnector();
            }
            return null;
        });
        connectionDialog.setOnCloseRequest(event -> {
            if (connectionDialog.getResult() == null) {
                event.consume();
            }
        });
    }

    /**
     * Loads the associated dialog fields with database connection information.
     * @param dbConnector provides database connection information
     */
    public void loadDBConnectors(GenericDBConnector dbConnector) {
        nameTextField.setText(dbConnector.getDBObjectsRoot().getName());
        for (DBConnectorFactoryHolder dbConnectorFactoryHolder : dbConnectorFactoryHolders){
            if (dbConnectorFactoryHolder.dbConnectorFactory.getDBConnectorClass() == dbConnector.getClass()){
                dbConnectorChoiceBox.setValue(dbConnectorFactoryHolder);
                break;
            }
        }
        hostTextField.setText(dbConnector.HOST);
        portTextField.setText(dbConnector.PORT);
        databaseComboBox.setValue(dbConnector.getInitialDatabaseName());
        usernameTextField.setText(dbConnector.getUsername());
        passwordField.setText(dbConnector.getPassword());
    }

    /**
     * Creates a new instance of appropriate class extending {@link DBConnector} filling it
     * with information from dialog fields.
     * @return
     */
    private DBConnector prepareNewDBConnector() {
        DBConnectorFactoryHolder dbConnectorFactoryHolder = dbConnectorChoiceBox.getValue();
        if (dbConnectorFactoryHolder == null) {
            return null;
        }
        DBConnectorFactory dbConnectorFactory = dbConnectorFactoryHolder.dbConnectorFactory;
        final String name = nameTextField.getText();
        final String host = hostTextField.getText();
        final String port = portTextField.getText();
        final String database = databaseComboBox.getValue();
        final String username = usernameTextField.getText();
        final String password = passwordField.getText();

        DBConnector dbConnector = dbConnectorFactory.newDBConnectorInstance(host, port, database, username, password);

        if (!name.isEmpty())
            dbConnector.getDBObjectsRoot().setName(name);
        return dbConnector;
    }
}
