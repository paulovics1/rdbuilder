package rdbuilder.control;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Pair;
import rdbuilder.dbobject.*;
import rdbuilder.view.ForeignKeyDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This controller controls the foreign key configuration dialog. It's
 * associated with the {@link ForeignKeyDialog} class.
 */
public class ForeignKeyDialogController {
    private final static int COLUMN_WIDTH = 120;
    private final ForeignKey resultForeignKey;
    private final ForeignKey initialForeignKey;
    private final ForeignKeyDialog foreignKeyDialog;
    private final ArrayList<ChoiceBox<Column>> columnsRow = new ArrayList<>();
    private final ArrayList<ChoiceBox<Column>> referencedColumnsRow = new ArrayList<>();
    private final Map<ChoiceBox<Column>, Column> dummyColumnsMap = new HashMap<>();
    @FXML public GridPane gridPane;
    @FXML public TextField foreignKeyNameTextField;
    @FXML public VBox root;
    public boolean columnChangeHandlerRunning = false;
    public boolean referencedColumnChangeHandlerRunning = false;
    public ChoiceBox<Table> tableChoiceBox;
    public ChoiceBox<Table> referencedTableChoiceBox;
    private ForeignKey result;

    public ForeignKeyDialogController(ForeignKeyDialog foreignKeyDialog, ForeignKey initialForeignKey) {
        this.foreignKeyDialog = foreignKeyDialog;
        if (initialForeignKey == null){
            resultForeignKey = new ForeignKey();
        }
        else {
            resultForeignKey = initialForeignKey.getOuterClone();
        }
        this.initialForeignKey = initialForeignKey;
    }

    /**
     * Adds an empty pair of choiceboxes and fills them with columns to choose from.
     * @return {@link Pair} of added choiceboxes
     */
    private Pair<ChoiceBox<Column>, ChoiceBox<Column>> addEmptyColumnPair() {
        if (!columnsRow.isEmpty() && !referencedColumnsRow.isEmpty()){
            ChoiceBox<Column> choiceBox1 = columnsRow.get(columnsRow.size() - 1);
            ChoiceBox<Column> choiceBox2 = referencedColumnsRow.get(referencedColumnsRow.size() - 1);
            if (choiceBox1.getValue().getParent() == null &&
                choiceBox2.getValue().getParent() == null){
            return new Pair<>(choiceBox1, choiceBox2);
        }
        }
        ChoiceBox<Column> nChoiceBox1 = new ChoiceBox<>();
        columnsRow.add(nChoiceBox1);

        Column dummyColumn1 = getDummyColumn(nChoiceBox1);
        ArrayList<Column> choices1 = new ArrayList<>();
        if (resultForeignKey.getTable() != null) {
            choices1.addAll(resultForeignKey.getTable().getColumns());
        }
        choices1.removeAll(resultForeignKey.getColumns());
        Collections.sort(choices1, DBObject.getComparator());

        nChoiceBox1.getItems().add(dummyColumn1);
        nChoiceBox1.getItems().addAll(choices1);
        nChoiceBox1.setValue(dummyColumn1);

        nChoiceBox1.valueProperty().addListener(getColumnChangeListener(false));

        gridPane.getChildren().add(nChoiceBox1);
        GridPane.setRowIndex(nChoiceBox1, 0);
        GridPane.setColumnIndex(nChoiceBox1, columnsRow.size()+1);

        ChoiceBox<Column> nChoiceBox2 = new ChoiceBox<>();
        referencedColumnsRow.add(nChoiceBox2);

        Column dummyColumn2 = getDummyColumn(nChoiceBox2);
        ArrayList<Column> choices2 = new ArrayList<>();
        if (resultForeignKey.getReferencedTable() != null) {
            choices2.addAll(resultForeignKey.getReferencedTable().getColumns());
        }
        choices2.removeAll(resultForeignKey.getReferencedColumns());
        Collections.sort(choices2, DBObject.getComparator());

        nChoiceBox2.getItems().add(dummyColumn2);
        nChoiceBox2.getItems().addAll(choices2);
        nChoiceBox2.setValue(dummyColumn2);

        nChoiceBox2.valueProperty().addListener(getColumnChangeListener(true));

        gridPane.getChildren().add(nChoiceBox2);
        GridPane.setRowIndex(nChoiceBox2, 1);
        GridPane.setColumnIndex(nChoiceBox2, referencedColumnsRow.size()+1);

        ColumnConstraints columnConstraints = new ColumnConstraints();
        setUpColumnConstraints(columnConstraints);
        gridPane.getColumnConstraints().add(columnConstraints);

        resultForeignKey.getColumns().add(dummyColumn1);
        resultForeignKey.getReferencedColumns().add(dummyColumn2);
        resultForeignKey.registerObserved(dummyColumn1);
        resultForeignKey.registerObserved(dummyColumn2);
        return new Pair<>(nChoiceBox1, nChoiceBox2);
    }

    /**
     * If all fields are filled correctly, closes the dialog.
     * @see #validate()
     */
    public void apply() {
        ArrayList<Column> referencedColumns = resultForeignKey.getReferencedColumns();
        ArrayList<Column> columns = (ArrayList<Column>) resultForeignKey.getColumns();
        for (int i = 0; i < resultForeignKey.getColumns().size(); i++) {
            if (columns.get(i).getParent() == null ||
                    referencedColumns.get(i).getParent() == null){
                resultForeignKey.removeColumnPair(i);
                i--;
            }
        }
        if (!validate())
            return;
        result = resultForeignKey;
        Platform.runLater(foreignKeyDialog::close);
    }


    /**
     * Discards all changes made and closes the dialog.
     */
    public void cancel() {
        result = null;
        Platform.runLater(foreignKeyDialog::close);
    }

    /**
     * The change listener is invoked, when a {@link javafx.scene.control.Control Control}
     * ({@link ChoiceBox}) changes it's value. Updates the model, adds empty choiceboxes if
     * necessary and removes unnecessary choiceboxes.
     * @param referenced true if a choicebox represents the choice of a referenced column, false otherwise
     */
    private ChangeListener<Column> getColumnChangeListener(boolean referenced) {
        return (observable, oldValue, newValue) -> {
            ArrayList<ChoiceBox<Column>> row = referenced ? referencedColumnsRow : columnsRow;
            ArrayList<ChoiceBox<Column>> otherRow = !referenced ? referencedColumnsRow : columnsRow;
            ArrayList<Column> columns = referenced ?
                    resultForeignKey.getReferencedColumns() :
                    (ArrayList<Column>) resultForeignKey.getColumns();
            Table table = referenced ? resultForeignKey.getReferencedTable() : resultForeignKey.getTable();

            if (referenced) {
                if (referencedColumnChangeHandlerRunning)
                    return;
                else
                    referencedColumnChangeHandlerRunning = true;
            } else {
                if (columnChangeHandlerRunning)
                    return;
                else
                    columnChangeHandlerRunning = true;
            }

            int columnIndex = columns.indexOf(oldValue);
            if (columnIndex != -1) {
                columns.remove(columnIndex);
                resultForeignKey.unregisterObserved(oldValue);
            } else {
                columnIndex = columns.size();
            }
            columns.add(columnIndex, newValue);
            resultForeignKey.registerObserved(newValue);

            Integer toRemoveIndex = null;
            for (int i = 0; i < row.size(); i++) {
                @SuppressWarnings("unchecked")
                ChoiceBox<Column> choiceBox = row.get(i);
                Column originalValue = choiceBox.getValue();
                ArrayList<Column> choices = new ArrayList<>();
                choices.addAll(table.getColumns());
                choices.add(getDummyColumn(choiceBox));
                choices.removeAll(columns);
                if (originalValue.getParent() != null) {
                    choices.add(originalValue);
                }
                else if (i != row.size()-1 && otherRow.get(i).getValue().getParent() == null){
                    toRemoveIndex = i;
                }
                Collections.sort(choices, DBObject.getComparator());

                choiceBox.getItems().clear();
                choiceBox.getItems().addAll(choices);
                choiceBox.setValue(originalValue);
            }

            if ((row.get(row.size()-1)).getValue().getParent() != null) {
                addEmptyColumnPair();
            }

            if (toRemoveIndex != null){
                ChoiceBox<Column> removedChoiceBox1 = row.remove((int) toRemoveIndex);
                ChoiceBox<Column> removedChoiceBox2 = otherRow.remove((int) toRemoveIndex);
                gridPane.getChildren().remove(removedChoiceBox1);
                gridPane.getChildren().remove(removedChoiceBox2);
                resultForeignKey.removeColumnPair(toRemoveIndex);
                for (int i = toRemoveIndex; i < row.size(); i++){
                    GridPane.setColumnIndex(row.get(i), i+2);
                    GridPane.setColumnIndex(otherRow.get(i), i+2);
                }
                gridPane.getColumnConstraints().remove(1);
            }

            if (referenced)
                referencedColumnChangeHandlerRunning = false;
            else
                columnChangeHandlerRunning = false;

        };
    }

    /**
     * @param choiceBox choicebox which the dummy column is required for
     * @return an empty column to represent empty choice
     */
    private Column getDummyColumn(ChoiceBox<Column> choiceBox){
        Column result = dummyColumnsMap.get(choiceBox);
        if (result == null){
            result = new Column(null, "", null, false, null);
        }
        return result;
    }

    public ForeignKey getResult() {
        return result;
    }

    /**
     * This method is automatically called upon construction of this controller.
     * Mostly objects that were not available during the constructor call are being initialized.
     */
    public void initialize() {
        foreignKeyNameTextField.setText(resultForeignKey.getName());

        if (initialForeignKey != null) {
            ArrayList<Column> columns = (ArrayList<Column>) initialForeignKey.getColumns();
            ArrayList<Column> referencedColumns = initialForeignKey.getReferencedColumns();

            int columnsSize = columns.size();
            for (int i = 0; i < columnsSize; i++) {
                loadColumnPair(new Pair<>(columns.get(i), referencedColumns.get(i)));
            }
        }

        Schema schema = ControllerMediator.getModelPaneController().getCurrentSchema();
        ArrayList<Table> tables = new ArrayList<>();
        tables.addAll(schema.getSubTree());
        tableChoiceBox.getItems().addAll(tables);
        if (initialForeignKey != null)
            tableChoiceBox.setValue(initialForeignKey.getTable());

        tableChoiceBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            resultForeignKey.setTable(newValue);
            for (ChoiceBox<Column> choiceBox : columnsRow) {
                choiceBox.setValue(getDummyColumn(choiceBox));
            }
        }
        );

        ArrayList<Table> referencedTables = new ArrayList<>();
        referencedTables.addAll(schema.getSubTree());
        referencedTableChoiceBox.getItems().addAll(referencedTables);
        if (initialForeignKey != null)
            referencedTableChoiceBox.setValue(initialForeignKey.getReferencedTable());

        referencedTableChoiceBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            resultForeignKey.setReferencedTable(newValue);
                    for (ChoiceBox<Column> choiceBox : referencedColumnsRow) {
                        choiceBox.setValue(getDummyColumn(choiceBox));
                    }
                }
        );

        gridPane.widthProperty().addListener((observable, oldValue, newValue) -> {
            root.setPrefWidth(newValue.doubleValue());
            root.setMaxWidth(newValue.doubleValue());
            root.getScene().getWindow().sizeToScene();
        });

        foreignKeyNameTextField.textProperty().addListener((observable, oldValue, newValue) ->
                resultForeignKey.setName(newValue));

        addEmptyColumnPair();
    }

    /**
     * Creates a new pair of choiceboxes and sets their value to columns in parameter
     * @param columnPair pair of values for the new choiceboxes
     */
    private void loadColumnPair(Pair<Column, Column> columnPair) {
        Pair<ChoiceBox<Column>, ChoiceBox<Column>> nChoiceBoxPair = addEmptyColumnPair();
        nChoiceBoxPair.getKey().setValue(columnPair.getKey());
        nChoiceBoxPair.getValue().setValue(columnPair.getValue());
    }

    /**
     * {@link ColumnConstraints} attribute setup.
     */
    private void setUpColumnConstraints(ColumnConstraints columnConstraints){
        columnConstraints.setMinWidth(COLUMN_WIDTH);
        columnConstraints.setPrefWidth(COLUMN_WIDTH);
        columnConstraints.setHgrow(Priority.ALWAYS);
        columnConstraints.setFillWidth(true);
    }

    /**
     * Checks if all the required fields are filled properly.
     * @return validation result
     */
    public boolean validate(){
        String errorMessage = null;

        final Schema schema = ControllerMediator.getModelPaneController().getCurrentSchema();
        final ForeignKey foreignKeyWithThisName = schema.getForeignKeyByName(resultForeignKey.getName());

        if (resultForeignKey.getName().equals("")){
            errorMessage = "The foreign key must have a non-empty name!";
        }
        else if (foreignKeyWithThisName != null && foreignKeyWithThisName != initialForeignKey){
            errorMessage = String.format("Another foreign key named \"%s\" already exists!", resultForeignKey.getName());
        }

        final boolean error = errorMessage != null;

        if (error){
            Alert alert = new Alert(Alert.AlertType.WARNING, "", ButtonType.OK);
            alert.setHeaderText("Foreign key could not be saved");
            alert.setHeaderText("One or more fields are not filled correctly.");
            alert.setContentText(errorMessage);
            alert.show();
        }

        return !error;
    }

}
