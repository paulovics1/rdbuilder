package rdbuilder.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import rdbuilder.Utils;
import rdbuilder.control.ConnectionDialogController;
import rdbuilder.dbconnector.DBConnector;
import rdbuilder.dbconnector.GenericDBConnector;

import java.io.IOException;
import java.util.NoSuchElementException;

public class ConnectionDialog extends Dialog<DBConnector> {
    public static final ButtonType TEST_BTN_TYPE = new ButtonType("Test", ButtonBar.ButtonData.RIGHT);
    public static final ButtonType SAVE_BTN_TYPE = new ButtonType("Save", ButtonBar.ButtonData.RIGHT);
    private static final String title = "Database connection dialog";
    private final ConnectionDialogController controller;

    public ConnectionDialog(Window owner) {
        super();
        initModality(Modality.APPLICATION_MODAL);
        initStyle(StageStyle.UTILITY);
        initOwner(owner);
        setTitle(title);

        DialogPane dialogPane = getDialogPane();
        dialogPane.getButtonTypes().addAll(TEST_BTN_TYPE, SAVE_BTN_TYPE, ButtonType.CLOSE);
        ((Button)dialogPane.lookupButton(SAVE_BTN_TYPE)).setDefaultButton(true);
        Node closeButton = dialogPane.lookupButton(ButtonType.CLOSE);
        closeButton.managedProperty().bind(closeButton.visibleProperty());
        closeButton.setVisible(false);

        controller = new ConnectionDialogController(this);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("connectionDialog.fxml"));
        fxmlLoader.setController(controller);
        try {
            dialogPane.setContent(fxmlLoader.load());
        } catch (IOException e) {
            Utils.showError(e, "Layout error", "An error has occured during rendering the connection dialog layout.");
            setResult(null);
        }
    }

    public ConnectionDialog(Window owner, GenericDBConnector dbConnector) {
        this(owner);
        try {
            controller.loadDBConnectors(dbConnector);
        }
        catch (NullPointerException ignore) {}
    }

    public DBConnector showAndGetDBConnector() {
        try {
            return showAndWait().get();
        } catch (NoSuchElementException e) {
            return null;
        }
    }
}
