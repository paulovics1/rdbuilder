package rdbuilder.view.modeling;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import rdbuilder.control.ModelPaneController;
import rdbuilder.dbobject.DBObject;
import rdbuilder.dbobject.DBObjectObserver;

import java.util.Collection;
import java.util.HashSet;

public abstract class DBObjectView implements DBObjectObserver {
    protected static final EventHandler<MouseEvent> EVENT_HANDLER_CONSUME = Event::consume;
    protected static final Color SELECTED_COLOR = Color.rgb(255, 70, 60);
    protected static final Color SELECTED_COLOR_ALT = SELECTED_COLOR.deriveColor(0, 1, 0.9, 1);
    protected final DBObject dbObject;
    final ModelPaneController parentPaneController;
    final Pane parentPane;
    private final HashSet<DBObject> observedDBObjects = new HashSet<>();
    double dragDeltaX = 0;
    double dragDeltaY = 0;
    private boolean selected;

    public DBObjectView(DBObject dbObject, ModelPaneController parentPaneController) {
        this.dbObject = dbObject;
        registerObserved(dbObject);
        this.parentPaneController = parentPaneController;
        this.parentPane = parentPaneController.dbModelPane;
    }

        @Override
    public Collection<DBObject> getObservedDBObjects() {
        return observedDBObjects;
    };

    @Override
    public void onObservedDestroyed(DBObject observed) {
        destroy();
    }

public void destroy() {
        unregisterObserved(dbObject);
        parentPaneController.unregisterDBObjectView(this);
    }

    public abstract void drag(MouseEvent event);

    public DBObject getDBObject() {
        return dbObject;
    }

    public double getParentX(MouseEvent event){
        return event.getSceneX()- parentPane.localToScene(parentPane.getBoundsInLocal()).getMinX();
    }

    public double getParentY(MouseEvent event){
        return event.getSceneY()- parentPane.localToScene(parentPane.getBoundsInLocal()).getMinY();
    }

    public Scene getScene(){
            return parentPaneController.dbModelPane.getScene();
        }

    public abstract void initializeDeltaPosition(MouseEvent event);

    public boolean isSelected() {
        return selected;
    }

    public final void setSelected(boolean selected){
        if (this.selected != selected) {
            this.selected = selected;
            setVisualSelection(selected);
        }
    }

    public abstract boolean selectionIntersects(Rectangle selectionRectangle);

    protected abstract void setVisualSelection(boolean selected);

    public abstract void setZOrder();
}
