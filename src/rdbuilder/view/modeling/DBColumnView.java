package rdbuilder.view.modeling;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import rdbuilder.Utils;
import rdbuilder.control.ModelPaneController;
import rdbuilder.dbobject.Column;

import java.util.ArrayList;
import java.util.HashSet;

public class DBColumnView extends DBObjectView{

    private static final Image PRIMARY_KEY_RES = new Image("file:../../res/primary_key.png");
    private static final Image PRIMARY_KEY_PLACEHOLDER_RES = new Image("file:../../res/primary_key_placeholder.png");
    private static final Image NOT_NULL_RES = new Image("file:../../res/not_null.png");
    private static final Image NOT_NULL_PLACEHOLDER_RES = new Image("file:../../res/not_null_placeholder.png");

    private static final Background SELECTED_BACKGROUND = new Background(new BackgroundFill(SELECTED_COLOR, null, null));
    private static final Background ODD_BACKGROUND = new Background(new BackgroundFill(Color.rgb(207,220,243), null, null));
    private static final Background SELECTED_ODD_BACKGROUND = new Background(new BackgroundFill(SELECTED_COLOR_ALT, null, null));
    private final DBTableView parentTableView;
    private final VBox columnViewsHolder;
    private final HBox columnViewHBox;
    private final ImageView primaryKeyImage;
    private final Label nameLabel;
    private final Label typeLabel;
    private final Region space;
    private final ImageView notNullImage;
    private final ArrayList<Node> components = new ArrayList<>();
    private final HashSet<Node> toBindtoMouse = new HashSet<>();

    public DBColumnView(Column column, DBTableView parentTableView, ModelPaneController parentPaneController){
        super(column, parentPaneController);
        this.parentTableView = parentTableView;
        columnViewsHolder = parentTableView.columnViewsHolder;
        primaryKeyImage = new ImageView();
        nameLabel = new Label();
        space = new Region();
        typeLabel = new Label();
        notNullImage = new ImageView();
        columnViewHBox = new HBox();

        components.add(columnViewHBox);
        components.add(space);
        components.add(primaryKeyImage);
        components.add(nameLabel);
        components.add(typeLabel);
        components.add(notNullImage);

        toBindtoMouse.add(columnViewHBox);
        toBindtoMouse.add(space);
        toBindtoMouse.add(nameLabel);
        toBindtoMouse.add(typeLabel);

        Utils.platformRunAndWait(() -> {
            nameLabel.setText(column.getName());
            typeLabel.setText(column.getDataType());

            primaryKeyImage.setFitHeight(20);
            primaryKeyImage.setPreserveRatio(true);
            primaryKeyImage.setImage(column.isInPrimaryKey() ? PRIMARY_KEY_RES : PRIMARY_KEY_PLACEHOLDER_RES);
            primaryKeyImage.setCursor(Cursor.HAND);
            primaryKeyImage.setOnMouseClicked(event -> {
                if (!column.isInPrimaryKey()) {
                    column.getParent().getPrimaryKey().addColumn(column);
                    primaryKeyImage.setImage(PRIMARY_KEY_RES);
                    notNullImage.setImage(NOT_NULL_RES);
                }
                else {
                    column.getParent().getPrimaryKey().removeColumn(column);
                    primaryKeyImage.setImage(PRIMARY_KEY_PLACEHOLDER_RES);
                }
                event.consume();
            });

            notNullImage.setFitHeight(20);
            notNullImage.setPreserveRatio(true);
            notNullImage.setImage(column.isNotNull() ? NOT_NULL_RES : NOT_NULL_PLACEHOLDER_RES);
            notNullImage.setCursor(Cursor.HAND);
            notNullImage.setOnMouseClicked(event -> {
                final boolean notNull = column.isNotNull();
                if (!column.isInPrimaryKey()) {
                    column.setNotNull(!notNull);
                    notNullImage.setImage(!notNull ? NOT_NULL_RES : NOT_NULL_PLACEHOLDER_RES);
                }
                event.consume();
            });

            columnViewHBox.getChildren().addAll(primaryKeyImage, nameLabel, space, typeLabel, notNullImage);

            HBox.setHgrow(primaryKeyImage, Priority.NEVER);
            HBox.setHgrow(nameLabel, Priority.NEVER);
            HBox.setHgrow(space, Priority.ALWAYS);
            HBox.setHgrow(typeLabel, Priority.NEVER);
            HBox.setHgrow(notNullImage, Priority.NEVER);

            nameLabel.setMinWidth(Region.USE_PREF_SIZE);
            typeLabel.setMinWidth(Region.USE_PREF_SIZE);
            nameLabel.setMinHeight(Region.USE_PREF_SIZE);
            typeLabel.setMinHeight(Region.USE_PREF_SIZE);
            space.setMinWidth(10);

            columnViewsHolder.getChildren().add(columnViewHBox);

            for (Node node : toBindtoMouse){
                node.setOnMousePressed(EVENT_HANDLER_CONSUME);
            }
        });

        setDefaultBackgroundColor();
        setZOrder();
    }

    public void destroy() {
        super.destroy();

        Utils.platformRunAndWait(() -> {
            parentTableView.columnViewDestroyed(this);
            parentTableView.columnViewsHolder.getChildren()
                    .remove(columnViewHBox);
        });
    }

    @Override
    public void setZOrder() {
        Utils.platformRunAndWait(columnViewHBox::toFront);
    }

    @Override
    public void drag(MouseEvent event) {

    }

    @Override
    public void initializeDeltaPosition(MouseEvent event) {

    }

    @Override
    protected void setVisualSelection(boolean selected) {
        if (selected){
            Utils.platformRunAndWait(() -> {
                if (isOdd())
                    columnViewHBox.setBackground(SELECTED_ODD_BACKGROUND);
                else
                    columnViewHBox.setBackground(SELECTED_BACKGROUND);
            });
        }
        else {
            setDefaultBackgroundColor();
        }
    }

    @Override
    public boolean selectionIntersects(Rectangle selectionRectangle) {
        return false;
    }

    public DBTableView getParentTableView() {
        return parentTableView;
    }

    private boolean isOdd() {
        return columnViewsHolder.getChildren().indexOf(columnViewHBox) % 2 == 1;
    }

    public void setDefaultBackgroundColor() {
        Utils.platformRunAndWait(() -> {
            if (isOdd())
                columnViewHBox.setBackground(ODD_BACKGROUND);
            else
                columnViewHBox.setBackground(Background.EMPTY);
        });
    }

    public void setOnMouseClicked(EventHandler<MouseEvent> mouseClickHandler) {
        for (Node node : toBindtoMouse){
            node.setOnMouseClicked(mouseClickHandler);
        }
    }

    public void setOnMouseDragged(EventHandler<MouseEvent> mouseDragHandler) {
        for (Node node : toBindtoMouse){
            node.setOnMouseDragged(mouseDragHandler);
        }
    }

    public void setOnMouseReleased(EventHandler<MouseEvent> mouseReleaseHandler) {
        for (Node node : toBindtoMouse){
            node.setOnMouseReleased(mouseReleaseHandler);
        }
    }
}
