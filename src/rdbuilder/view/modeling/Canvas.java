package rdbuilder.view.modeling;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Canvas extends javafx.scene.canvas.Canvas {
    public static final int GRID_WIDTH = 20;
    public static final int GRID_HEIGHT = 20;
    private int scrollOffsetX = 0;
    private int scrollOffsetY = 0;
    private int offsetX = 0;
    private int offsetY = 0;

    public Canvas() {
        widthProperty().addListener(evt -> draw());
        heightProperty().addListener(evt -> draw());
    }

    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public double prefHeight(double width) {
        return getHeight();
    }

    @Override
    public double prefWidth(double height) {
        return getWidth();
    }

    private void draw(){
        double width = getWidth();
        double height = getHeight();

        GraphicsContext gc = getGraphicsContext2D();
        gc.clearRect(0, 0, width, height);
        for (int i = -scrollOffsetX - offsetX; i < width + GRID_WIDTH; i += GRID_WIDTH) {
            for (int j = -scrollOffsetY - offsetY; j < height + GRID_HEIGHT; j += GRID_HEIGHT) {
                gc.setFill(Color.LIGHTGRAY);
                gc.fillOval(i, j, 2, 2);
                gc.setFill(Color.GRAY);
                gc.fillOval(i, j, 1, 1);
            }
        }
    }

    public void setGridOffset(int offsetX, int offsetY) {
        this.offsetX = (this.offsetX + offsetX) % GRID_WIDTH;
        this.offsetY = (this.offsetY + offsetY) % GRID_HEIGHT;
        draw();
    }

    public void setScrollOffset(Integer scrollOffsetX, Integer scrollOffsetY) {
        if (scrollOffsetX != null)
            this.scrollOffsetX = scrollOffsetX % GRID_WIDTH;
        if (scrollOffsetY != null)
            this.scrollOffsetY = scrollOffsetY % GRID_HEIGHT;
        draw();
    }
}
