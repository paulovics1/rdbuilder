package rdbuilder.view.modeling;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class SelectionRectangle extends Rectangle{
    public static final Color STROKE_COLOR = DBObjectView.SELECTED_COLOR;
    public static final Color FILL_COLOR = STROKE_COLOR.deriveColor(0, 1, 1, 0.3);

    public SelectionRectangle(double x, double y) {
        super(x, y, 0, 0);
        setStrokeWidth(1);
        setStroke(STROKE_COLOR);
        setFill(FILL_COLOR);
        toFront();
    }
}
