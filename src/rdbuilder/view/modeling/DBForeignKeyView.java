package rdbuilder.view.modeling;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Bounds;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import rdbuilder.Utils;
import rdbuilder.control.ModelPaneController;
import rdbuilder.dbobject.ForeignKey;
import rdbuilder.view.ForeignKeyDialog;

import java.util.ArrayList;


public class DBForeignKeyView extends DBObjectView{
    public static final int STROKE_WIDTH = 5;
    private static final int ARROW_ANGLE = 15;
    private static final int ARROW_SIZE = 15;
    private static final Color DEFAULT_STROKE = Color.BLACK;
    final ArrayList<Line> lines = new ArrayList();
    final Label label;
    final Line arrow1, arrow2;
    private final ArrayList<Node> components = new ArrayList<>();
    private final ChangeListener<Bounds> lineBoundsChangeListener;
    public DBTableView.Pin endPin;
    DBTableView.Pin startPin;
    private Boolean loop = null;

    public DBForeignKeyView(ForeignKey foreignKey, ModelPaneController parentPaneController) {
        super(foreignKey, parentPaneController);

        arrow1 = new Line();
        arrow2 = new Line();
        label = new Label();

        components.add(arrow1);
        components.add(arrow2);
        components.add(label);

        lineBoundsChangeListener = (observable, oldValue, newValue) -> {
            alignComponentsToLine();
        };

        Utils.platformRunAndWait(() -> {
            parentPaneController.dbModelPane.getChildren().addAll(components);
            updateIsLoop();

            label.setCursor(Cursor.HAND);
            label.textProperty().bind(dbObject.nameProperty());
            label.setFont(Font.font(14));
            label.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
            label.translateXProperty().bind(label.widthProperty().negate().divide(2));
            label.translateYProperty().bind(label.heightProperty().negate().divide(2));

            arrow1.setStrokeWidth(STROKE_WIDTH);
            arrow2.setStrokeWidth(STROKE_WIDTH);

            updatePins();

            label.setOnMouseClicked(event -> {
                edit();
                event.consume();
            });

            label.setOnMousePressed(EVENT_HANDLER_CONSUME);

            alignComponentsToLine();
        });
    }

    @Override
    public ForeignKey getDBObject() {
        return (ForeignKey) super.getDBObject();
    }

    @Override
    public void setZOrder() {
        Utils.platformRunAndWait(() -> {
            for (Node component : components){
                component.toBack();
            }
        });
    }

    @Override
    public void drag(MouseEvent event) {

    }

    @Override
    public void initializeDeltaPosition(MouseEvent event) {

    }

    @Override
    protected void setVisualSelection(boolean selected) {
        if (selected){
            for (Line line : lines)
                line.setStroke(SELECTED_COLOR_ALT);
            arrow1.setStroke(SELECTED_COLOR_ALT);
            arrow2.setStroke(SELECTED_COLOR_ALT);
        }
        else{
            for (Line line : lines)
                line.setStroke(DEFAULT_STROKE);
            arrow1.setStroke(DEFAULT_STROKE);
            arrow2.setStroke(DEFAULT_STROKE);
        }
    }

    @Override
    public boolean selectionIntersects(Rectangle selectionRectangle) {
        for (Line line : lines)
            if (Shape.intersect(line, selectionRectangle).getBoundsInLocal().getWidth() != -1)
                return true;
        return false;
    }

    @Override
    public void destroy() {
        super.destroy();

        Utils.platformRunAndWait(() -> {
            releasePins();
            parentPaneController.dbModelPane.getChildren()
                    .removeAll(components);
        });
    }

    private void alignComponentsToLine() {
        if (!loop) {
            Line line = getFirstLine();
            final double startX = line.getStartX();
            final double startY = line.getStartY();
            final double endX = line.getEndX();
            final double endY = line.getEndY();
            final double length = (Math.sqrt(Math.pow(endY - startY, 2) + Math.pow(endX - startX, 2)));

            Platform.runLater(() -> label.setMaxWidth(length - ARROW_SIZE * 2 - STROKE_WIDTH));

            double angle = getLineAngle();
            arrow1.setStartX(endX - ARROW_SIZE * Math.cos(Math.toRadians(angle + ARROW_ANGLE)));
            arrow1.setStartY(endY - ARROW_SIZE * Math.sin(Math.toRadians(angle + ARROW_ANGLE)));
            arrow2.setStartX(endX - ARROW_SIZE * Math.cos(Math.toRadians(angle - ARROW_ANGLE)));
            arrow2.setStartY(endY - ARROW_SIZE * Math.sin(Math.toRadians(angle - ARROW_ANGLE)));
            angle = (angle + 360 + 90) % 180 - 90;
            label.setRotate(angle);
        }
        else {
            Line line = getLastLine();
            final double endX = line.getEndX();
            final double endY = line.getEndY();
            arrow1.setStartX(endX - ARROW_SIZE * Math.cos(Math.toRadians(180 + ARROW_ANGLE)));
            arrow1.setStartY(endY - ARROW_SIZE * Math.sin(Math.toRadians(180 + ARROW_ANGLE)));
            arrow2.setStartX(endX - ARROW_SIZE * Math.cos(Math.toRadians(180 - ARROW_ANGLE)));
            arrow2.setStartY(endY - ARROW_SIZE * Math.sin(Math.toRadians(180 - ARROW_ANGLE)));
            label.setRotate(0);
        }
    }

    private void bindLoopLines() {
        final Line firstLine = getFirstLine();
        final Line middleLine = getMiddleLine();
        final Line lastLine = getLastLine();
        firstLine.endXProperty().bind(firstLine.startXProperty().add(label.widthProperty().divide(2)).add(ARROW_SIZE));
        firstLine.endYProperty().bind(firstLine.startYProperty());
        lastLine.startXProperty().bind(lastLine.endXProperty().add(label.widthProperty().divide(2)).add(ARROW_SIZE));
        lastLine.startYProperty().bind(lastLine.endYProperty());
        middleLine.startXProperty().bind(firstLine.endXProperty());
        middleLine.endXProperty().bind(lastLine.startXProperty());
        middleLine.startYProperty().bind(firstLine.endYProperty());
        middleLine.endYProperty().bind(lastLine.startYProperty());
    }

    public void edit(){
        ForeignKey foreignKey = new ForeignKeyDialog(getScene().getWindow(), (ForeignKey) dbObject).showAndGetResult();
        if (foreignKey != null) {
            ((ForeignKey) dbObject).extractFrom(foreignKey);
            updateIsLoop();
            updatePins();
            alignComponentsToLine();
        }
    }

    public Line getFirstLine() {
        return lines.get(0);
    }

    public Line getLastLine() {
        return lines.get(lines.size()-1);
    }

    public double getLineAngle() {
        Line line = getFirstLine();
        return (Math.toDegrees(Math.atan2(line.getEndY() - line.getStartY(),
                line.getEndX() - line.getStartX()))+360) % 360;
    }

    public Line getMiddleLine() {
        return lines.get((lines.size()-1)/2);
    }

    public DBTableView getReferencedTableView() {
        return (DBTableView) parentPaneController.getDBObjectView(((ForeignKey) dbObject).getReferencedTable());
    }

    public DBTableView getTableView() {
        return (DBTableView) parentPaneController.getDBObjectView(((ForeignKey) dbObject).getTable());
    }

    public double getTablesAngle() {
        if (loop){
            return 0;
        }

        DBTableView tableView = getTableView();
        DBTableView referencedTableView = getReferencedTableView();

        Line line = getFirstLine();
        double startX, startY, endX, endY;
        if (tableView != null){
            startX = tableView.getCenterX();
            startY = tableView.getCenterY();

        }
        else {
            startX = line.getStartX();
            startY = line.getStartY();
        }

        if (referencedTableView != null){
            endX = referencedTableView.getCenterX();
            endY = referencedTableView.getCenterY();

        }
        else {
            endX = line.getEndX();
            endY = line.getEndY();
        }

        double angle = Math.toDegrees(Math.atan2(endY - startY,
                endX - startX));

        angle = (angle + 360) % 360;
        return angle;
    }

    public boolean isLoop() {
        return loop;
    }

    public void pointTo(double x, double y) {
        Line line = getFirstLine();
        line.setEndX(x);
        line.setEndY(y);
    }

    public void releasePins(){
        if (startPin != null){
            startPin.destroy();
            startPin = null;
        }
        if (endPin != null){
            endPin.destroy();
            endPin = null;
        }
    }

    private void unbindLine(Line line) {
        line.layoutBoundsProperty().removeListener(lineBoundsChangeListener);
        line.startXProperty().unbind();
        line.startYProperty().unbind();
        line.endXProperty().unbind();
        line.endYProperty().unbind();
        line.setOnMouseClicked(null);
        line.setOnMousePressed(null);
    }

    private void updateIsLoop() {
        ForeignKey foreignKey = getDBObject();
        boolean newIsLoop = foreignKey.getTable() == foreignKey.getReferencedTable();
        if (loop == null || loop != newIsLoop){

            for (Line line: lines){
                unbindLine(line);
                components.remove(line);
                parentPaneController.dbModelPane.getChildren().remove(line);
            }
            lines.clear();

            loop = newIsLoop;
            if (loop){
                for (int i = 0; i < 3; i++){
                    final Line line = new Line();
                    lines.add(line);
                    components.add(line);
                    parentPaneController.dbModelPane.getChildren().add(line);
                }
            }
            else {
                final Line line = new Line();
                lines.add(line);
                components.add(line);
                parentPaneController.dbModelPane.getChildren().add(line);
            }

            final Line firstLine = getFirstLine();
            final Line middleLine = getMiddleLine();
            final Line lastLine = getLastLine();
            arrow1.endXProperty().bind(lastLine.endXProperty());
            arrow1.endYProperty().bind(lastLine.endYProperty());
            arrow2.endXProperty().bind(lastLine.endXProperty());
            arrow2.endYProperty().bind(lastLine.endYProperty());
            label.layoutXProperty().bind((middleLine.startXProperty().add(middleLine.endXProperty())).divide(2));
            label.layoutYProperty().bind((middleLine.endYProperty().add(middleLine.startYProperty())).divide(2));

            if (loop){
                bindLoopLines();
            }

            lastLine.layoutBoundsProperty().addListener(lineBoundsChangeListener);

            ArrayList<Line> arrow = new ArrayList<>(lines);
            arrow.add(arrow1);
            arrow.add(arrow2);
            for (Line line: arrow) {
                line.setStrokeWidth(STROKE_WIDTH);
                line.setOnMouseClicked(event -> {
                    if (event.getButton() == MouseButton.PRIMARY) {
                        parentPaneController.viewMouseClick(this, event);
                        event.consume();
                    } else {
                        edit();
                        event.consume();
                    }
                });
                line.setOnMousePressed(EVENT_HANDLER_CONSUME);
            }
            setZOrder();
        }
    }

    public void updatePins(){
        releasePins();

        DBTableView tableView = getTableView();
        DBTableView referencedTableView = getReferencedTableView();

        if (tableView != null)
            tableView.bind(this, true);
        if (referencedTableView != null)
            referencedTableView.bind(this, false);
    }
}
