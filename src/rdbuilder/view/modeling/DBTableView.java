package rdbuilder.view.modeling;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableNumberValue;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Pair;
import rdbuilder.Utils;
import rdbuilder.control.ModelPaneController;
import rdbuilder.control.TableDialogController.TableDialogResult;
import rdbuilder.dbobject.Column;
import rdbuilder.dbobject.DBObjectObserver;
import rdbuilder.dbobject.ForeignKey;
import rdbuilder.dbobject.Table;
import rdbuilder.view.TableDialog;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class DBTableView extends DBObjectView implements DBObjectObserver{
    private static final Image EDIT_RES = new Image("file:../../res/edit.png");
    private static final Color FG_FILL = Color.WHITE;
    private static final Color BG_FILL = Color.CORNFLOWERBLUE;
    private static final Color DEFAULT_STROKE = Color.BLACK;
    private static final int DEFAULT_STROKE_WIDTH = 2;
    private static final Color SELECTED_HIGHLIGHT = SELECTED_COLOR;
    private static final DropShadow DRAG_OVER_SHADOW = new DropShadow(BlurType.GAUSSIAN, Color.BLACK, 7, 0.4, 0, 0);
    private static final Color DRAG_OVER_STROKE = Color.WHITE;
    private static final int DEFAULT_ARC_SIZE = 10;
    private static final int PADDING = 5;
    private static final Color SELECTED_STROKE = SELECTED_COLOR.deriveColor(0, 1, 0.85, 1);
    private static final Border DEFAULT_BORDER = new Border(new BorderStroke(
            DEFAULT_STROKE,
            BorderStrokeStyle.SOLID,
            CornerRadii.EMPTY,
            new BorderWidths(DEFAULT_STROKE_WIDTH)));
    private static final Border SELECTED_BORDER = new Border(new BorderStroke(
            SELECTED_STROKE,
            BorderStrokeStyle.SOLID,
            null,
            new BorderWidths(DEFAULT_STROKE_WIDTH)));
    private static final Border DRAG_OVER_BORDER = new Border(new BorderStroke(
            DRAG_OVER_STROKE,
            BorderStrokeStyle.SOLID,
            CornerRadii.EMPTY,
            new BorderWidths(DEFAULT_STROKE_WIDTH)));
    private static final int EDIT_IMAGE_SIZE = 16;
    final VBox columnViewsHolder;
    private final Rectangle baseRectangle;
    private final Label headerLabel;
    private final ArrayList<Node> components = new ArrayList<>();
    private final ArrayList<DBColumnView> columnViews = new ArrayList<>();
    private final ChangeListener<Number> columnHolderWidthListener;
    private final ImageView editImageView;
    private final ChangeListener<Bounds> boundsChangeListener;
    @SuppressWarnings("unchecked")
    private final ArrayList<Pin>[] pins = new ArrayList[]{new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList()};

    public class Pin {
        final DoubleProperty xProperty = new SimpleDoubleProperty(0);
        final DoubleProperty yProperty = new SimpleDoubleProperty(0);
        private DBForeignKeyView boundForeignKey;
        private boolean initialTablePin;

        public void bind(DBForeignKeyView foreignKeyView, boolean initialTable){
            if (boundForeignKey == foreignKeyView && initialTablePin == initialTable)
                return;

            unbind();

            boundForeignKey = foreignKeyView;
            initialTablePin = initialTable;

            if (initialTablePin){
                boundForeignKey.startPin = this;
                Utils.platformRunAndWait(() -> {
                    foreignKeyView.getFirstLine().startXProperty().bind(xProperty);
                    foreignKeyView.getFirstLine().startYProperty().bind(yProperty);
                });
            }
            else {
                boundForeignKey.endPin = this;
                Utils.platformRunAndWait(() -> {
                    foreignKeyView.getLastLine().endXProperty().bind(xProperty);
                    foreignKeyView.getLastLine().endYProperty().bind(yProperty);
                });
            }
        }

        public void destroy(){
            removePin(this);
            unbind();
            xProperty.unbind();
            yProperty.unbind();
        }

        public int getDestinationSide() {
            double angle = getShiftedAngle();
            return (int) (angle / 90);
        }

        private double getShiftedAngle() {
            double angle = boundForeignKey.getTablesAngle();
            if (!initialTablePin && !boundForeignKey.isLoop())
                angle += 180;
            return (angle + 135) % 360;
        }

        public void unbind() {
            if (boundForeignKey != null) {
                if (initialTablePin){
                    boundForeignKey.startPin = null;
                    Utils.platformRunAndWait(() -> {
                        boundForeignKey.getFirstLine().startXProperty().unbind();
                        boundForeignKey.getFirstLine().startYProperty().unbind();
                    });
                }
                else {
                    boundForeignKey.endPin = null;
                    Utils.platformRunAndWait(() -> {
                        boundForeignKey.getLastLine().endXProperty().unbind();
                        boundForeignKey.getLastLine().endYProperty().unbind();
                    });
                }
                boundForeignKey = null;
            }
        }
    }

    public DBTableView(Table table, ModelPaneController parentPaneController) {
        super(table, parentPaneController);
        headerLabel = new Label();
        baseRectangle = new Rectangle();
        columnViewsHolder = new VBox();
        editImageView = new ImageView();

        components.add(baseRectangle);
        components.add(columnViewsHolder);
        components.add(headerLabel);

        boundsChangeListener = (observable, oldValue, newValue) -> onBoundsChange();
        columnHolderWidthListener = (observable, oldValue, newValue) -> updateWidth();

        Utils.platformRunAndWait(() -> {
            parentPaneController.dbModelPane.getChildren().addAll(components);
            parentPaneController.dbModelPane.getChildren().add(editImageView);

            /* FIRST RUN */
            headerLabel.textProperty().bind(table.nameProperty());
            headerLabel.setMinHeight(Region.USE_COMPUTED_SIZE);
            headerLabel.setOnMousePressed(EVENT_HANDLER_CONSUME);
            final Font defaultFont = Font.getDefault();
            headerLabel.setFont(Font.font(defaultFont.getFamily(),
                    FontWeight.BOLD,
                    defaultFont.getSize()));

            baseRectangle.setArcHeight(DEFAULT_ARC_SIZE);
            baseRectangle.setArcWidth(DEFAULT_ARC_SIZE);
            baseRectangle.setFill(BG_FILL);
            baseRectangle.setStroke(DEFAULT_STROKE);
            baseRectangle.setStrokeWidth(DEFAULT_STROKE_WIDTH);
            baseRectangle.setOnMousePressed(EVENT_HANDLER_CONSUME);
            baseRectangle.setOnMouseDragEntered(getMouseDragEnteredHandler(this));
            baseRectangle.setOnMouseDragExited(getMouseDragExitedHandler(this));
            baseRectangle.boundsInParentProperty().addListener(boundsChangeListener);

            columnViewsHolder.setBorder(DEFAULT_BORDER);
            columnViewsHolder.setBackground(new Background(new BackgroundFill(FG_FILL, CornerRadii.EMPTY, null)));
            columnViewsHolder.layoutXProperty().bind(baseRectangle.layoutXProperty());
            columnViewsHolder.layoutYProperty().bind(baseRectangle.layoutYProperty()
                    .add(headerLabel.heightProperty()));
            columnViewsHolder.widthProperty().addListener(columnHolderWidthListener);

            editImageView.setFitHeight(EDIT_IMAGE_SIZE);
            editImageView.setPreserveRatio(true);
            editImageView.setImage(EDIT_RES);
            editImageView.layoutXProperty().bind(baseRectangle.layoutXProperty()
                    .add(baseRectangle.widthProperty()
                    .subtract(EDIT_IMAGE_SIZE + DEFAULT_ARC_SIZE / 3)));

            editImageView.layoutYProperty().bind(baseRectangle.layoutYProperty().add(DEFAULT_ARC_SIZE / 3));
            editImageView.setCursor(Cursor.HAND);
            editImageView.setOnMouseClicked(event -> {
                TableDialogResult result = new TableDialog(getScene().getWindow(), ((Table) dbObject)).showAndGetResult();
                if (result != null) {
                    ((Table) dbObject).extractFrom(result.table, result.columnMap, result.uniqueConstraintsMap);
                }
                reloadColumns();
                event.consume();
            });

            /* SECOND RUN */
            headerLabel.layoutXProperty().bind(baseRectangle.layoutXProperty()
                    .add((baseRectangle.widthProperty().subtract(headerLabel.widthProperty())).divide(2)));
            headerLabel.layoutYProperty().bind(baseRectangle.layoutYProperty());
            headerLabel.widthProperty().addListener(columnHolderWidthListener);

            baseRectangle.heightProperty().bind(headerLabel.heightProperty()
                    .add(columnViewsHolder.heightProperty())
                    .add(DEFAULT_ARC_SIZE));

            for (Node node : components) {
                node.setOnMouseDragged(getMouseDragHandler(this));
                node.setOnMouseClicked(getMouseClickHandler(this));
                node.setOnMouseReleased(getMouseReleaseHandler(this));
                node.setOnMouseDragReleased(getMouseDragReleaseHandler(this));
            }
        });

        reloadColumns();
        setZOrder();
        updateWidth();
    }

    @Override
    public void setZOrder(){
        Utils.platformRunAndWait(() -> {
            for (Node component : components) {
                component.toFront();
            }
            for (DBColumnView columnView : columnViews) {
                columnView.setZOrder();
            }
            editImageView.toFront();
        });
    }

    @Override
    public void drag(MouseEvent event) {
        baseRectangle.setLayoutX(event.getSceneX() - dragDeltaX);
        baseRectangle.setLayoutY(event.getSceneY() - dragDeltaY);
    }

    @Override
    public void initializeDeltaPosition(MouseEvent event) {
        dragDeltaX = event.getSceneX() - baseRectangle.getLayoutX();
        dragDeltaY = event.getSceneY() - baseRectangle.getLayoutY();
    }

    @Override
    protected void setVisualSelection(boolean selected) {
        baseRectangle.setEffect(null);
        if (selected){
            baseRectangle.setFill(SELECTED_HIGHLIGHT);
            baseRectangle.setStroke(SELECTED_STROKE);
            columnViewsHolder.setBorder(SELECTED_BORDER);
        }
        else {
            baseRectangle.setFill(BG_FILL);
            baseRectangle.setStroke(DEFAULT_STROKE);
            columnViewsHolder.setBorder(DEFAULT_BORDER);
        }
    }

    public boolean selectionIntersects(Rectangle selectionRectangle) {
        return selectionRectangle.getBoundsInParent().intersects(baseRectangle.getBoundsInParent());
    }

    @Override
    public void destroy() {
        super.destroy();

        Utils.platformRunAndWait(() -> {
            parentPaneController.dbModelPane.getChildren()
                    .removeAll(components);
            parentPaneController.dbModelPane.getChildren()
                    .remove(editImageView);
        });
    }

    public DBColumnView addColumn(Column column){
        DBColumnView columnView = new DBColumnView(column, this, parentPaneController);
        columnViews.add(columnView);

        Utils.platformRunAndWait(() -> {
            columnView.setOnMouseDragged(getMouseDragHandler(columnView));
            columnView.setOnMouseClicked(getMouseClickHandler(columnView));
            columnView.setOnMouseReleased(getMouseReleaseHandler(columnView));
        });
        return columnView;
    }

    public void bind(DBForeignKeyView foreignKeyView, boolean initialTable){
        double angle = foreignKeyView.getTablesAngle() + 135;

        if (!initialTable && !foreignKeyView.isLoop()) {
            angle = (angle + 180);
        }

        angle %= 360;

        int side = (int) (angle / 90);
        Pin pin = new Pin();
        pin.bind(foreignKeyView, initialTable);
        pins[side].add(pin);
        updatePinsBindingsOnSide(side);
        updatePinsOrderOnSide(side);
    }

    public void columnViewDestroyed(DBColumnView dbColumnView){
        columnViews.remove(dbColumnView);
        for (DBColumnView columnView : columnViews){
            columnView.setVisualSelection(columnView.isSelected());
        }
    }

    public int getBottom() {
        return getTop() + (int) baseRectangle.getHeight();
    }

    private Bounds getBoundsInParent() {
        return baseRectangle.getBoundsInParent();
    }

    public Point2D getCenter() {
        return new Point2D(getCenterX(), getCenterY());
    }

    public double getCenterX() {
        return baseRectangle.getLayoutX() + baseRectangle.getWidth()/2 ;
    }

    public double getCenterY() {
        return baseRectangle.getLayoutY() + baseRectangle.getHeight()/2 ;
    }

    public VBox getColumnViewsHolder() {
        return columnViewsHolder;
    }

    private double getHeaderWidth() {
        final double[] result = {0};
        Utils.platformRunAndWait(() ->
                result[0] = headerLabel.getWidth() +
                        2*(PADDING + EDIT_IMAGE_SIZE + DEFAULT_ARC_SIZE / 3));

        return result[0];
    }

    public int getHeight() {
        return (int) baseRectangle.getHeight();
    }

    public int getLeft() {
        return (int) baseRectangle.getLayoutX();
    }

    public EventHandler<MouseEvent> getMouseClickHandler(DBObjectView dbObjectView){
        return event -> {
            parentPaneController.viewMouseClick(dbObjectView, event);
            event.consume();
        };
    }

    private EventHandler<? super MouseDragEvent> getMouseDragEnteredHandler(DBObjectView dbObjectView) {
        return event -> parentPaneController.viewMouseDragEntered(dbObjectView, event);
    }

    private EventHandler<? super MouseDragEvent> getMouseDragExitedHandler(DBObjectView dbObjectView) {
        return event -> parentPaneController.viewMouseDragExited(dbObjectView, event);
    }

    public EventHandler<MouseEvent> getMouseDragHandler(DBObjectView dbObjectView){
        return event -> {
            parentPaneController.viewMouseDrag(dbObjectView, event);
            event.consume();
        };
    }

    private EventHandler<? super MouseDragEvent> getMouseDragReleaseHandler(DBTableView dbTableView) {
        return event -> {
            parentPaneController.viewMouseDragRelease(this, event);
            event.consume();
        };
    }

    public EventHandler<MouseEvent> getMouseReleaseHandler(DBObjectView dbObjectView){
        return event -> {
            parentPaneController.viewMouseRelease(dbObjectView, event);
            event.consume();
        };
    }

    public Point2D getPosition() {
        return new Point2D(baseRectangle.getLayoutX(), baseRectangle.getLayoutY());
    }

    public void setPosition(Point2D position) {
        baseRectangle.setLayoutX(position.getX());
        baseRectangle.setLayoutY(position.getY());
    }

    public int getRight() {
        return getLeft() + (int) baseRectangle.getWidth();
    }

    public int getTop() {
        return (int) baseRectangle.getLayoutY();
    }

    public int getWidth() {
        return (int) baseRectangle.getWidth();
    }

    public boolean intersects(DBTableView dbTableView){
        return baseRectangle.getBoundsInParent().intersects(dbTableView.getBoundsInParent());
    }

    public void move(int dx, int dy) {
        baseRectangle.setLayoutX(baseRectangle.getLayoutX()+dx);
        baseRectangle.setLayoutY(baseRectangle.getLayoutY()+dy);
        dragDeltaX -= dx;
        dragDeltaY -= dy;
    }

    public void onBoundsChange() {
        HashSet<DBTableView> toUpdatePinSides = new HashSet<>();
        for (ArrayList<Pin> pinArray : pins) {
            for (Pin pin : pinArray) {
                ForeignKey foreignKey = ((ForeignKey) pin.boundForeignKey.dbObject);
                DBTableView tableView = (DBTableView) parentPaneController.getDBObjectView(foreignKey.getTable());
                DBTableView referencedTableView = (DBTableView) parentPaneController.getDBObjectView(foreignKey.getReferencedTable());

                if (tableView != null)
                    toUpdatePinSides.add(tableView);
                if (referencedTableView != null)
                    toUpdatePinSides.add(referencedTableView);
            }
        }
        for (DBTableView dbTableView : toUpdatePinSides) {
            dbTableView.updatePinSides();
        }
    }

    private void reloadColumns() {
        while (!columnViews.isEmpty()){
            columnViews.get(columnViews.size()-1).destroy();
        }



        Utils.platformRunAndWait(() -> {
            headerLabel.setPrefWidth(Region.USE_COMPUTED_SIZE);
            headerLabel.autosize();
            baseRectangle.setWidth(Region.USE_COMPUTED_SIZE);
            columnViewsHolder.setPrefWidth(Region.USE_COMPUTED_SIZE);
            columnViewsHolder.autosize();
        });

        try {
            dbObject.loadContent(false);
        } catch (SQLException e) {
            Utils.showError(e, "Error during database communication", "It was not possible to load the database object content.");
        }

        ArrayList<Column> columns = ((Table) dbObject).getColumns();
        for (Column column : columns){
            parentPaneController.getDbObjectViewMap().put(column, addColumn(column));
        }
    }

    public void removePin(Pin pin){
        for (int i = 0; i < pins.length; i++){
            if (pins[i].remove(pin)){
                updatePinsBindingsOnSide(i);
                return;
            }
        }
    }

    public void setForeignKeyDraggedOver(boolean bool){
        if (bool) {
            baseRectangle.setEffect(DRAG_OVER_SHADOW);
            baseRectangle.setStroke(DRAG_OVER_STROKE);
            columnViewsHolder.setBorder(DRAG_OVER_BORDER);
        }
        else {
            setVisualSelection(isSelected());
        }
    }

    public void setPosition(java.awt.geom.Point2D position) {
        baseRectangle.setLayoutX(position.getX());
        baseRectangle.setLayoutY(position.getY());
    }

    public void updatePinSides() {
        HashSet<Integer> updatePinsOnSides = new HashSet<>();
        for (int i = 0; i < pins.length; i++){
            Iterator<Pin> pinIterator = pins[i].iterator();
            while (pinIterator.hasNext()){
                Pin pin = pinIterator.next();
                int side = pin.getDestinationSide();
                if (side != i){
                    pinIterator.remove();
                    pins[side].add(pin);
                    updatePinsOnSides.add(side);
                    updatePinsOnSides.add(i);
                }
            }
        }

        for (int i = 0; i < pins.length; i++) {
            if (updatePinsOnSides.contains(i)) {
                updatePinsBindingsOnSide(i);
            }
            updatePinsOrderOnSide(i);
        }
    }

    private void updatePinsBindingsOnSide(int i) {
        for (int j = 0; j < pins[i].size(); j++){
            Pin pin = pins[i].get(j);
            if (i % 2 == 0){
                ObservableNumberValue heightToAdd = i == 0 ?
                        new SimpleIntegerProperty(0) :
                        baseRectangle.heightProperty();

                pin.xProperty.bind(baseRectangle.layoutXProperty()
                        .add(baseRectangle.widthProperty()
                                .divide(pins[i].size()+1)
                                .multiply(j+1)));
                pin.yProperty.bind(baseRectangle.layoutYProperty().add(heightToAdd));
            }
            else{
                ObservableNumberValue widthToAdd = i == 3 ?
                        new SimpleIntegerProperty(0) :
                        baseRectangle.widthProperty();

                pin.xProperty.bind(baseRectangle.layoutXProperty().add(widthToAdd));
                pin.yProperty.bind(baseRectangle.layoutYProperty()
                        .add(baseRectangle.heightProperty()
                        .divide(pins[i].size()+1)
                        .multiply(j+1)));
            }
        }
    }

    private void updatePinsOrderOnSide(int side) {
        ArrayList<Pair<DBForeignKeyView, Boolean>> orderedList = new ArrayList<>();

        for (Pin pin : pins[side]) {
            orderedList.add(new Pair<>(pin.boundForeignKey, pin.initialTablePin));
            pin.unbind();
        }

        orderedList.sort((o1, o2) -> {
            DBForeignKeyView fkv1 = o1.getKey();
            DBForeignKeyView fkv2 = o2.getKey();
            if (fkv1 == fkv2)
                return 0;

            DBTableView initialTableView1 = (DBTableView) parentPaneController.getDBObjectView(((ForeignKey) fkv1.dbObject).getTable());
            DBTableView initialTableView2 = (DBTableView) parentPaneController.getDBObjectView(((ForeignKey) fkv2.dbObject).getTable());
            Double angle1 = fkv1.getTablesAngle()+45;
            Double angle2 = fkv2.getTablesAngle()+45;

            if (DBTableView.this != initialTableView1 && !fkv1.isLoop()) {
                angle1 = (angle1 + 180);
            }
            if (DBTableView.this != initialTableView2 && !fkv2.isLoop()) {
                angle2 = (angle2 + 180);
            }

            angle1 %= 360;
            angle2 %= 360;

            if (angle1.equals(angle2)){
                return fkv1.dbObject.getName().compareTo(fkv2.dbObject.getName());
            }

            if (side < 2) {
                return angle1.compareTo(angle2);
            }

            return angle2.compareTo(angle1);

        });

        int i = 0;
        for (Pair<DBForeignKeyView, Boolean> pair : orderedList) {
            pins[side].get(i++).bind(pair.getKey(), pair.getValue());
        }
    }

    public void updateWidth() {
        Utils.platformRunAndWait(() -> {
            double maxWidth = Math.max(getHeaderWidth(), columnViewsHolder.getWidth());
            baseRectangle.setWidth(maxWidth);
            columnViewsHolder.setPrefWidth(maxWidth);
            columnViewsHolder.autosize();
        });
    }
}
