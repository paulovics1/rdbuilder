package rdbuilder.view;

import com.sun.javafx.stage.StageHelper;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import rdbuilder.control.TaskExecutionPopupController;

import java.io.IOException;

public class TaskExecutionPopup extends Stage {

    private TaskExecutionPopupController controller;

    public TaskExecutionPopup(Window owner, String description, boolean cancellable) throws IOException {
        super();

        initOwner(owner);
        initModality(Modality.WINDOW_MODAL);
        initStyle(StageStyle.UNDECORATED);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("taskExecutionPopup.fxml"));
        Parent content = fxmlLoader.load();
        ((Label) content.lookup("#descriptionLabel")).setText(description);
        content.lookup("#cancelButton").setVisible(cancellable);
        setScene(new Scene(content));

        controller = fxmlLoader.getController();

        setOnShown(event -> {
            final Stage mainStage = StageHelper.getStages().get(0);
            final double x = mainStage.getX() + mainStage.getWidth() / 2 - getWidth() / 2;
            final double y = mainStage.getY() + mainStage.getHeight() / 2 - getHeight() / 2;
            Platform.runLater(()-> {
                setX(x);
                setY(y);
            });
        });
    }

    public TaskExecutionPopupController getController() {
        return controller;
    }
}
