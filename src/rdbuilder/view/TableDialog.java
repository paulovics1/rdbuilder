package rdbuilder.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import rdbuilder.Utils;
import rdbuilder.control.TableDialogController;
import rdbuilder.control.TableDialogController.TableDialogResult;
import rdbuilder.dbobject.Table;

import java.io.IOException;

public class TableDialog extends Stage {
    private final TableDialogController controller;
    public TableDialog(Window owner, Table initialTable) {
        super();
        initModality(Modality.APPLICATION_MODAL);
        initStyle(StageStyle.UTILITY);
        setResizable(false);
        initOwner(owner);
        setTitle(initialTable == null ?
                "Add new table" :
                "Edit table " + initialTable.getName());

        controller = new TableDialogController(this, initialTable);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("tableDialog.fxml"));
        fxmlLoader.setController(controller);
        try {
            setScene(new Scene(fxmlLoader.load()));
        } catch (IOException e) {
            Utils.showError(e, "FXML file load exception", "It was not possible to load table dialog layout file.");
        }
    }

    public TableDialogResult showAndGetResult() {
        showAndWait();
        return controller.getResult();
    }
}
