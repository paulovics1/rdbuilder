package rdbuilder.view;

import javafx.beans.property.BooleanProperty;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import rdbuilder.control.ControllerMediator;
import rdbuilder.dbobject.DBObject;
import rdbuilder.dbobject.DBRoot;
import rdbuilder.dbobject.Schema;

import java.sql.SQLException;

public class TreeCell extends javafx.scene.control.TreeCell<DBObject> {
    private static final int GRAPHIC_SIZE = 20;
    private static final Image REFRESH_RES = new Image("file:../../res/refresh.png", GRAPHIC_SIZE, GRAPHIC_SIZE, true, true);
    private static final Image CONFIGURE_RES = new Image("file:../../res/config.png", GRAPHIC_SIZE, GRAPHIC_SIZE, true, false);
    private static final Image DISCONNECT_RES = new Image("file:../../res/disconnect.png", GRAPHIC_SIZE, GRAPHIC_SIZE, true, true);
    private static final Image FORGET_RES = new Image("file:../../res/delete.png", GRAPHIC_SIZE, GRAPHIC_SIZE, true, true);
    private static final Image DOWN_RES = new Image("file:../../res/download.png", GRAPHIC_SIZE, GRAPHIC_SIZE, true, true);
    private static final Image UP_RES = new Image("file:../../res/upload.png", GRAPHIC_SIZE, GRAPHIC_SIZE, true, true);
    private final ContextMenu contextMenu = new ContextMenu();
    private final MenuItem refreshMenuItem;
    private final MenuItem configureMenuItem;
    private final MenuItem forgetMenuItem;
    private final MenuItem disconnectMenuItem;
    private final MenuItem downloadMenuItem;
    private final MenuItem uploadMenuItem;
    private final SeparatorMenuItem separatorMenuItem1;
    private final SeparatorMenuItem separatorMenuItem2;

    public TreeCell() {
        super();
        refreshMenuItem = new MenuItem("Refresh", new ImageView(REFRESH_RES));
        refreshMenuItem.setOnAction(event -> {
            ControllerMediator.getDBTreeController().loadContent(getTreeItem());
        });

        configureMenuItem = new MenuItem("Configure", new ImageView(CONFIGURE_RES));
        configureMenuItem.setOnAction(event -> {
            ControllerMediator.getRootController().configureConnectionDialog();
        });
        disconnectMenuItem = new MenuItem("Disconnect", new ImageView(DISCONNECT_RES));
        disconnectMenuItem.setOnAction(event -> {
            ControllerMediator.getRootController().disconnect();
        });
        downloadMenuItem = new MenuItem("Load schema model", new ImageView(DOWN_RES));
        downloadMenuItem.setOnAction(event -> {
            ControllerMediator.getRootController().loadSchemaModelFromDatabase();
        });
        uploadMenuItem = new MenuItem("Upload schema model", new ImageView(UP_RES));
        uploadMenuItem.setOnAction(event -> {
            ControllerMediator.getRootController().applySchema();
        });
        forgetMenuItem = new MenuItem("Forget", new ImageView(FORGET_RES));
        forgetMenuItem.setOnAction(event -> {
            ControllerMediator.getRootController().deleteConnection();
        });
        separatorMenuItem1 = new SeparatorMenuItem();
        separatorMenuItem2 = new SeparatorMenuItem();
        contextMenu.getItems().addAll(refreshMenuItem,
                separatorMenuItem1,
                downloadMenuItem,
                uploadMenuItem,
                configureMenuItem,
                disconnectMenuItem,
                separatorMenuItem2,
                forgetMenuItem);
    }

    @Override
    public void updateItem(DBObject item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
            setContextMenu(null);

        } else {
            setText(item.toString());
            final ImageView imageView = new ImageView(new Image("file:../../res/"+item.getClass().getSimpleName()+".png"));
            imageView.setFitHeight(GRAPHIC_SIZE);
            imageView.setPreserveRatio(true);
            setGraphic(imageView);

            setContextMenu(contextMenu);
            final BooleanProperty schemaDrawnProperty = ControllerMediator.getModelPaneController().getSchemaDrawnProperty();
            boolean isDBRoot = item instanceof DBRoot;
            boolean isSchema = item instanceof Schema;
            separatorMenuItem1.setVisible(isDBRoot || isSchema);

            uploadMenuItem.setVisible(isSchema);
            uploadMenuItem.disableProperty().bind(schemaDrawnProperty.not());

            downloadMenuItem.setVisible(isSchema);
            configureMenuItem.setVisible(isDBRoot);
            disconnectMenuItem.setVisible(isDBRoot);
            try {
                disconnectMenuItem.setDisable(!(item.getRoot().getDBConnector().isConnected()));
            } catch (SQLException e) {
                disconnectMenuItem.setDisable(true);
            }
            separatorMenuItem2.setVisible(isDBRoot);
            forgetMenuItem.setVisible(isDBRoot);
        }
    }
}
