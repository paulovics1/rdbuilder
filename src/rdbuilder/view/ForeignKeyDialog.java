package rdbuilder.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import rdbuilder.Utils;
import rdbuilder.control.ForeignKeyDialogController;
import rdbuilder.dbobject.ForeignKey;

import java.io.IOException;

public class ForeignKeyDialog extends Stage {
    private final ForeignKeyDialogController controller;
    public ForeignKeyDialog(Window owner, ForeignKey initialForeignKey) {
        super();
        initModality(Modality.APPLICATION_MODAL);
        initStyle(StageStyle.UTILITY);
        setResizable(false);
        initOwner(owner);
        setTitle(initialForeignKey == null ?
                "Add new foreign key" :
                "Edit foreign key " + initialForeignKey.getName());

        controller = new ForeignKeyDialogController(this, initialForeignKey);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("foreignKeyDialog.fxml"));
        fxmlLoader.setController(controller);
        try {
            setScene(new Scene(fxmlLoader.load()));
        } catch (IOException e) {
            Utils.showError(e, "FXML file load exception", "It was not possible to load table dialog layout file.");
        }
    }

    public ForeignKey showAndGetResult() {
        showAndWait();
        return controller.getResult();
    }
}
