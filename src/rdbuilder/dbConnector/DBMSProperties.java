package rdbuilder.dbconnector;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * This class contains experimental features that are not being used yet. In the future releases,
 * it should be possible to define a set of necessary parameters for connections to every
 * database system with an application control object to retrieve the parameter with.
 */
public class DBMSProperties {
    public final String NAME;
    public final String VERSION;
    public final List<ConnectionProperty> connectionProperties;

    class ConnectionProperty {
        public final String LABEL_TEXT;
        public final Callable<Control> newControlInstance;
        public final Callable<Method> getValue;

        public ConnectionProperty(String labelText, Callable<Control> newControlInstance, Callable<Method> getValue) {
            LABEL_TEXT = labelText;
            this.newControlInstance = newControlInstance;
            this.getValue = getValue;
        }
    }

    class TextFieldStringProperty extends ConnectionProperty {
        public TextFieldStringProperty(String labelText, String promptText, String defaultText) {
            super(labelText,
                    () -> {
                        TextField textField = new TextField();
                        textField.setPromptText(promptText);
                        textField.setText(defaultText);
                        return textField;
                    },
                    () -> TextField.class.getMethod("getText"));
        }
    }

    class ComboBoxStringProperty extends ConnectionProperty {
        public ComboBoxStringProperty(String labelText, String promptText, Collection<String> items) {
            super(labelText,
                    () -> {
                        ComboBox<String> comboBox = new ComboBox<>();
                        comboBox.getItems().addAll(items);
                        comboBox.setEditable(true);
                        comboBox.setPromptText(promptText);
                        comboBox.setValue(items.iterator().hasNext() ? items.iterator().next() : "");
                        return comboBox;
                    },
                    () -> ComboBox.class.getMethod("getValue"));
        }
    }

    public DBMSProperties(String name, String version) {
        NAME = name;
        VERSION = version;
        connectionProperties = new ArrayList<>();
    }
}
