package rdbuilder.dbconnector;

import rdbuilder.dbobject.DBObject;
import rdbuilder.dbobject.DBRoot;
import rdbuilder.dbobject.Schema;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.DOTALL;
import static java.util.regex.Pattern.MULTILINE;

public abstract class DBConnector implements Serializable {
    transient protected PreparedStatements preparedStatements;
    transient protected Connection connection;
    public Properties connectionProperties;
    protected DBRoot dbRoot;

    /**
     * A helper class to store implementations of {@link PreparedStatement}.
     */
    protected class PreparedStatements {
        final ArrayList<PreparedStatement> list = new ArrayList<>();

        public void clear() throws SQLException {
            for (PreparedStatement preparedStatement : list) {
                preparedStatement.close();
            }
            list.clear();
        }

        public PreparedStatement get(Integer key) {
            return list.get(key);
        }

        public int prepare(String sql) throws SQLException {
            list.add(connection.prepareStatement(sql));
            return list.size() - 1;
        }

    }

    public DBConnector() {
        connectionProperties = new Properties();
        connectionProperties.setProperty("ApplicationName", "RDBuilder");
    }

    /**
     * Attempts to establish a connection. Resets an existing connection.
     */
    public void connect() throws SQLException {
        disconnect();
        initializeAuxiliaryStructures();
        establishConnection();
    }

    /**
     * Closes the connection.
     */
    public void disconnect() {
        try {
            if (isConnected()) {
                connection.close();
            }
        } catch (SQLException e) {
            //ignore exception
        }
    }

    /**
     * Should not be called solely, is only a part of the whole conneciton process.
     */
    private void establishConnection() throws SQLException {
        connection = DriverManager.getConnection(getConnectionString(), connectionProperties);
        connection.setAutoCommit(false);
        if (connection.getTransactionIsolation() != Connection.TRANSACTION_NONE) {
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
        }
        prepareStatements();
    }

    /**
     * Parses SQL queries from a script delimited by ; and executes them using the connection.
     */
    public void executeSQLScript(String script, Schema remoteSchema) throws SQLException {
        setPath(remoteSchema);
        Pattern pattern = Pattern.compile("((/\\*).*?(\\*/))|(--.*?$)", DOTALL|MULTILINE);
        script = pattern.matcher(script).replaceAll("");
        Scanner scanner = new Scanner(script);
        scanner.useDelimiter(";");
        try {
            while (scanner.hasNext()) {
                String statement = scanner.next().trim();
                connection.createStatement().execute(statement);
            }
        } catch (SQLException e){
            connection.rollback();
        }
        connection.commit();
    }

    public Connection getConnection() {
        return connection;
    }

    /**
     * @return a valid JDBC connection string
     */
    protected abstract String getConnectionString();

    /**
     * @return parameters which the constructor was called with
     */
    public abstract Object[] getConstructorParameters();

    public DBRoot getDBObjectsRoot() {
        return dbRoot;
    }

    /**
     * @return default name of a connection to be used if it was not specified
     */
    public abstract String getDefaultDBRootName();

    /**
     * @return instance of the associated class extending {@link DBConnectorFactory}
     */
    public abstract DBConnectorFactory getFactory();

    /**
     * @return instance of the associated class extending {@link SQLGenerator}
     */
    public abstract SQLGenerator getSQLGenerator();

    /**
     * This method should be overridden to initialize any helper data structures
     * dependent on the connection.
     */
    public void initializeAuxiliaryStructures() throws SQLException {
        try {
            preparedStatements.clear();
        } catch (NullPointerException e) {
            preparedStatements = new PreparedStatements();
        }
    };

    public boolean isConnected() throws SQLException {
        return connection != null && !connection.isClosed();
    }

    /**
     * This method should be able to handle any database object and load it's content
     * using the connection.
     */
    public abstract void loadDBObjectContent(DBObject dbObject, boolean forceReload) throws SQLException;

    /**
     * This method should be overridden to initialize all prepared statements to be used.
     */
    public void prepareStatements() throws SQLException {
        requireConnection();
        initializeAuxiliaryStructures();
    }

    /**
     * This method should be called before any commands that require a working database connection.
     */
    protected void requireConnection() throws SQLException {
        if (!isConnected()) {
            connect();
        }
    }

    /**
     * Sets the path in the database system to be able to work with the specified database object.
     */
    public abstract void setPath(DBObject dbObject) throws SQLException;

}

