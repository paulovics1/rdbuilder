package rdbuilder.dbconnector;

import java.util.Collection;

public class GenericDBMSProperties extends DBMSProperties {

    public GenericDBMSProperties(String name, String version, Collection<String> initialDatabases, String defaultHost, String defaultPort) {
        super(name, version);
        connectionProperties.add(new TextFieldStringProperty("Host", "Server IP address or domain name", defaultHost));
        connectionProperties.add(new TextFieldStringProperty("Port", "Database server listener port", defaultPort));
        connectionProperties.add(new ComboBoxStringProperty("Database", "Initial database to connect to", initialDatabases));
    }
}
