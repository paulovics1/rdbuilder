package rdbuilder.dbconnector;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.lang.reflect.InvocationTargetException;

public class DBConnectorConverter implements Converter {
    @Override
    public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
        DBConnector dbConnector = (DBConnector) value;
        writer.startNode("factory-class-name");
        writer.setValue(dbConnector.getFactory().getClass().getName());
        writer.endNode();

        writer.startNode("connection-name");
        writer.setValue(dbConnector.getDBObjectsRoot().getName());
        writer.endNode();

        writer.startNode("connection-parameters");

        final Object[] parameters = dbConnector.getConstructorParameters();
        context.convertAnother(parameters);
        writer.endNode();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        reader.moveDown();
        String factoryClassName = reader.getValue();
        reader.moveUp();

        reader.moveDown();
        String connectionName = reader.getValue();
        reader.moveUp();

        DBConnectorFactory factory;
        try {
            Class<?> factoryClass = Thread.currentThread().getContextClassLoader().loadClass(factoryClassName);
            factory = (DBConnectorFactory) factoryClass.getMethod("getInstance").invoke(null);
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            return null;
        }

        reader.moveDown();
        Object[] connectionParameters = (Object[]) context.convertAnother(null, Object[].class);
        reader.moveUp();

        DBConnector dbConnector = factory.newDBConnectorInstance(connectionParameters);
        dbConnector.getDBObjectsRoot().setName(connectionName);
        return dbConnector;

    }

    @Override
    public boolean canConvert(Class aClass) {
        return DBConnector.class.isAssignableFrom(aClass);
    }
}
