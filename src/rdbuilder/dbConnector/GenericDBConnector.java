package rdbuilder.dbconnector;

import rdbuilder.Utils;
import rdbuilder.dbobject.*;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.NoSuchElementException;

/**
 * This class contains common methods and fields of relational database system connectors.
 */
public abstract class GenericDBConnector extends DBConnector implements Serializable {
    transient protected String schemaName;
    transient protected int PS_SELECT_DATABASE_SCHEMAS;
    transient protected int PS_SELECT_SCHEMA_TABLES_INFO;
    transient protected int PS_SELECT_SCHEMA_CONSTRAINTS_INFO;
    public final String SUBPROTOCOL;
    public final String SUBNAME;
    public final String HOST;
    public final String PORT;
    public final String INITIAL_DATABASE;

    protected String databaseName;

    public GenericDBConnector(String subprotocol, String subname, String host, String port, String initialDatabase, String username, String password) {
        super();
        this.SUBPROTOCOL = subprotocol;
        this.SUBNAME = subname;
        this.HOST = host;
        this.PORT = port;
        this.INITIAL_DATABASE = initialDatabase;
        connectionProperties.setProperty("user", username);
        connectionProperties.setProperty("password", password);
        dbRoot = new DBRoot(null, this);

        try {
            setDatabase(initialDatabase);
        } catch (SQLException ignore) {
        }
    }

    @Override
    public void loadDBObjectContent(DBObject dbObject, boolean forceReload) throws SQLException {
        if (dbObject instanceof DBRoot){
            loadDBRootContent((DBRoot) dbObject, forceReload);
        }
        else if (dbObject instanceof Database){
            loadDatabaseContent((Database) dbObject, forceReload);
        }
        else if (dbObject instanceof Schema){
            loadSchemaContent((Schema) dbObject, forceReload);
        }
        else if (dbObject instanceof Table){
            loadTableContent((Table) dbObject, forceReload);
        }
    }

    @Override
    public void prepareStatements() throws SQLException {
        super.prepareStatements();
        PS_SELECT_DATABASE_SCHEMAS = preparedStatements.prepare("SELECT schema_name FROM information_schema.schemata WHERE catalog_name = ?");
        PS_SELECT_SCHEMA_TABLES_INFO = preparedStatements.prepare("SELECT table_name FROM information_schema.tables WHERE upper(table_type) = 'BASE TABLE' AND table_catalog = ? AND table_schema = ?");
        PS_SELECT_SCHEMA_CONSTRAINTS_INFO = preparedStatements.prepare("SELECT c.constraint_name, c.table_name, c.constraint_type, c.constraint_catalog, c.constraint_schema, k.column_name, r.unique_constraint_name, c2.table_name as referenced_table_name " +
                "FROM information_schema.table_constraints c " +
                "LEFT OUTER JOIN information_schema.key_column_usage k ON (c.constraint_name = k.constraint_name AND " +
                "c.table_name = k.table_name AND c.constraint_schema = k.constraint_schema AND c.constraint_catalog = k.constraint_catalog) " +
                "LEFT OUTER JOIN information_schema.referential_constraints r ON (r.constraint_name = k.constraint_name AND " +
                "r.constraint_schema = k.constraint_schema AND r.constraint_catalog = k.constraint_catalog) " +
                "LEFT OUTER JOIN information_schema.table_constraints c2 ON (r.unique_constraint_name = c2.constraint_name AND " +
                "r.unique_constraint_schema = c2.constraint_schema AND r.unique_constraint_catalog = c2.constraint_catalog) " +
                "WHERE k.table_catalog = ? AND k.table_schema = ? " +
                "ORDER BY CASE WHEN UPPER(c.constraint_type) = 'FOREIGN KEY' THEN 1 ELSE 0 END, c.table_name, c.constraint_name, k.ordinal_position");
    }

    @Override
    public String getDefaultDBRootName() {
        return String.format("%s@%s:%s", getUsername(), HOST, PORT);
    }

    @Override
    public void setPath(DBObject dbObject) throws SQLException {
        requireConnection();
        String nDatabaseName, nSchemaName;
        if (dbObject instanceof Table) {
            nDatabaseName = dbObject.getParent().getParent().getName();
            nSchemaName = dbObject.getParent().getName();
        } else if (dbObject instanceof Schema) {
            nDatabaseName = dbObject.getParent().getName();
            nSchemaName = dbObject.getName();
        } else if (dbObject instanceof Database) {
            nDatabaseName = dbObject.getName();
            try {
                nSchemaName = ((Schema)((Database) dbObject)
                        .getSubTree().iterator().next()).getName();
            }
            catch (NoSuchElementException e){
                nSchemaName = "";
            }
        } else {
            return;
        }
        if (!databaseName.equals(nDatabaseName)) {
            setDatabase(nDatabaseName);
        }
        if (schemaName != null && !schemaName.equals(nSchemaName)) {
            setSchema(nSchemaName);
        }
    }

    @Override
    public Object[] getConstructorParameters() {
        return new Object[]{HOST,
                PORT,
                INITIAL_DATABASE,
                connectionProperties.get("user"),
                connectionProperties.get("password")};
    }

    /**
     * This is a helper method called during loading database constraints. Checks, if the
     * constraint differs from a constraint specified in the current row of result set.
     */
    protected boolean constraintDiffers(TableConstraint constraint, ResultSet resultSet, Class clazz) throws SQLException {
        return !(constraint != null && constraint.getClass().isAssignableFrom(clazz) &&
                constraint.getName().equals(resultSet.getString("constraint_name")) &&
                constraint.getTable().getName().equals(resultSet.getString("table_name")));
    }

    /**
     * The result set is a result of {@link java.sql.DatabaseMetaData#getColumns(String, String, String, String)}.
     * This method should return the data type of the column in the current row of result set.
     */
    public abstract String getColumnDataType(ResultSet row) throws SQLException;

    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * @return name of the database, which the current connection was initialized on
     */
    public String getInitialDatabaseName() {
        return INITIAL_DATABASE;
    }

    public String getPassword() {
        return connectionProperties.getProperty("password");
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getUsername() {
        return connectionProperties.getProperty("user");
    }

    protected void loadDBRootContent(DBRoot dbRoot, boolean forceReload) throws SQLException {
        if (forceReload || dbRoot.getSubTree().isEmpty()) {
            ResultSet databasesNames = selectConnectionDatabasesNames();
            dbRoot.getSubTree().clear();
            Utils.forEachRow(databasesNames, resultSet ->
                    dbRoot.addChild(new Database(dbRoot, resultSet.getString(1))));
        }
    }

    protected void loadDatabaseContent(Database database, boolean forceReload) throws SQLException {
        @SuppressWarnings("unchecked")
        Database<Schema> schemaDatabase = (Database<Schema>) database;
        if (forceReload || database.getSubTree().isEmpty()) {
            ResultSet schemasInfo = selectSchemasInfo(database);
            schemaDatabase.getSubTree().clear();
            Utils.forEachRow(schemasInfo, resultSet ->
                    schemaDatabase.addChild(new Schema(database, resultSet.getString("schema_name"))));
        }
    }

    protected void loadForeignKey(TableConstraint[] lastConstraintFinal, TableConstraint[] lastReferenceFinal, ResultSet resultSet) throws SQLException {
        final Database database = dbRoot.getChildByName(resultSet.getString("constraint_catalog"));
        final Schema schema = (Schema) database.getChildByName(resultSet.getString("constraint_schema"));
        if (constraintDiffers(lastConstraintFinal[0], resultSet, ForeignKey.class)){
            final Table table = schema.getChildByName(resultSet.getString("table_name"));
            final Table referencedTable = schema.getChildByName(resultSet.getString("referenced_table_name"));
            lastReferenceFinal[0] = referencedTable.getConstraintByName(resultSet.getString("unique_constraint_name"));
            lastConstraintFinal[0] = new ForeignKey(schema,
                    resultSet.getString("constraint_name"),
                    table,
                    referencedTable);
            schema.registerForeignKey((ForeignKey) lastConstraintFinal[0]);
        }
        ((ForeignKey) lastConstraintFinal[0]).addColumnPair(resultSet.getString("column_name"),
                lastReferenceFinal[0].getColumn(lastConstraintFinal[0].getColumns().size()).getName());
    }

    protected void loadPrimaryKey(TableConstraint[] lastConstraintFinal, ResultSet resultSet) throws SQLException {
        final Database database = dbRoot.getChildByName(resultSet.getString("constraint_catalog"));
        final Schema schema = (Schema) database.getChildByName(resultSet.getString("constraint_schema"));
        if (constraintDiffers(lastConstraintFinal[0], resultSet, PrimaryKey.class)){
            lastConstraintFinal[0] = new PrimaryKey(
                    resultSet.getString("constraint_name"),
                    resultSet.getString("table_name"),
                    schema);
            Table table = schema.getChildByName(resultSet.getString("table_name"));
            table.setPrimaryKey((PrimaryKey) lastConstraintFinal[0]);
            lastConstraintFinal[0] = table.getPrimaryKey();
        }
        ((PrimaryKey) lastConstraintFinal[0]).addColumn(resultSet.getString("column_name"));
    }

    protected void loadSchemaContent(Schema schema, boolean forceReload) throws SQLException {
        if (forceReload || (schema.getSubTree().isEmpty() && schema.getForeignKeys().isEmpty())) {
            ResultSet tablesInfo = selectTablesInfo(schema);
            schema.getSubTree().clear();
            Utils.forEachRow(tablesInfo, resultSet -> {
                Table table = new Table(schema, resultSet.getString("table_name"));
                schema.addChild(table);
                loadDBObjectContent(table, false);
            });

            ResultSet constraintsInfo = selectConstraintsInfo(schema);
            schema.clearForeignKeys();
            final TableConstraint[] lastConstraint = {null};
            final TableConstraint[] lastReference = {null};
            Utils.forEachRow(constraintsInfo, resultSet -> {
                switch (resultSet.getString("constraint_type").toUpperCase()){
                    case "PRIMARY KEY":
                        loadPrimaryKey(lastConstraint, resultSet);
                        break;
                    case "UNIQUE":
                        loadUnique(lastConstraint, resultSet);
                        break;
                    case "FOREIGN KEY":
                        loadForeignKey(lastConstraint, lastReference, resultSet);
                        break;
                }
            });
        }
    }

    protected void loadTableContent(Table table, boolean forceReload) throws SQLException {
        if (forceReload || table.getColumns().isEmpty()) {
            ResultSet columnsInfo = connection.getMetaData()
                    .getColumns(null, table.getParent().getName(), table.getName(), "%");
            table.getColumns().clear();
            Utils.forEachRow(columnsInfo, resultSet -> table.addColumn(
                    new Column(null,
                            resultSet.getString(4),
                            getColumnDataType(resultSet),
                            resultSet.getString(11).equals("NO"),
                            resultSet.getString(13)))
            );
        }
    }

    protected void loadUnique(TableConstraint[] lastConstraintFinal, ResultSet resultSet) throws SQLException {
        final Database database = dbRoot.getChildByName(resultSet.getString("constraint_catalog"));
        final Schema schema = (Schema) database.getChildByName(resultSet.getString("constraint_schema"));
        if (constraintDiffers(lastConstraintFinal[0], resultSet, UniqueConstraint.class)){
            final Table table = schema.getChildByName(resultSet.getString("table_name"));
            lastConstraintFinal[0] = new UniqueConstraint(table,
                    resultSet.getString("constraint_name"));
            table.registerUniqueConstraint((UniqueConstraint) lastConstraintFinal[0]);
        }
        ((UniqueConstraint) lastConstraintFinal[0]).addColumn(resultSet.getString("column_name"));
    }

    protected ResultSet selectConnectionDatabasesNames() throws SQLException {
        requireConnection();
        return connection.getMetaData().getCatalogs();
    }

    protected ResultSet selectConstraintsInfo(rdbuilder.dbobject.Schema schema) throws SQLException {
        setPath(schema);
        PreparedStatement preparedStatement = preparedStatements.get(PS_SELECT_SCHEMA_CONSTRAINTS_INFO);
        preparedStatement.setString(1, schema.getParent().getName());
        preparedStatement.setString(2, schema.getName());
        return preparedStatement.executeQuery();
    }

    protected ResultSet selectSchemasInfo(rdbuilder.dbobject.Database database) throws SQLException {
        setPath(database);
        PreparedStatement preparedStatement = preparedStatements.get(PS_SELECT_DATABASE_SCHEMAS);
        preparedStatement.setString(1, database.getName());
        return preparedStatement.executeQuery();
    }

    protected ResultSet selectTablesInfo(rdbuilder.dbobject.Schema schema) throws SQLException {
        setPath(schema);
        PreparedStatement preparedStatement = preparedStatements.get(PS_SELECT_SCHEMA_TABLES_INFO);
        preparedStatement.setString(1, schema.getParent().getName());
        preparedStatement.setString(2, schema.getName());
        return preparedStatement.executeQuery();
    }

    public void setDatabase(String databaseName) throws SQLException {
        if (isConnected()) {
            connection.setCatalog(databaseName);
            this.databaseName = databaseName;
        } else
            this.databaseName = databaseName;
    }

    public void setSchema(String schemaName) throws SQLException {
        if (isConnected()) {
            connection.setSchema(schemaName);
        }
        this.schemaName = schemaName;
    }
}

