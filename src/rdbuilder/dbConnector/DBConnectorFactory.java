package rdbuilder.dbconnector;

import java.lang.reflect.ParameterizedType;

public abstract class DBConnectorFactory<E extends DBConnector> {
    public final DBMSProperties DBMS_PROPERTIES;

    public DBConnectorFactory(DBMSProperties dbms_properties) {
        DBMS_PROPERTIES = dbms_properties;
    }

    /**
     * @return the associated class extending {@link DBConnector}
     */
    @SuppressWarnings("unchecked")
    public Class<E> getDBConnectorClass() {
        return (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * @return array of database names, that should exist in the respective database system by default
     */
    public abstract String[] getDefaultDatabases();

    /**
     * @return string containing number of the default listener port used by the respective database
     * system.
     */
    public abstract String getDefaultPort();

    /**
     * @return a new instance of the associated class extending {@link DBConnector}. A constructor
     * is called via reflection using specified parameters.
     */
    public abstract E newDBConnectorInstance(Object... args);
}
