package rdbuilder.dbconnector.external;

import rdbuilder.dbconnector.DBConnectorFactory;
import rdbuilder.dbconnector.GenericDBMSProperties;

import java.util.Arrays;

public class MySQLDBConnectorFactory extends DBConnectorFactory<MySQLDBConnector> {
    private static final String DBMS_NAME = "MySQL";
    private static final String[] DEFAULT_DATABASES = new String[]{"information_schema"};
    private static final String DEFAULT_PORT = "3306";
    private static final MySQLDBConnectorFactory instance = new MySQLDBConnectorFactory();
    private static final String VERSION = "5.6";

    private MySQLDBConnectorFactory() {
        super(new GenericDBMSProperties(DBMS_NAME,
                VERSION,
                Arrays.asList(DEFAULT_DATABASES),
                "",
                DEFAULT_PORT));
    }

    @Override
    public MySQLDBConnector newDBConnectorInstance(Object... args) {
        return new MySQLDBConnector((String) args[0], (String) args[1], (String) args[2], (String) args[3], (String) args[4]);
    }

    @Override
    public String[] getDefaultDatabases() {
        return DEFAULT_DATABASES;
    }

    @Override
    public String getDefaultPort() {
        return DEFAULT_PORT;
    }

    public static MySQLDBConnectorFactory getInstance(){
        return instance;
    }
}