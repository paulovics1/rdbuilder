package rdbuilder.dbconnector.external;

import rdbuilder.dbconnector.DBConnectorFactory;
import rdbuilder.dbconnector.GenericDBConnector;
import rdbuilder.dbconnector.SQLGenerator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;

public class MicrosoftSQLServerDBConnector extends GenericDBConnector {
    private final static String SUBPROTOCOL = "sqlserver";
    private final static String SUBNAME = "";
    private static final HashSet<Integer> NO_SCALE_RESTRICTION_CONSTANTS = new HashSet<>(
            Arrays.asList(new Integer[]{0}));
    private final static HashSet<String> typesNoAtt = new HashSet<>(Arrays.asList(
            new String[]{"bigint", "int", "smallint", "tinyint", "bit", "money",
            "smallmoney", "real", "datetime", "smalldatetime", "date", "text", "float",
            "ntext", "image", "sql_variant", "timestamp", "uniqueidentifier", "xml"}));
    private final static HashSet<String> typesWithSize = new HashSet<>(Arrays.asList(
            new String[]{"decimal", "numeric", "char", "varchar", "nchar",
                    "nvarchar", "binary", "varbinary"}));
    private final static HashSet<String> typesWithScale = new HashSet<>(Arrays.asList(
            new String[]{"decimal", "numeric", "time", "datetimeoffset"}));
    protected int PS_SELECT_CURRENT_SCHEMA_NAME;

    public MicrosoftSQLServerDBConnector(String host, String port, String initialDatabase, String username, String password) {
        super(SUBPROTOCOL, SUBNAME, host, port, initialDatabase, username, password);
    }

    @Override
    public void connect() throws SQLException {
        super.connect();
        schemaName = selectCurrentSchemaName();
    }

    @Override
    public DBConnectorFactory getFactory() {
        return MicrosoftSQLServerDBConnectorFactory.getInstance();
    }

    @Override
    public SQLGenerator getSQLGenerator() {
        return new MicrosoftSQLServerSQLGenerator();
    }

    @Override
    public String getConnectionString() {
        return String.format("jdbc:%s:%s//%s:%s;databaseName=%s", SUBPROTOCOL,
                SUBNAME,
                HOST,
                PORT,
                databaseName);
    }

    @Override
    public void prepareStatements() throws SQLException {
        super.prepareStatements();
        PS_SELECT_CURRENT_SCHEMA_NAME = preparedStatements.prepare("SELECT SCHEMA_NAME()");
    }

    @Override
    public String getColumnDataType(ResultSet resultSet) throws SQLException {
        String typeName = resultSet.getString(6).toLowerCase();
        Integer size = resultSet.getInt(7);

        String result = typeName;
        if (!typesNoAtt.contains(typeName)){
            final boolean withSize = typesWithSize.contains(typeName);
            final boolean withScale = typesWithScale.contains(typeName);
            Integer scale = resultSet.getInt(9);

            if (withSize){
                result += "(" + size;
                if (withScale && !NO_SCALE_RESTRICTION_CONSTANTS.contains(scale)){
                    result += "," + scale;
                }
                result += ")";
            }
            else if (withScale && !NO_SCALE_RESTRICTION_CONSTANTS.contains(scale)) {
                result += "(" + scale + ")";
            }
        }
        return result;
    }

    @Override
    public void setSchema(String schemaName) throws SQLException {
        this.schemaName = schemaName;
    }

    public String selectCurrentSchemaName() throws SQLException {
        requireConnection();
        ResultSet schemaNameResultSet = preparedStatements.get(PS_SELECT_CURRENT_SCHEMA_NAME).executeQuery();
        schemaNameResultSet.next();
        return schemaNameResultSet.getString(1);
    }
}
