package rdbuilder.dbconnector.external;

import rdbuilder.Utils;
import rdbuilder.dbconnector.DBConnectorFactory;
import rdbuilder.dbconnector.GenericDBConnector;
import rdbuilder.dbconnector.SQLGenerator;
import rdbuilder.dbobject.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;

public class MySQLDBConnector extends GenericDBConnector {
    private final static String SUBPROTOCOL = "mysql";
    private final static String SUBNAME = "";
    private final static HashSet<String> typesNoAtt = new HashSet<>(Arrays.asList(
            new String[]{"tinyint", "mediumint", "smallint", "int", "bigint", "tinytext",
            "text", "mediumtext", "longtext", "tinyblob", "blob", "mediumblob", "longblob",
            "enum", "set", "date", "datetime", "time", "timestamp", "year"}));
    private final static HashSet<String> typesWithSize = new HashSet<>(Arrays.asList(
            new String[]{"float", "double", "decimal", "bit", "char", "varchar", "binary", "varbinary"
            }));
    private final static HashSet<String> typesWithScale = new HashSet<>(Arrays.asList(
            new String[]{"float", "double", "decimal"}));
    private int PS_SELECT_CONNECTION_DATABASES_NAMES;

    public MySQLDBConnector(String host, String port, String initialDatabase, String username, String password) {
        super(SUBPROTOCOL, SUBNAME, host, port, initialDatabase, username, password);
        schemaName = initialDatabase;
    }

    @Override
    public String getColumnDataType(ResultSet resultSet) throws SQLException {
        String typeName = resultSet.getString(6).toLowerCase();
        String result = typeName;

        if (!typesNoAtt.contains(typeName)){
            final boolean withSize = typesWithSize.contains(typeName);
            final boolean withScale = typesWithScale.contains(typeName);
            Integer scale = resultSet.getInt(9);
            if (withScale && scale == 0)
                return typeName;
            if (withSize){
                Integer size = resultSet.getInt(7);
                result += "(" + size;
                if (withScale){
                    result += "," + scale;
                }
                result += ")";
            }
            else if (withScale) {
                result += "(" + scale + ")";
            }
        }
        return result;
    }

    @Override
    public DBConnectorFactory getFactory() {
        return MySQLDBConnectorFactory.getInstance();
    }

    @Override
    public SQLGenerator getSQLGenerator() {
        return new MySQLSQLGenerator();
    }

    @Override
    protected String getConnectionString() {
        return String.format("jdbc:%s:%s//%s:%s/%s", SUBPROTOCOL,
                SUBNAME,
                HOST,
                PORT,
                schemaName);
    }

    @Override
    public void prepareStatements() throws SQLException {
        super.prepareStatements();
        PS_SELECT_CONNECTION_DATABASES_NAMES = preparedStatements.prepare("SELECT DISTINCT catalog_name FROM information_schema.schemata");
        PS_SELECT_SCHEMA_CONSTRAINTS_INFO = preparedStatements.prepare("SELECT c.constraint_name, c.table_name, c.constraint_type, c.constraint_catalog, c.constraint_schema, k.column_name, k.referenced_table_name, k.referenced_column_name " +
                "FROM information_schema.table_constraints c " +
                "LEFT OUTER JOIN information_schema.key_column_usage k ON (c.constraint_name = k.constraint_name AND " +
                "c.table_name = k.table_name AND c.constraint_schema = k.constraint_schema AND c.constraint_catalog = k.constraint_catalog) " +
                "WHERE k.table_catalog = ? AND k.table_schema = ? " +
                "ORDER BY CASE WHEN UPPER(c.constraint_type) = 'FOREIGN KEY' THEN 1 ELSE 0 END, c.table_name, c.constraint_name, k.ordinal_position");
    }

    @Override
    protected void loadForeignKey(TableConstraint[] lastConstraintFinal, TableConstraint[] lastReferenceFinal, ResultSet resultSet) throws SQLException {
        final Database database = dbRoot.getChildByName(resultSet.getString("constraint_catalog"));
        final Schema schema = (Schema) database.getChildByName(resultSet.getString("constraint_schema"));
        if (constraintDiffers(lastConstraintFinal[0], resultSet, ForeignKey.class)){
            final Table table = schema.getChildByName(resultSet.getString("table_name"));
            final Table referencedTable = schema.getChildByName(resultSet.getString("referenced_table_name"));
            lastConstraintFinal[0] = new ForeignKey(schema,
                    resultSet.getString("constraint_name"),
                    table,
                    referencedTable);
            schema.registerForeignKey((ForeignKey) lastConstraintFinal[0]);
        }
        ((ForeignKey)lastConstraintFinal[0]).addColumnPair(resultSet.getString("column_name"),
                resultSet.getString("referenced_column_name"));
    }

    @Override
    protected ResultSet selectConnectionDatabasesNames() throws SQLException {
        requireConnection();
        return preparedStatements.get(PS_SELECT_CONNECTION_DATABASES_NAMES).executeQuery();
    }

    @Override
    public void setDatabase(String databaseName) throws SQLException {
        this.databaseName = databaseName;
    }

    @Override
    public void setSchema(String schemaName) throws SQLException {
        this.schemaName = schemaName;
        if (!isConnected())
            connect();
    }

    @Override
    protected void loadTableContent(Table table, boolean forceReload) throws SQLException {
        if (forceReload || table.getColumns().isEmpty()) {
            ResultSet columnsInfo = connection.getMetaData()
                    .getColumns(table.getParent().getName(), null, table.getName(), "%");
            table.getColumns().clear();
            Utils.forEachRow(columnsInfo, resultSet -> table.addColumn(
                    new Column(null,
                            resultSet.getString(4),
                            getColumnDataType(resultSet),
                            resultSet.getString(11).equals("NO"),
                            resultSet.getString(13)))
            );
        }
    }
}
