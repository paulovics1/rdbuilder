package rdbuilder.dbconnector.external;

import rdbuilder.Utils;
import rdbuilder.dbconnector.DBConnectorFactory;
import rdbuilder.dbconnector.GenericDBConnector;
import rdbuilder.dbconnector.SQLGenerator;
import rdbuilder.dbobject.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class OracleDBConnector extends GenericDBConnector {
    private final static String DRIVER = "thin";
    private final static String SUBPROTOCOL = "oracle";
    private final static String SUBNAME = "";
    private static final HashSet<Integer> NO_ARGS_RESTRICTION_SCALES = new HashSet<>(
            Arrays.asList(new Integer[]{-127}));
    private static final HashSet<Integer> NO_SCALE_RESTRICTION_CONSTANTS = new HashSet<>(
            Arrays.asList(new Integer[]{0}));
    private static final HashMap<Integer, String> floatSizeTypeMap = new HashMap<>();
    private final static HashSet<String> typesNoAtt = new HashSet<>(Arrays.asList(
            new String[]{"date", "bfile", "blob", "clob", "nclob", "rowid", "urowid",
            "long", "long raw"}));
    private final static HashSet<String> typesWithSize = new HashSet<>(Arrays.asList(
            new String[]{"char", "nchar", "nvarchar2", "raw", "number", "float"}));
    private final static HashSet<String> typesWithScale = new HashSet<>(Arrays.asList(
            new String[]{"number"}));

    static {
        floatSizeTypeMap.put(63, "real");
        floatSizeTypeMap.put(126, "double precision");
    }

    private int PS_SELECT_CONNECTION_DATABASES_NAMES;

    public OracleDBConnector(String host, String port, String initialDatabase, String username, String password) {
        super(SUBPROTOCOL, SUBNAME, host, port, initialDatabase, username, password);
    }

    @Override
    protected void loadSchemaContent(Schema schema, boolean forceReload) throws SQLException {
        if (forceReload || schema.getSubTree().isEmpty() && schema.getForeignKeys().isEmpty()) {
            ResultSet tablesInfo = selectTablesInfo(schema);
            schema.getSubTree().clear();
            Utils.forEachRow(tablesInfo, resultSet -> {
                Table table = new Table(schema, resultSet.getString("table_name"));
                schema.addChild(table);
                loadTableContent(table, false);
            });

            ResultSet constraintsInfo = selectConstraintsInfo(schema);
            schema.clearForeignKeys();
            final TableConstraint[] lastConstraint = {null};
            final HashMap<String, TableConstraint> tableConstraintMap = new HashMap<>();
            Utils.forEachRow(constraintsInfo, resultSet -> {
                switch (resultSet.getString("constraint_type").toUpperCase()){
                    case "P":
                        if (constraintDiffers(lastConstraint[0], resultSet, PrimaryKey.class)){
                            lastConstraint[0] = new PrimaryKey(
                                    resultSet.getString("constraint_name"),
                                    resultSet.getString("table_name"),
                                    schema);
                            final Table table = schema.getChildByName(resultSet.getString("table_name"));
                            table.setPrimaryKey((PrimaryKey) lastConstraint[0]);
                            lastConstraint[0] = table.getPrimaryKey();
                            tableConstraintMap.put(resultSet.getString("constraint_name"), lastConstraint[0]);
                        }
                        ((PrimaryKey)lastConstraint[0]).addColumn(resultSet.getString("column_name"));
                        break;
                    case "U":
                        if (constraintDiffers(lastConstraint[0], resultSet, UniqueConstraint.class)){
                            final Table table = schema.getChildByName(resultSet.getString("table_name"));
                            lastConstraint[0] = new UniqueConstraint(table,
                                    resultSet.getString("constraint_name"));
                            table.registerUniqueConstraint((UniqueConstraint) lastConstraint[0]);
                            tableConstraintMap.put(resultSet.getString("constraint_name"), lastConstraint[0]);
                        }
                        ((UniqueConstraint)lastConstraint[0]).addColumn(resultSet.getString("column_name"));
                        break;
                    case "R":
                        if (constraintDiffers(lastConstraint[0], resultSet, ForeignKey.class)){
                            final Table table = schema.getChildByName(resultSet.getString("table_name"));
                            final Table referencedTable = tableConstraintMap.get(resultSet.getString("r_constraint_name")).getTable();

                            lastConstraint[0] = new ForeignKey(schema,
                                    resultSet.getString("constraint_name"),
                                    table,
                                    referencedTable);
                            schema.registerForeignKey((ForeignKey) lastConstraint[0]);
                        }
                        TableConstraint referencedConstraint = tableConstraintMap.get(resultSet.getString("r_constraint_name"));
                        ((ForeignKey)lastConstraint[0]).addColumnPair(resultSet.getString("column_name"),
                                referencedConstraint.getColumn(lastConstraint[0].getColumns().size()).getName());
                        break;
                }
            });
        }
    }

    @Override
    public String getColumnDataType(ResultSet resultSet) throws SQLException {
        String typeName = resultSet.getString(6).toLowerCase();
        Integer size = resultSet.getInt(7);

        if (typeName.equals("float") && floatSizeTypeMap.containsKey(size))
            typeName = floatSizeTypeMap.get(size);

        String result = typeName;
        if (!typesNoAtt.contains(typeName)){
            final boolean withSize = typesWithSize.contains(typeName);
            final boolean withScale = typesWithScale.contains(typeName);
            Integer scale = resultSet.getInt(9);

            if (withScale && NO_ARGS_RESTRICTION_SCALES.contains(scale))
                return result;

            if (withSize){
                result += "(" + size;
                if (withScale && !NO_SCALE_RESTRICTION_CONSTANTS.contains(scale)){
                    result += "," + scale;
                }
                result += ")";
            }
            else if (withScale && !NO_SCALE_RESTRICTION_CONSTANTS.contains(scale)) {
                result += "(" + scale + ")";
            }
        }
        return result;
    }

    @Override
    public void connect() throws SQLException {
        super.connect();
        schemaName = getUsername().toUpperCase();
    }

    @Override
    public DBConnectorFactory getFactory() {
        return OracleDBConnectorFactory.getInstance();
    }

    @Override
    public SQLGenerator getSQLGenerator() {
        return new OracleSQLGenerator();
    }

    @Override
    public String getConnectionString() {
        return String.format("jdbc:%s:%s:@%s:%s:%s", SUBPROTOCOL,
                DRIVER,
                HOST,
                PORT,
                databaseName);
    }

    @Override
    public void prepareStatements() throws SQLException {
        PS_SELECT_CONNECTION_DATABASES_NAMES = preparedStatements.prepare("SELECT sys_context('userenv','instance_name') FROM dual");
        PS_SELECT_DATABASE_SCHEMAS = preparedStatements.prepare("SELECT ? AS catalog_name, username as schema_name FROM dba_users");
        PS_SELECT_SCHEMA_TABLES_INFO = preparedStatements.prepare("SELECT ? AS table_catalog, owner AS table_schema, table_name FROM dba_tables WHERE owner = ?");
        PS_SELECT_SCHEMA_CONSTRAINTS_INFO = preparedStatements.prepare("SELECT * FROM all_constraints a INNER JOIN all_cons_columns b " +
                "ON (a.constraint_name = b.constraint_name AND a.table_name = b.table_name) WHERE a.owner = ? " +
                "ORDER BY CASE WHEN UPPER(a.constraint_type) = 'R' THEN 1 ELSE 0 END, a.table_name, a.constraint_name, b.position");
    }

    @Override
    protected ResultSet selectConstraintsInfo(rdbuilder.dbobject.Schema schema) throws SQLException {
        setPath(schema);
        PreparedStatement preparedStatement = preparedStatements.get(PS_SELECT_SCHEMA_CONSTRAINTS_INFO);
        preparedStatement.setString(1, schema.getName());
        return preparedStatement.executeQuery();
    }

    @Override
    public ResultSet selectConnectionDatabasesNames() throws SQLException {
        requireConnection();
        return preparedStatements.get(PS_SELECT_CONNECTION_DATABASES_NAMES).executeQuery();
    }

    @Override
    public void setSchema(String schemaName) throws SQLException {
        this.schemaName = schemaName;
    }


}
