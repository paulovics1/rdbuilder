package rdbuilder.dbconnector.external;

import rdbuilder.dbconnector.GenericSQLGenerator;
import rdbuilder.dbobject.Schema;

public class OracleSQLGenerator extends GenericSQLGenerator {
    public OracleSQLGenerator(Schema schema) {
        super(schema);
    }

    public OracleSQLGenerator() {
        super();
    }

    @Override
    public String getSQLSyntaxName() {
        return "Oracle";
    }
}
