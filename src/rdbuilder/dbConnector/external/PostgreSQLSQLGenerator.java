package rdbuilder.dbconnector.external;

import rdbuilder.dbconnector.GenericSQLGenerator;
import rdbuilder.dbobject.Schema;

public class PostgreSQLSQLGenerator extends GenericSQLGenerator {
    public PostgreSQLSQLGenerator(Schema schema) {
        super(schema);
    }

    public PostgreSQLSQLGenerator() {
        super();
    }

    @Override
    public String getSQLSyntaxName() {
        return "PostgreSQL";
    }
}
