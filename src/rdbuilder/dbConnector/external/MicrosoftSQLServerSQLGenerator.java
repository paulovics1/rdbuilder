package rdbuilder.dbconnector.external;

import javafx.util.Pair;
import rdbuilder.dbconnector.GenericSQLGenerator;
import rdbuilder.dbobject.Schema;

public class MicrosoftSQLServerSQLGenerator extends GenericSQLGenerator {
    public MicrosoftSQLServerSQLGenerator(Schema schema) {
        super(schema);
    }

    public MicrosoftSQLServerSQLGenerator() {
        super();
    }

    @Override
    public String getSQLSyntaxName() {
        return "Microsoft SQL Server";
    }

    @Override
    protected Pair<String, String> getIdentifierQuotes() {
        return new Pair<>("[", "]");
    }
}
