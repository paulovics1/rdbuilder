package rdbuilder.dbconnector.external;

import rdbuilder.dbconnector.DBConnectorFactory;
import rdbuilder.dbconnector.GenericDBMSProperties;

import java.util.Arrays;

public class PostgreSQLDBConnectorFactory extends DBConnectorFactory<PostgreSQLDBConnector> {
    private static final String DBMS_NAME = "PostgreSQL";
    private static final String[] DEFAULT_DATABASES = new String[]{"postgres", "template0", "template1"};
    private static final String DEFAULT_PORT = "5432";
    private static final PostgreSQLDBConnectorFactory instance = new PostgreSQLDBConnectorFactory();
    private static final String VERSION = "9.4";

    private PostgreSQLDBConnectorFactory() {
        super(new GenericDBMSProperties(DBMS_NAME,
                VERSION,
                Arrays.asList(DEFAULT_DATABASES),
                "",
                DEFAULT_PORT));
    }

    @Override
    public PostgreSQLDBConnector newDBConnectorInstance(Object... args) {
        return new PostgreSQLDBConnector((String) args[0], (String) args[1], (String) args[2], (String) args[3], (String) args[4]);
    }

    @Override
    public String[] getDefaultDatabases() {
        return DEFAULT_DATABASES;
    }

    @Override
    public String getDefaultPort() {
        return DEFAULT_PORT;
    }

    public static PostgreSQLDBConnectorFactory getInstance() {
        return instance;
    }
}