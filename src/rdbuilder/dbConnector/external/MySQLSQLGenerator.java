package rdbuilder.dbconnector.external;

import javafx.util.Pair;
import rdbuilder.dbconnector.GenericSQLGenerator;
import rdbuilder.dbobject.Schema;

public class MySQLSQLGenerator extends GenericSQLGenerator {
    public MySQLSQLGenerator(Schema schema) {
        super(schema);
    }

    public MySQLSQLGenerator() {
        super();
    }

    @Override
    public String getSQLSyntaxName() {
        return "MySQL";
    }

    @Override
    protected Pair<String, String> getIdentifierQuotes() {
        return new Pair<>("`", "`");
    }

    @Override
    protected void appendContentBefore() {
        super.appendContentBefore();
        sqlBuilder.append("SET foreign_key_checks = 0;");
        sqlBuilder.append(System.lineSeparator());
        sqlBuilder.append(System.lineSeparator());
    }

    @Override
    protected void appendContentAfter() {
        sqlBuilder.append("SET foreign_key_checks = 1;");
    }

}
