package rdbuilder.dbconnector.external;

import rdbuilder.dbconnector.DBConnectorFactory;
import rdbuilder.dbconnector.GenericDBMSProperties;

import java.util.Arrays;

public class MicrosoftSQLServerDBConnectorFactory extends DBConnectorFactory<MicrosoftSQLServerDBConnector> {
    private static final String DBMS_NAME = "Microsoft SQL Server";
    private static final String[] DEFAULT_DATABASES = new String[]{"master"};
    private static final String DEFAULT_PORT = "1433";
    private static final MicrosoftSQLServerDBConnectorFactory instance = new MicrosoftSQLServerDBConnectorFactory();
    private static final String VERSION = "2014";

    private MicrosoftSQLServerDBConnectorFactory() {
        super(new GenericDBMSProperties(DBMS_NAME,
                VERSION,
                Arrays.asList(DEFAULT_DATABASES),
                "",
                DEFAULT_PORT));
    }

    @Override
    public String[] getDefaultDatabases() {
        return DEFAULT_DATABASES;
    }

    @Override
    public String getDefaultPort() {
        return DEFAULT_PORT;
    }

    @Override
    public MicrosoftSQLServerDBConnector newDBConnectorInstance(Object... args) {
        return new MicrosoftSQLServerDBConnector((String) args[0], (String) args[1], (String) args[2], (String) args[3], (String) args[4]);
    }

    public static MicrosoftSQLServerDBConnectorFactory getInstance(){
        return instance;
    }
}
