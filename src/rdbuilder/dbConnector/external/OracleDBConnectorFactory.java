package rdbuilder.dbconnector.external;

import rdbuilder.dbconnector.DBConnectorFactory;
import rdbuilder.dbconnector.GenericDBMSProperties;

import java.util.Arrays;

public class OracleDBConnectorFactory extends DBConnectorFactory<OracleDBConnector> {
    private static final String DBMS_NAME = "Oracle";
    private static final String[] DEFAULT_DATABASES = new String[]{"orcl"};
    private static final String DEFAULT_PORT = "1521";
    private static final OracleDBConnectorFactory instance = new OracleDBConnectorFactory();
    private static final String VERSION = "12.1.0";

    private OracleDBConnectorFactory() {
        super(new GenericDBMSProperties(DBMS_NAME,
                VERSION,
                Arrays.asList(DEFAULT_DATABASES),
                "",
                DEFAULT_PORT));
    }

    @Override
    public OracleDBConnector newDBConnectorInstance(Object... args) {
        return new OracleDBConnector((String) args[0], (String) args[1], (String) args[2], (String) args[3], (String) args[4]);
    }

    @Override
    public String[] getDefaultDatabases() {
        return DEFAULT_DATABASES;
    }

    @Override
    public String getDefaultPort() {
        return DEFAULT_PORT;
    }

    public static OracleDBConnectorFactory getInstance() {
        return instance;
    }
}