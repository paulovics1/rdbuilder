package rdbuilder.dbconnector.external;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import rdbuilder.Utils;
import rdbuilder.dbconnector.DBConnectorFactory;
import rdbuilder.dbconnector.GenericDBConnector;
import rdbuilder.dbconnector.SQLGenerator;
import rdbuilder.dbobject.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class PostgreSQLDBConnector extends GenericDBConnector {
    private final static String SUBPROTOCOL = "postgresql";
    private final static String SUBNAME = "";
    private static final HashSet<Integer> NO_SIZE_RESTRICTION_CONSTANTS = new HashSet<>(
            Arrays.asList(new Integer[]{131089, 2147483647, 0}));
    private static final HashMap<String, String> serialTypeMap = new HashMap<>();
    private final static HashSet<String> typesNoAtt = new HashSet<>(Arrays.asList(
            new String[]{"int8", "serial8", "bool", "box", "bytea", "cidr", "circle",
                    "date", "float8", "inet", "int4", "json", "jsonb", "line", "lseg",
                    "macaddr", "money", "path", "pg_lsn", "point", "polygon",
                    "float4", "int2", "serial2", "serial4", "tsquery", "ts_vector",
                    "txid_snapshot", "uuid", "xml"}));
    private final static HashSet<String> typesWithSize = new HashSet<>(Arrays.asList(
            new String[]{"bit", "varbit", "char", "varchar", "numeric"}));
    private final static HashSet<String> typesWithScale = new HashSet<>(Arrays.asList(
            new String[]{"numeric", "time", "timetz", "timestamp", "timestamptz"}));

    static {
        serialTypeMap.put("int2", "smallserial");
        serialTypeMap.put("int4", "serial");
        serialTypeMap.put("int8", "bigserial");
    }

    private int PS_SELECT_CONNECTION_DATABASES_NAMES;
    private int PS_SELECT_SCHEMA_CONSTRAINTS_INFO;
    private int PS_SELECT_TABLE_COLUMNS_ATTNUM;
    private BiMap<DBObject, Integer> dbObjectOIDMap;
    private HashMap<Table, BiMap<Column, Integer>> tableColumnAttnumMap;


    public PostgreSQLDBConnector(String host, String port, String initialDatabase, String username, String password) {
        super(SUBPROTOCOL, SUBNAME, host, port, initialDatabase, username, password);
    }

    @Override
    protected void loadDatabaseContent(Database database, boolean forceReload) throws SQLException {
        @SuppressWarnings("unchecked")
        Database<Schema> schemaDatabase = (Database<Schema>) database;
        if (forceReload || database.getSubTree().isEmpty()) {
            ResultSet schemasInfo = selectSchemasInfo(database);
            schemaDatabase.getSubTree().clear();
            Utils.forEachRow(schemasInfo, resultSet -> {
                Schema schema = new Schema(schemaDatabase, resultSet.getString("schema_name"));
                schemaDatabase.addChild(schema);
                dbObjectOIDMap.forcePut(schema, resultSet.getInt("oid"));
            });
        }
    }

    @Override
    protected void loadSchemaContent(Schema schema, boolean forceReload) throws SQLException {
        if (forceReload || (schema.getSubTree().isEmpty() && schema.getForeignKeys().isEmpty())) {
            ResultSet tablesInfo = selectTablesInfo(schema);
            schema.getSubTree().clear();
            Utils.forEachRow(tablesInfo, resultSet -> {
                Table table = new Table(schema, resultSet.getString("table_name"));
                schema.addChild(table);
                dbObjectOIDMap.forcePut(table, resultSet.getInt("oid"));
                loadDBObjectContent(table, false);
            });

            ResultSet constraintsInfo = selectConstraintsInfo(schema);
            schema.clearForeignKeys();
            Utils.forEachRow(constraintsInfo, resultSet -> {
                TableConstraint tableConstraint = null;
                Table table = (Table) dbObjectOIDMap.inverse().get(resultSet.getInt("conrelid"));
                switch (resultSet.getString("contype").charAt(0)){
                    case 'p':
                        tableConstraint = new PrimaryKey(table,
                                resultSet.getString("conname"));
                        loadConstraintColumns(tableConstraint, (Number[]) resultSet.getArray("conkey").getArray());
                        table.setPrimaryKey((PrimaryKey) tableConstraint);
                        tableConstraint = table.getPrimaryKey();
                        break;
                    case 'u':
                        tableConstraint = new UniqueConstraint(table,
                                resultSet.getString("conname"));
                        table.registerUniqueConstraint((UniqueConstraint) tableConstraint);
                        loadConstraintColumns(tableConstraint, (Number[]) resultSet.getArray("conkey").getArray());
                        break;
                    case 'f':
                        Table referencedTable = (Table) dbObjectOIDMap.inverse().get(resultSet.getInt("confrelid"));
                        tableConstraint = new ForeignKey(schema,
                                resultSet.getString("conname"),
                                table,
                                referencedTable);
                        schema.registerForeignKey((ForeignKey) tableConstraint);

                        loadForeignKeyColumns((ForeignKey) tableConstraint, (Number[]) resultSet.getArray("conkey").getArray(),
                                (Number[]) resultSet.getArray("confkey").getArray());
                        break;
                }
                if (tableConstraint != null) {
                    dbObjectOIDMap.forcePut(tableConstraint, resultSet.getInt("oid"));
                }
            });
        }
    }

    @Override
    protected void loadTableContent(Table table, boolean forceReload) throws SQLException {
        if (forceReload || table.getColumns().isEmpty()) {
            ResultSet columnsInfo = selectColumnsAttnum(table);
            tableColumnAttnumMap.putIfAbsent(table, HashBiMap.create());
            HashBiMap<Column, Integer> biMap = (HashBiMap<Column, Integer>) tableColumnAttnumMap.get(table);
            table.getColumns().clear();
            ResultSet columnMetaData = connection.getMetaData().getColumns(null, table.getParent().getName(), table.getName(), "%");
            Utils.forEachRow(columnMetaData, resultSet -> {
                Column column = table.addColumn(new Column(null,
                        resultSet.getString(4),
                        getColumnDataType(resultSet),
                        resultSet.getString(11).equals("NO"),
                        resultSet.getString(13)));

                if (column.getDataType().toLowerCase().matches(".*serial"))
                    column.setDefaultValue(null);

                columnsInfo.next();
                biMap.forcePut(column, columnsInfo.getInt("attnum"));
            });
        }
    }

    @Override
    public String getColumnDataType(ResultSet resultSet) throws SQLException {
        String typeName = resultSet.getString(6).toLowerCase();
        boolean autoIncrement = resultSet.getString(23).equals("YES");
        if (autoIncrement && serialTypeMap.containsKey(typeName))
            typeName = serialTypeMap.get(typeName);

        String result = typeName;
        if (!typesNoAtt.contains(typeName)){
            final boolean withSize = typesWithSize.contains(typeName);
            final boolean withScale = typesWithScale.contains(typeName);
            Integer scale = resultSet.getInt(9);
            if (withSize){
                Integer size = resultSet.getInt(7);
                if (NO_SIZE_RESTRICTION_CONSTANTS.contains(size))
                    return result;
                result += "(" + size;
                if (withScale){
                    result += "," + scale;
                }
                result += ")";
            }
            else if (withScale) {
                result += "(" + scale + ")";
            }
        }
        return result;
    }

    @Override
    public void connect() throws SQLException {
        super.connect();
        schemaName = connection.getSchema();
    }

    @Override
    public DBConnectorFactory getFactory() {
        return PostgreSQLDBConnectorFactory.getInstance();
    }

    @Override
    public SQLGenerator getSQLGenerator() {
        return new PostgreSQLSQLGenerator();
    }

    @Override
    protected String getConnectionString() {
        return String.format("jdbc:%s:%s//%s:%s/%s", SUBPROTOCOL,
                SUBNAME,
                HOST,
                PORT,
                databaseName);
    }

    @Override
    public void prepareStatements() throws SQLException {
        super.prepareStatements();
        PS_SELECT_CONNECTION_DATABASES_NAMES = preparedStatements.prepare("SELECT datname FROM pg_database");
        PS_SELECT_DATABASE_SCHEMAS = preparedStatements.prepare("SELECT nspname AS schema_name, oid FROM pg_catalog.pg_namespace");
        PS_SELECT_SCHEMA_TABLES_INFO = preparedStatements.prepare("SELECT c.oid, i.table_name FROM information_schema.tables i " +
                "JOIN pg_catalog.pg_class c ON (i.table_name = c.relname AND c.relnamespace = ?) " +
                "WHERE i.table_catalog = ? AND i.table_schema = ? AND upper(i.table_type) = 'BASE TABLE'");
        PS_SELECT_TABLE_COLUMNS_ATTNUM = preparedStatements.prepare("SELECT a.attnum " +
                "FROM information_schema.columns c " +
                "JOIN pg_catalog.pg_attribute a ON (c.column_name, a.attrelid) = (a.attname, ?) " +
                "WHERE c.table_catalog = ? AND c.table_schema = ? AND c.table_name = ? " +
                "ORDER BY ordinal_position ASC");
        PS_SELECT_SCHEMA_CONSTRAINTS_INFO = preparedStatements.prepare("SELECT oid, * FROM pg_catalog.pg_constraint WHERE connamespace = ?");
    }

    protected ResultSet selectConstraintsInfo(rdbuilder.dbobject.Schema schema) throws SQLException {
        setPath(schema);
        PreparedStatement preparedStatement = preparedStatements.get(PS_SELECT_SCHEMA_CONSTRAINTS_INFO);
        preparedStatement.setInt(1, dbObjectOIDMap.get(schema));
        return preparedStatement.executeQuery();
    }

    @Override
    protected ResultSet selectConnectionDatabasesNames() throws SQLException {
        requireConnection();
        return preparedStatements.get(PS_SELECT_CONNECTION_DATABASES_NAMES).executeQuery();
    }

    @Override
    protected ResultSet selectSchemasInfo(rdbuilder.dbobject.Database database) throws SQLException {
        setPath(database);
        PreparedStatement preparedStatement = preparedStatements.get(PS_SELECT_DATABASE_SCHEMAS);
        return preparedStatement.executeQuery();
    }

    @Override
    public void setDatabase(String databaseName) throws SQLException {
        this.databaseName = databaseName;
        if (isConnected())
            connect();
    }

    @Override
    protected ResultSet selectTablesInfo(rdbuilder.dbobject.Schema schema) throws SQLException {
        setPath(schema);
        PreparedStatement preparedStatement = preparedStatements.get(PS_SELECT_SCHEMA_TABLES_INFO);
        preparedStatement.setInt(1, dbObjectOIDMap.get(schema));
        preparedStatement.setString(2, schema.getParent().getName());
        preparedStatement.setString(3, schema.getName());
        return preparedStatement.executeQuery();
    }

    @Override
    public void initializeAuxiliaryStructures() throws SQLException {
        super.initializeAuxiliaryStructures();
        if (dbObjectOIDMap != null)
            dbObjectOIDMap.clear();
        dbObjectOIDMap = HashBiMap.create();

        if (tableColumnAttnumMap != null)
            tableColumnAttnumMap.clear();
        tableColumnAttnumMap = new HashMap<>();
    }

    public void loadConstraintColumns(TableConstraint tableConstraint, Number[] conkey) throws SQLException {
        tableConstraint.clearColumns();
        BiMap<Integer, Column> tableBiMap = tableColumnAttnumMap.get(tableConstraint.getTable()).inverse();
        for (Number attnum : conkey){
            tableConstraint.addColumn(tableBiMap.get(attnum.intValue()));
        }
    }

    private void loadForeignKeyColumns(ForeignKey foreignKey, Number[] conkeys, Number[] confkeys) {
        foreignKey.clearColumns();
        BiMap<Integer, Column> tableBiMap = tableColumnAttnumMap.get(foreignKey.getTable()).inverse();
        BiMap<Integer, Column> refBiMap = tableColumnAttnumMap.get(foreignKey.getReferencedTable()).inverse();

        for (int i = 0; i < conkeys.length; i++){
            foreignKey.addColumnPair(tableBiMap.get(conkeys[i]), refBiMap.get(confkeys[i]));
        }
    }

    protected ResultSet selectColumnsAttnum(rdbuilder.dbobject.Table table) throws SQLException {
        setPath(table);
        PreparedStatement preparedStatement = preparedStatements.get(PS_SELECT_TABLE_COLUMNS_ATTNUM);
        preparedStatement.setInt(1, dbObjectOIDMap.get(table));
        preparedStatement.setString(2, table.getParent().getParent().getName());
        preparedStatement.setString(3, table.getParent().getName());
        preparedStatement.setString(4, table.getName());
        return preparedStatement.executeQuery();
    }
}
