package rdbuilder.dbconnector;

import rdbuilder.Utils;
import rdbuilder.dbobject.Schema;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class SQLGenerator {
    protected Schema schema;

    protected SQLGenerator(Schema schema) {
        this.schema = schema;
    }

    protected SQLGenerator(){};

    @Override
    public String toString() {
        return getSQLSyntaxName();
    }

    /**
     * The whole process of SQL script generation.
     * @return SQL script in a string
     */
    public abstract String generateSQL();

    /**
     * @return the name of the result SQL syntax, which this SQL generator should be identified with
     */
    public abstract String getSQLSyntaxName();

    public void saveSQLDumpToFile(File file){
        try {
            FileWriter writer = new FileWriter(file, false);
            writer.write(generateSQL());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            Utils.showError(e, "I/O error", "It was not possible to save SQL dump to the specified file.");
        }
    }

    /**
     * @param schema the schema to generate the SQL script from
     */
    public void setSchema(Schema schema) {
        this.schema = schema;
    }
}
