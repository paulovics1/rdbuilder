package rdbuilder.dbobject;

import java.util.Collection;
import java.util.Iterator;

public interface DBObjectObserver {
    Collection<DBObject> getObservedDBObjects();

    void onObservedDestroyed(DBObject observed);

    default void onObservedReplaced(DBObject observed, DBObject replacement){
        unregisterObserved(observed);
        registerObserved(replacement);
    };

    default void registerObserved(DBObject observed){
        if (observed != null) {
            getObservedDBObjects().add(observed);
            observed.dbObjectObservers.add(this);
        }
    }

        default void replaceWithObserver(DBObjectObserver replacement){
        Iterator<DBObject> observedIterator = getObservedDBObjects().iterator();
        while (observedIterator.hasNext()){
            DBObject observed = observedIterator.next();
            unregisterObserved(observed);
            observedIterator.remove();
            replacement.registerObserved(observed);
        }
    };

    default void unregisterAll(){
        Iterator<DBObject> observedIterator = getObservedDBObjects().iterator();
        while (observedIterator.hasNext()){
            observedIterator.next().dbObjectObservers.remove(this);
            observedIterator.remove();
        }
    }

    default void unregisterObserved(DBObject observed){
        if (observed != null) {
            getObservedDBObjects().remove(observed);
            observed.dbObjectObservers.remove(this);
        }
    }
}
