package rdbuilder.dbobject;

import rdbuilder.Utils;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.stream.Stream;

public abstract class DBContainer<E extends DBObject> extends DBObject implements Serializable {
    transient protected HashSet<E> subTree = new HashSet<>();

    public DBContainer(DBContainer parent, String name) {
        super(parent, name);
    }

    public String toStringDetailed(int depth) {
        String indentationString = Utils.getIndentationString(getDepth() - depth, Utils.DEFAULT_TAB_SIZE);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(super.toStringDetailed(0));
        if (!subTree.isEmpty()) {
            stringBuilder.append(String.format(" {%s", System.lineSeparator()));
            int i = 1;
            for (E dbObject : subTree) {
                stringBuilder.append(String.format("%s%s%s",
                        dbObject.toStringDetailed(depth),
                        i < subTree.size() ? "," : "",
                        System.lineSeparator()));
                i++;
            }
            stringBuilder.append(indentationString);
            stringBuilder.append("}");
        }
        return stringBuilder.toString();
    }

    @Override
    public void updateDepth() {
        Integer prevDepth = depth;
        super.updateDepth();
        if (!Objects.equals(prevDepth, depth))
            subTree.forEach(DBObject::updateDepth);
    }

    public boolean addChild(E dbObject) {
        dbObject.setParent(this);
        return subTree.add(dbObject);
    }

    public void clearChildren() {
        subTree.clear();
    }

    public E getChildByName(String childName) {
        for (E child : subTree) {
            if (child.getName().toLowerCase().equals(childName.toLowerCase()))
                return child;
        }
        return null;
    }

    public Stream<E> getSortedSubTreeStream() {
        return subTree.stream().sorted(DBObject.getComparator());
    }

    public HashSet<E> getSubTree() {
        return subTree;
    }

    public boolean removeChild(E dbObject) {
        dbObject.setParent(null);
        return subTree.remove(dbObject);
    }


}
