package rdbuilder.dbobject;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class ForeignKey extends TableConstraint implements DBObjectObserver {
    protected final ArrayList<Column> columns = new ArrayList<>();
    ;
    protected final ArrayList<Column> referencedColumns = new ArrayList<>();
    ;
    protected final Collection<DBObject> observedDBObjects = new HashSet<>();
    private Table referencedTable;

    public ForeignKey(DBObject parent, String name, Table table, Table referencedTable) {
        super(parent, name, table);
        this.referencedTable = referencedTable;
        registerObserved(table);
        registerObserved(referencedTable);
    }

    public ForeignKey() {
    }

    @Override
    public boolean addColumn(Column column) {
        return false;
    }

    @Override
    public boolean removeColumn(Column column) {
        return false;
    }

    @Override
    public Column getColumn(int index) {
        return columns.get(index);
    }

    @Override
    public Collection<Column> getColumns() {
        return columns;
    }

    @Override
    public void onObservedDestroyed(DBObject observed) {
        if (observed instanceof Column) {
            int index = columns.indexOf(observed);
            if (index != -1) {
                removeColumnPair(index);
            } else {
                index = referencedColumns.indexOf(observed);
                if (index != -1) {
                    removeColumnPair(index);
                }
            }
        } else if (observed instanceof Table) {
            destroy();
        }
    }

    @Override
    public Collection<DBObject> getObservedDBObjects() {
        return observedDBObjects;
    }

    @Override
    protected void onDestroy() {
        if (parent != null) {
            ((Schema) parent).unregisterForeignKey(this);
        }

        unregisterAll();
    }

    @Override
    public void setParent(DBObject parent) {
        unregisterObserved(this.parent);
        registerObserved(parent);
        super.setParent(parent);
    }

    @Override
    public void onObservedReplaced(DBObject observed, DBObject replacement) {
        DBObjectObserver.super.onObservedReplaced(observed, replacement);
        if (observed instanceof Column && replacement instanceof Column) {
            int index = columns.indexOf(observed);
            if (index != -1) {
                columns.set(index, (Column) replacement);
            } else {
                index = referencedColumns.indexOf(observed);
                if (index != -1) {
                    referencedColumns.set(index, (Column) replacement);
                }
            }
        }
    }

    @Override
    public void clearColumns() {
        while (!columns.isEmpty()) {
            removeColumnPair(columns.size() - 1);
        }
    }

    public Pair<Column, Column> addColumnPair(String columnName, String referencedColumnName) {
        return addColumnPair(table.getColumnByName(columnName),
                referencedTable.getColumnByName(referencedColumnName));

    }

    public Pair<Column, Column> addColumnPair(Column column, Column referencedColumn) {
        registerObserved(column);
        registerObserved(referencedColumn);
        columns.add(column);
        referencedColumns.add(referencedColumn);
        return new Pair<>(column, referencedColumn);
    }

    public void extractFrom(ForeignKey foreignKey) {
        if (foreignKey.getColumns().isEmpty()) {
            destroy();
        } else {
            setName(foreignKey.getName());
            setTable(foreignKey.getTable());
            setReferencedTable(foreignKey.getReferencedTable());
            for (int i = 0; i < columns.size(); i++) {
                unregisterObserved(columns.get(i));
                unregisterObserved(referencedColumns.get(i));
            }
            columns.clear();
            referencedColumns.clear();
            for (int i = 0; i < foreignKey.columns.size(); i++) {
                addColumnPair(foreignKey.columns.get(i), foreignKey.referencedColumns.get(i));
            }
        }
        foreignKey.destroy();
    }

    public ForeignKey getOuterClone() {
        ForeignKey result = new ForeignKey();
        result.setName(getName());
        result.setTable(table);
        result.setReferencedTable(referencedTable);
        return result;
    }

    public ArrayList<Column> getReferencedColumns() {
        return referencedColumns;
    }

    public Table getReferencedTable() {
        return referencedTable;
    }

    public void setReferencedTable(Table referencedTable) {
        unregisterObserved(this.referencedTable);
        this.referencedTable = referencedTable;
        registerObserved(referencedTable);
    }

    public void removeColumnPair(int index) {
        Column column = columns.remove(index);
        Column referencedColumn = referencedColumns.remove(index);
        unregisterObserved(column);
        unregisterObserved(referencedColumn);
        if (columns.isEmpty()) {
            destroy();
        }
    }

    public void setTable(Table table) {
        unregisterObserved(this.table);
        this.table = table;
        registerObserved(table);
    }
}