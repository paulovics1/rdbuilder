package rdbuilder.dbobject;

public class Column extends DBObject {
    private String dataType;
    private boolean notNull;
    private String defaultValue;

    public Column(DBObject parent, String name, String dataType, boolean notNull, String defaultValue) {
        super(parent, name);
        this.dataType = dataType;
        this.notNull = notNull;
        this.defaultValue = defaultValue == null ? "NULL" : defaultValue;
    }

    public Column() {
        dataType = "";
        defaultValue = "NULL";
    }

    public String toString(){
        return getName();
    }

    @Override
    protected void onDestroy() {
        if (parent != null){
            ((Table) parent).removeColumn(this);
        }
    }

    @Override
    public Table getParent() {
        return (Table) super.getParent();
    }

    @Override
    public void replaceWith(DBObject dbObject) {
        if (parent != null){
            final Column column = (Column) dbObject;
            if (isInPrimaryKey()){
                getParent().getPrimaryKey().removeColumn(this);
                getParent().getPrimaryKey().addColumn(column);
            }
            column.getParent().removeColumn(column);
            getParent().replaceColumn(this, column);
        }
        super.replaceWith(dbObject);
    }

        public void copyFrom(Column column) {
        setName(column.getName());
        dataType = column.dataType;
        notNull = column.notNull;
        defaultValue = column.defaultValue;
    };

    public Column getClone() {
        Column clone = new Column();
        clone.setName(getName());
        clone.dataType = dataType;
        clone.notNull = notNull;
        clone.defaultValue = defaultValue;
        return clone;
    }

    public String getDataType() {
        return dataType.trim();
    }

    public void setDataType(String dataType) {
        this.dataType = dataType == null ? "" : dataType;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        if (defaultValue != null){
            defaultValue = defaultValue.trim();
            if (!defaultValue.isEmpty()){
                this.defaultValue = defaultValue;
                return;
            }
        }
        this.defaultValue = "NULL";
    }

    public boolean isInPrimaryKey() {
        return getParent().getPrimaryKey().getColumns().contains(this);
    }

    public boolean isNotNull() {
        return notNull;
    }

    public void setNotNull(boolean notNull) {
        this.notNull = notNull;
    }

    public String toStringDetailed() {
        return String.format("NAME: %s, TYPE: %s", getName(), getDataType());
    }
}
