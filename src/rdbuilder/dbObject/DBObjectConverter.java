package rdbuilder.dbobject;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import javafx.geometry.Point2D;
import rdbuilder.Utils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class DBObjectConverter implements Converter {
    public static final HashMap<DBObject, Point2D> coordinateMap = new HashMap<>();
    private static final XStream xStream = new XStream();
    private static final DBObjectConverter instance = new DBObjectConverter();
    private static final HashSet<Class> canConvert = new HashSet<>(Arrays.asList(
            new Class[]{
                    Schema.class,
                    Table.class,
                    Column.class,
                    PrimaryKey.class,
                    ForeignKey.class,
                    UniqueConstraint.class
            }
    ));

    private DBObjectConverter(){
        xStream.registerConverter(this);
        xStream.alias("column", Column.class);
        xStream.alias("table", Table.class);
        xStream.alias("primary-key", PrimaryKey.class);
        xStream.alias("foreign-key", ForeignKey.class);
        xStream.alias("unique", UniqueConstraint.class);
        xStream.alias("schema", Schema.class);
        xStream.setMode(XStream.XPATH_ABSOLUTE_REFERENCES);
    }

    @Override
    public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
        DBObject dbObject = (DBObject) value;

        writer.startNode("name");
        writer.setValue(dbObject.getName());
        writer.endNode();

        if (dbObject instanceof Schema) {
            writer.startNode("tables");
            final HashSet<Table> subTree = ((Schema) dbObject).getSubTree();
            context.convertAnother(Utils.getSortedDBObjectsCollection(subTree));
            writer.endNode();

            writer.startNode("foreign-keys");
            final HashSet<ForeignKey> foreignKeys = ((Schema) dbObject).getForeignKeys();
            context.convertAnother(Utils.getSortedDBObjectsCollection(foreignKeys));
            writer.endNode();
        }

        else if (dbObject instanceof ForeignKey){
            writer.startNode("table");
            context.convertAnother(((ForeignKey) dbObject).getTable());
            writer.endNode();

            writer.startNode("referenced-table");
            context.convertAnother(((ForeignKey) dbObject).getReferencedTable());
            writer.endNode();

            writer.startNode("columns");
            context.convertAnother(((ForeignKey) dbObject).getColumns());
            writer.endNode();

            writer.startNode("referenced-columns");
            context.convertAnother(((ForeignKey) dbObject).getReferencedColumns());
            writer.endNode();
        }

        else if (dbObject instanceof TableConstraint) {
            writer.startNode("columns");
            context.convertAnother(((TableConstraint) dbObject).getColumns());
            writer.endNode();
        }

        else if (dbObject instanceof Table){
            writer.startNode("position");
            final Point2D position = coordinateMap.get(dbObject);
            writer.startNode("x");
            writer.setValue(String.valueOf(position.getX()));
            writer.endNode();
            writer.startNode("y");
            writer.setValue(String.valueOf(position.getY()));
            writer.endNode();
            writer.endNode();

            writer.startNode("columns");
            context.convertAnother(((Table) dbObject).getColumns());
            writer.endNode();

            writer.startNode("primary-key");
            context.convertAnother(((Table) dbObject).getPrimaryKey());
            writer.endNode();

            writer.startNode("unique-constraints");
            final HashSet<UniqueConstraint> uniqueConstraints = ((Table) dbObject).getUniqueConstraints();
            context.convertAnother(Utils.getSortedDBObjectsCollection(uniqueConstraints));
            writer.endNode();
        }

        else if (dbObject instanceof Column){
            writer.startNode("data-type");
            writer.setValue(((Column) dbObject).getDataType());
            writer.endNode();

            writer.startNode("not-null");
            context.convertAnother(((Column) dbObject).isNotNull());
            writer.endNode();

            writer.startNode("default-value");
            writer.setValue(((Column) dbObject).getDefaultValue());
            writer.endNode();
        }
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        @SuppressWarnings("unchecked")
        Class<? extends DBObject> currentClass = context.getRequiredType();
        DBObject dbObject = null;
        try {
            dbObject = currentClass.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            Utils.showError(e, "An error has occured", "It was not possible to load the schema model.");
            return null;
        }

        reader.moveDown();
        dbObject.setName(reader.getValue());
        reader.moveUp();

        if (dbObject instanceof Schema) {
            reader.moveDown();
            @SuppressWarnings("unchecked")
            HashSet<Table> tables = (HashSet<Table>) context.convertAnother(null, HashSet.class);
            for (Table table : tables) {
                ((Schema) dbObject).addChild(table);
            }
            reader.moveUp();

            reader.moveDown();
            @SuppressWarnings("unchecked")
            HashSet<ForeignKey> foreignKeys = (HashSet<ForeignKey>) context.convertAnother(null, HashSet.class);
            for (ForeignKey foreignKey : foreignKeys) {
                ((Schema) dbObject).registerForeignKey(foreignKey);
            }
            reader.moveUp();
        }

        if (dbObject instanceof ForeignKey){
            reader.moveDown();
            Table table = (Table) context.convertAnother(null, Table.class);
            ((ForeignKey) dbObject).setTable(table);
            reader.moveUp();

            reader.moveDown();
            Table refTable = (Table) context.convertAnother(null, Table.class);
            ((ForeignKey) dbObject).setReferencedTable(refTable);
            reader.moveUp();

            reader.moveDown();
            @SuppressWarnings("unchecked")
            ArrayList<Column> columns = (ArrayList<Column>) context.convertAnother(null, ArrayList.class);
            reader.moveUp();

            reader.moveDown();
            @SuppressWarnings("unchecked")
            ArrayList<Column> refColumns = (ArrayList<Column>) context.convertAnother(null, ArrayList.class);
            reader.moveUp();

            for (int i = 0; i < columns.size(); i++) {
                ((ForeignKey) dbObject).addColumnPair(columns.get(i), refColumns.get(i));
            }
        }

        else if (dbObject instanceof TableConstraint) {
            reader.moveDown();
            @SuppressWarnings("unchecked")
            ArrayList<Column> columns = (ArrayList) context.convertAnother(null, ArrayList.class);
            reader.moveUp();
            for (Column column : columns) {
                ((TableConstraint) dbObject).addColumn(column);
            }
        }

        else if (dbObject instanceof Table){
            reader.moveDown();
            double x, y;
            reader.moveDown();
            x = Double.parseDouble(reader.getValue());
            reader.moveUp();
            reader.moveDown();
            y = Double.parseDouble(reader.getValue());
            reader.moveUp();
            coordinateMap.put(dbObject, new Point2D(x, y));
            reader.moveUp();

            reader.moveDown();
            @SuppressWarnings("unchecked")
            ArrayList<Column> columns = (ArrayList) context.convertAnother(null, ArrayList.class);
            reader.moveUp();
            for (Column column : columns) {
                ((Table) dbObject).addColumn(column);
            }

            reader.moveDown();
            PrimaryKey primaryKey = (PrimaryKey) context.convertAnother(null, PrimaryKey.class);
            ((Table) dbObject).getPrimaryKey().copyFrom(primaryKey);
            reader.moveUp();

            reader.moveDown();
            @SuppressWarnings("unchecked")
            HashSet<UniqueConstraint> uniqueConstraints = (HashSet<UniqueConstraint>) context.convertAnother(null, HashSet.class);
            reader.moveUp();
            for (UniqueConstraint uniqueConstraint : uniqueConstraints) {
                ((Table) dbObject).registerUniqueConstraint(uniqueConstraint);
            }
        }

        else if (dbObject instanceof Column){
            reader.moveDown();
            ((Column) dbObject).setDataType(reader.getValue());
            reader.moveUp();

            reader.moveDown();
            boolean notNull = (boolean) context.convertAnother(null, Boolean.class);
            ((Column) dbObject).setNotNull(notNull);
            reader.moveUp();

            reader.moveDown();
            ((Column) dbObject).setDefaultValue(reader.getValue());
            reader.moveUp();
        }
        return dbObject;

    }

    @Override
    public boolean canConvert(Class aClass) {
        return canConvert.contains(aClass);
    }

    public static Object fromXML(String string) {
        coordinateMap.clear();
        return xStream.fromXML(string);
    }

    public static String toXML(Object object) {
        return xStream.toXML(object);
    }
}
