package rdbuilder.dbobject;

import java.util.HashSet;

public class Schema extends DBContainer<Table> {
    protected final HashSet<ForeignKey> foreignKeys = new HashSet<>();

    public Schema(DBContainer parent, String name) {
        super(parent, name);
    }

    public Schema() {
        super(null, null);
    }

    @Override
    protected void onDestroy() {
        if (parent != null) {
            //noinspection unchecked
            ((DBContainer<Schema>) parent).removeChild(this);
        }
    }

    public void clearForeignKeys() {
        while (!foreignKeys.isEmpty())
            foreignKeys.iterator().next().destroy();
    }

    public ForeignKey getForeignKeyByName(String name) {
        name = name.toLowerCase();
        for (ForeignKey foreignKey: foreignKeys){
            if (foreignKey.getName().toLowerCase().equals(name.toLowerCase()))
                return foreignKey;
        }
        return null;
    }

    public HashSet<ForeignKey> getForeignKeys() {
        return foreignKeys;
    }

    public boolean registerForeignKey(ForeignKey foreignKey){
        foreignKey.setParent(this);
        return foreignKeys.add(foreignKey);
    }

    public boolean unregisterForeignKey(ForeignKey foreignKey){
        foreignKey.setParent(null);
        return foreignKeys.remove(foreignKey);
    }
}
