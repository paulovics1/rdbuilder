package rdbuilder.dbobject;

import java.util.Collection;

public abstract class TableConstraint extends DBObject {
    protected Table table;

    public TableConstraint(DBObject parent, String name, Table table) {
        super(parent, name);
        this.table = table;
    }

    protected TableConstraint() {}

    public void addAllColumns(Collection<Column> toAdd) {
        for (Column column : toAdd){
            addColumn(column);
        }
    }

    public abstract boolean addColumn(Column column);

    public abstract void clearColumns();

    public abstract Column getColumn(int index);

    public abstract Collection<Column> getColumns();

    public Table getTable() {
        return table;
    }

    public abstract boolean removeColumn(Column column);
}
