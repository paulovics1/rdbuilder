package rdbuilder.dbobject;

import rdbuilder.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

public class Table extends DBObject {

    private final ArrayList<Column> columns = new ArrayList<>();
    private final HashSet<UniqueConstraint> uniqueConstraints = new HashSet<>();
    private final PrimaryKey primaryKey = new PrimaryKey();

    public Table(DBContainer parent, String name) {
        super(parent, name);
        primaryKey.setParent(this);
    }

    public Table() {
        super();
    }

    @Override
    public Schema getParent() {
        return (Schema) super.getParent();
    }

    public String toStringDetailed(int depth) {
        final String indentationString = Utils.getIndentationString(getDepth(), Utils.DEFAULT_TAB_SIZE);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(indentationString);
        stringBuilder.append(super.toStringDetailed(getDepth()));

        if (!columns.isEmpty()) {
            stringBuilder.append(String.format(" {%s", System.lineSeparator()));
            String columnIndentationString = Utils.getIndentationString(getDepth() + 1 - depth, Utils.DEFAULT_TAB_SIZE);
            int i = 1;
            for (Column column : columns) {
                stringBuilder.append(String.format("%s%s%s%s",
                        columnIndentationString,
                        column.toStringDetailed(),
                        i < columns.size() ? "," : "",
                        System.lineSeparator()));
                i++;
            }
            stringBuilder.append(indentationString);
            stringBuilder.append("}");
        }
        return stringBuilder.toString();
    }

    @Override
    protected void onDestroy() {
        if (parent != null) {
            //noinspection unchecked
            ((DBContainer<Table>) parent).removeChild(this);
        }
        @SuppressWarnings("unchecked")
        ArrayList<Column> columnsClone = (ArrayList<Column>) columns.clone();
        for (Column column : columnsClone){
            column.destroy();
        }
    }

    private void addAllColumns(Collection<Column> columns) {
        final Column[] columnsArray =  columns.toArray(new Column[]{});
        for (Column column : columnsArray)
            addColumn(column);
    }

    public Column addColumn(Column column) {
        if (column.parent != null && column.parent != this){
            column.getParent().removeColumn(column);
        }
        column.setParent(this);
        columns.add(column);
        return column;
    }

    public Column addColumn() {
        return addColumn(new Column(this, "", "", false, "null"));
    }

    public void clear() {
        columns.clear();
    }

    public void extractFrom(Table table, Map<Column, Column> columnMap, Map<UniqueConstraint, UniqueConstraint> uniqueConstraintMap){
        setName(table.getName());
        @SuppressWarnings("unchecked")
        ArrayList<Column> columnsClone = (ArrayList<Column>) columns.clone();
        for (Column column : columnsClone){
            if (!columnMap.containsValue(column)){
                column.destroy();
            }
        }
        columns.clear();

        for (Map.Entry<Column, Column> columnsEntry : columnMap.entrySet()){
            columnsEntry.getValue().copyFrom(columnsEntry.getKey());
            columnsEntry.getKey().replaceWith(columnsEntry.getValue());
        }
        addAllColumns(table.columns);
        primaryKey.copyFrom(table.primaryKey);

        for (UniqueConstraint uniqueConstraint : uniqueConstraints){
            if (!uniqueConstraintMap.containsValue(uniqueConstraint)){
                uniqueConstraint.destroy();
            }
        }
        unregisterAllUniqueConstraints();
        registerAllUniqueConstraints(table.uniqueConstraints);


        for (Map.Entry<UniqueConstraint, UniqueConstraint> uniqueConstraintsEntry : uniqueConstraintMap.entrySet()){
            uniqueConstraintsEntry.getValue().copyFrom(uniqueConstraintsEntry.getKey());
            uniqueConstraintsEntry.getKey().replaceWith(uniqueConstraintsEntry.getValue());
        }
        table.destroy();
    }

    public Table getClone(){
        Table resultTable = new Table();
        resultTable.setName(getName());
        resultTable.primaryKey.setName(primaryKey.getName());

        for (Column column : columns){
            Column columnClone = resultTable.addColumn(column.getClone());
            if (primaryKey.columns.contains(column)){
                resultTable.primaryKey.addColumn(columnClone);
            }
        }

        for (UniqueConstraint uniqueConstraint : uniqueConstraints){
            UniqueConstraint uniqueConstraintClone = new UniqueConstraint();
            uniqueConstraintClone.setName(uniqueConstraint.getName());
            for (Column column : uniqueConstraint.columns){
                Column columnClone = resultTable.getColumns().get(getColumns().indexOf(column));
                uniqueConstraintClone.addColumn(columnClone);
            }

            resultTable.registerUniqueConstraint(uniqueConstraintClone);
        }
        return resultTable;
    }

    public Column getColumnByName(String column_name) {
        Column result = null;
        for (Column column : columns){
            if (column.getName().equals(column_name)){
                result = column;
                break;
            }
        }
        return result;
    }

    public ArrayList<Column> getColumns() {
        return columns;
    }

    public TableConstraint getConstraintByName(String unique_constraint_name) {
        unique_constraint_name = unique_constraint_name.toLowerCase();
        if (primaryKey.getName().toLowerCase().equals(unique_constraint_name))
            return primaryKey;

        for (UniqueConstraint uniqueConstraint : uniqueConstraints){
            if (uniqueConstraint.getName().toLowerCase().equals(unique_constraint_name))
                return uniqueConstraint;
        }
        return null;
    }

    public PrimaryKey getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(PrimaryKey primaryKey) {
        this.primaryKey.copyFrom(primaryKey);
    }

    public HashSet<UniqueConstraint> getUniqueConstraints() {
        return uniqueConstraints;
    }

    private void registerAllUniqueConstraints(HashSet<UniqueConstraint> uniqueConstraints) {
        for (UniqueConstraint uniqueConstraint : uniqueConstraints)
            registerUniqueConstraint(uniqueConstraint);
    }

    public boolean registerUniqueConstraint(UniqueConstraint uniqueConstraint) {
        uniqueConstraint.setParent(this);
        return uniqueConstraints.add(uniqueConstraint);
    }

    public void removeColumn(Column column) {
        columns.remove(column);
        column.setParent(null);
    }

    public Column replaceColumn(int index, Column column) {
        Column originalColumn = columns.get(index);
        originalColumn.setParent(null);
        columns.set(index, column);
        column.setParent(this);
        return originalColumn;
    }

    public Column replaceColumn(Column originalColumn, Column column) {
        int index = columns.indexOf(originalColumn);
        originalColumn.setParent(null);
        columns.set(index, column);
        column.setParent(this);
        return originalColumn;
    }

    private void unregisterAllUniqueConstraints() {
        @SuppressWarnings("unchecked")
        HashSet<UniqueConstraint> uniqueConstraintsClone = (HashSet<UniqueConstraint>) uniqueConstraints.clone();
        uniqueConstraintsClone.forEach(this::unregisterUniqueConstraint);
    }

    public boolean unregisterUniqueConstraint(UniqueConstraint uniqueConstraint) {
        uniqueConstraint.setParent(null);
        return uniqueConstraints.remove(uniqueConstraint);
    }
}
