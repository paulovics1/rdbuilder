package rdbuilder.dbobject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class PrimaryKey extends TableConstraint implements DBObjectObserver {
    protected final ArrayList<Column> columns = new ArrayList<>();
    private final Collection<DBObject> observedDBObjects = new HashSet<>();

    public PrimaryKey(Table parent, String name) {
        super(parent, name, parent);
    }

    public PrimaryKey(){}

    public PrimaryKey(String name, String table_name, Schema schema) {
        super(schema.getChildByName(table_name), name, schema.getChildByName(table_name));
    }

    @Override
    public void clearColumns() {
        columns.clear();
    }

    @Override
    public Column getColumn(int index) {
        return columns.get(index);
    }

    @Override
    public Collection<Column> getColumns() {
        return columns;
    }

    @Override
    public boolean addColumn(Column column) {
        if (!columns.contains(column)){
            columns.add(column);
            column.setNotNull(true);
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {}

    @Override
    public void onObservedReplaced(DBObject observed, DBObject replacement) {
        DBObjectObserver.super.onObservedReplaced(observed, replacement);
        if (observed instanceof Column && replacement instanceof  Column) {
            columns.remove(observed);
            addColumn((Column) replacement);
        }
    }

    @Override
    public void onObservedDestroyed(DBObject observed) {
        if (observed instanceof Column){
            removeColumn((Column) observed);
        }
    }

    @Override
    public boolean removeColumn(Column observed) {
        return columns.remove(observed);
    }

    @Override
    public Collection<DBObject> getObservedDBObjects() {
        return observedDBObjects;
    }

    @Override
    public void replaceWith(DBObject dbObject) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void replaceWithObserver(DBObjectObserver replacement) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setParent(DBObject parent) {
        this.parent = parent;
        table = (Table) parent;
        registerObserved(parent);
    }

    public void addColumn(String column_name) {
        addColumn(table.getColumnByName(column_name));
    }

    public void copyFrom(PrimaryKey primaryKey) {
        setName(primaryKey.getName());
        clearColumns();
        addAllColumns(primaryKey.getColumns());
    }
}
