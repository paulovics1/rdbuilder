package rdbuilder.dbobject;

public class Database<E extends DBObject> extends DBContainer<E> {
    public Database(DBContainer parent, String name) {
        super(parent, name);
    }

    @Override
    protected void onDestroy() {
        if (parent != null) {
            //noinspection unchecked
            ((DBContainer<Database>) parent).removeChild(this);
        }
    }

}
