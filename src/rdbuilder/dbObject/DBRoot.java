package rdbuilder.dbobject;

import rdbuilder.dbconnector.DBConnector;

import java.sql.SQLException;

public class DBRoot extends DBContainer<Database> {
    private DBConnector dbConnector;

    public DBRoot(String name, DBConnector dbConnector) {
        super(null, name);
        this.dbConnector = dbConnector;
    }

    @Override
    protected void loadDBObjectContent(DBObject dbObject, boolean forceReload) throws SQLException {
        dbConnector.loadDBObjectContent(dbObject, forceReload);
    }

    @Override
    public int getDepth() {
        return 0;
    }

    @Override
    public void updateDepth() {
        subTree.forEach(DBObject::updateDepth);
    }

    @Override
    public String getName() {
        String name = super.getName();
        return name != null ? name : dbConnector.getDefaultDBRootName();
    }

    @Override
    public DBRoot getRoot() {
        return this;
    }

    @Override
    public DBConnector getDBConnector() {
        return dbConnector;
    }

    @Override
    protected void onDestroy() { }
}
