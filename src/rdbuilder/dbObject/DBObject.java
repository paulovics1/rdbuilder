package rdbuilder.dbobject;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import rdbuilder.Utils;
import rdbuilder.dbconnector.DBConnector;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashSet;

public abstract class DBObject {
    transient private StringProperty name;
    private static final Comparator<DBObject> NAME_COMPARATOR = (o1, o2) -> o1.getName().compareTo(o2.getName());
    protected HashSet<DBObjectObserver> dbObjectObservers = new HashSet<>();
    protected Integer depth;
    protected DBObject parent;

    public DBObject(DBObject parent, String name) {
        nameProperty().set(name);
        this.parent = parent;
    }

    protected DBObject() {}

    public String toString() {
        return String.format("[%s] %s",
                getClass().getSimpleName().toUpperCase(),
                getName());
    }

    public static Comparator<DBObject> getComparator() {
        return NAME_COMPARATOR;
    }

    public final void destroy(){
        @SuppressWarnings("unchecked")
        HashSet<DBObjectObserver> observersClone = (HashSet<DBObjectObserver>) dbObjectObservers.clone();
        for (DBObjectObserver dbObjectObserver : observersClone){
            dbObjectObserver.onObservedDestroyed(this);
            dbObjectObserver.unregisterObserved(this);
        }
        onDestroy();
    }

    public DBConnector getDBConnector() {
        return getRoot().getDBConnector();
    }

    public int getDepth() {
        if (depth == null) {
            updateDepth();
        }
        return depth;
    }

    public String getName() {
        final String name = nameProperty().get();
        return name == null ? name : name.trim();
    }

    public void setName(String name) {
        nameProperty().setValue(name.trim());
    }

    public DBObject getParent() {
        return parent;
    }

    public void setParent(DBObject parent) {
        this.parent = parent;
    }

    public String getPath(){
        if (getParent() == null){
            return toString();
        }
        return parent.getPath() + " > " + toString();
    }

    public DBRoot getRoot(){
        if (parent != null)
            return parent.getRoot();
        return null;
    }

    public void loadContent(boolean forceReload) throws SQLException {
        loadDBObjectContent(this, forceReload);
    }

    protected void loadDBObjectContent(DBObject dbObject, boolean forceReload) throws SQLException {
        if (parent != null)
            parent.loadDBObjectContent(dbObject, forceReload);
    }

    public StringProperty nameProperty(){
        if (name == null)
            name = new SimpleStringProperty("");
        return name;
    }

    protected abstract void onDestroy();

    public void replaceWith(DBObject dbObject) {
        assert dbObject.getClass() == getClass();

        @SuppressWarnings("unchecked")
        HashSet<DBObjectObserver> observersClone = (HashSet<DBObjectObserver>) dbObjectObservers.clone();
        for (DBObjectObserver dbObjectObserver : observersClone){
            dbObjectObserver.onObservedReplaced(this, dbObject);
        }
    }

    public String toStringDetailed(int depth) {
        String indentationString = Utils.getIndentationString(getDepth() - depth, Utils.DEFAULT_TAB_SIZE);
        return indentationString + toString();
    }

    public void updateDepth() {
        if (parent == null) {
            depth = 0;
        } else {
            depth = parent.getDepth() + 1;
        }
    }
}
