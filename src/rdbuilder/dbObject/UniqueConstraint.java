package rdbuilder.dbobject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class UniqueConstraint extends TableConstraint implements DBObjectObserver {
    protected final ArrayList<Column> columns = new ArrayList<>();
    private final Collection<DBObject> observedDBObjects = new HashSet<>();

    public UniqueConstraint(Table parent, String name) {
        super(parent, name, parent);
    }

    public UniqueConstraint(){
        super();
    }

    @Override
    public void clearColumns() {
        columns.clear();
    }

    @Override
    public Column getColumn(int index) {
        return columns.get(index);
    }

    @Override
    public ArrayList<Column> getColumns() {
        return columns;
    }

    public boolean addColumn(Column column) {
        if (!columns.contains(column)){
            columns.add(column);
            registerObserved(column);
            return true;
        }
        return false;
    }

    public boolean removeColumn(Column column) {
        unregisterObserved(column);
        boolean result = columns.remove(column);
        if (columns.isEmpty()){
            destroy();
        }
        return result;
    }

    @Override
    protected void onDestroy() {
        if (parent != null) {
            ((Table) parent).unregisterUniqueConstraint(this);
        }

        unregisterAll();
    }

    @Override
    public void onObservedDestroyed(DBObject observed) {
        if (observed instanceof Column){
            removeColumn((Column) observed);
        }
    }

    @Override
    public void setParent(DBObject parent) {
        unregisterObserved(this.parent);
        registerObserved(parent);
        super.setParent(parent);
    }

    @Override
    public Collection<DBObject> getObservedDBObjects() {
        return observedDBObjects;
    }

    @Override
    public void onObservedReplaced(DBObject observed, DBObject replacement) {
        DBObjectObserver.super.onObservedReplaced(observed, replacement);
        if (observed instanceof Column && replacement instanceof  Column) {
            columns.remove(observed);
            addColumn((Column) replacement);
        }
    }

    @Override
    public void replaceWith(DBObject dbObject) {
        if (parent != null){
            Table parentTable = (Table) parent;
            parentTable.unregisterUniqueConstraint(this);
            parentTable.registerUniqueConstraint((UniqueConstraint) dbObject);
        }
        super.replaceWith(dbObject);
    }

    public boolean addColumn(String column_name) {
        return addColumn(table.getColumnByName(column_name));
    }

    public void copyFrom(UniqueConstraint key) {
        setName(key.getName());
    }

    public void extractFrom(UniqueConstraint toCopyFrom) {
        setName(toCopyFrom.getName());
        @SuppressWarnings("unchecked")
        HashSet<Column> columnsClone = (HashSet<Column>) columns.clone();
        for (Column column : columnsClone){
            removeColumn(column);
        }
        addAllColumns(toCopyFrom.getColumns());
    }
}
