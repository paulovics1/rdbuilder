package rdbuilder.dbconnector;

import rdbuilder.dbconnector.external.MySQLDBConnector;

public class MySQLDBConnectorTest extends GenericDBConnectorTest {
	static final String LOCAL_USERNAME = "root";
	static final String LOCAL_PASSWORD = "bcpraca";

	public MySQLDBConnectorTest() {
		super(LOCAL_USERNAME, LOCAL_PASSWORD);
	}

	@Override
	public void setUp() throws Exception {
		defaultLocalGenericDBConnector = new MySQLDBConnector("localhost",
				MySQLDBConnector.DEFAULT_PORT,
				LOCAL_USERNAME,
				LOCAL_PASSWORD, (String) args[4]);
		defaultLocalGenericDBConnector.setDatabase(MySQLDBConnector.DEFAULT_DATABASES[0]);
		super.setUp();
	}
}