package rdbuilder.dbconnector;

import junit.framework.TestCase;
import org.junit.After;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public abstract class DBConnectorTest extends TestCase {
	protected DBConnector defaultLocalDBConnector;

	public void printColumnNames(ResultSet resultSet) throws SQLException {
		ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
		int columnCount = resultSetMetaData.getColumnCount();
		for (int i = 1; i <= columnCount; i++) {
			System.out.printf("%s %s %s %s%s",
					resultSetMetaData.getColumnTypeName(i),
					resultSetMetaData.getColumnType(i),
					resultSetMetaData.getColumnName(i),
					resultSetMetaData.getColumnLabel(i),
					System.lineSeparator());
		}
	}

	public void printResultSet(ResultSet resultSet) throws SQLException {
		while (resultSet.next()) {
			for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
				System.out.print(resultSet.getObject(i) + " ");
			}
			System.out.println();
		}
	}

	@After
	public void tearDown() throws Exception {
		defaultLocalDBConnector.disconnect();
	}

	public void testDefaultLocalConnection() throws Exception {
		try {
			System.out.printf(">>> Connection string: %s\n", defaultLocalDBConnector.getConnectionString());
			defaultLocalDBConnector.connect();
		} catch (SQLException e) {
			fail(e.toString());
		}
		Connection connection = defaultLocalDBConnector.getConnection();
		System.out.printf(">>> Connection database system: %s %s\n", connection.getMetaData().getDatabaseProductName(), connection.getMetaData().getDatabaseProductVersion());
		System.out.println(">>> Preferred terms:");
		System.out.printf("catalog = %s\n", connection.getMetaData().getCatalogTerm());
		System.out.printf("schema = %s\n", connection.getMetaData().getSchemaTerm());
		System.out.printf("procecdure = %s\n", connection.getMetaData().getProcedureTerm());
		System.out.printf(">>> Client info: %s\n", connection.getClientInfo().toString());
	}
}
