package rdbuilder.dbconnector;

import rdbuilder.dbconnector.external.PostgreSQLDBConnector;

public class PostgreSQLDBConnectorTest extends GenericDBConnectorTest {
	static final String LOCAL_USERNAME = "postgres";
	static final String LOCAL_PASSWORD = "bcpraca";

	public PostgreSQLDBConnectorTest() {
		super(LOCAL_USERNAME, LOCAL_PASSWORD);
	}

	@Override
	public void setUp() throws Exception {
		defaultLocalGenericDBConnector = new PostgreSQLDBConnector("localhost",
				PostgreSQLDBConnector.DEFAULT_PORT,
				LOCAL_USERNAME,
				LOCAL_PASSWORD);
		defaultLocalGenericDBConnector.setDatabase(PostgreSQLDBConnector.DEFAULT_DATABASES[0]);
		super.setUp();
	}



}