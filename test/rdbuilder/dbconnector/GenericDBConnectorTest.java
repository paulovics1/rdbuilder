package rdbuilder.dbconnector;

import rdbuilder.dbobject.DBRoot;
import rdbuilder.dbobject.Database;
import rdbuilder.dbobject.Schema;
import rdbuilder.dbobject.Table;

public abstract class GenericDBConnectorTest extends DBConnectorTest{
	final String LOCAL_USERNAME;
	final String LOCAL_PASSWORD;
	protected GenericDBConnector defaultLocalGenericDBConnector;

	public GenericDBConnectorTest(String local_username, String local_password) {
		LOCAL_USERNAME = local_username;
		LOCAL_PASSWORD = local_password;
	}

	@Override
	public void testDefaultLocalConnection() throws Exception {
		super.testDefaultLocalConnection();
		System.out.printf(">>> Connection database: %s\n", defaultLocalGenericDBConnector.getDatabaseName());
		System.out.printf(">>> Connection schema: %s\n", defaultLocalGenericDBConnector.getSchemaName());
		System.out.println(">>> Database objects tree with first branch expanded:");
		DBRoot<Database<Schema<Table>>> dbRoot = defaultLocalDBConnector.getDBObjectsRoot();
		try {
			dbRoot.loadContent();
			Database<Schema<Table>> database = dbRoot.getSubTree().firstEntry().getValue();
			database.loadContent();
			Schema<Table> schema = database.getSubTree().firstEntry().getValue();
			schema.loadContent();
			Table table = schema.getSubTree().firstEntry().getValue();
			table.loadContent();
		} catch (NullPointerException e) {}
		System.out.println(defaultLocalDBConnector.getDBObjectsRoot().toString());
	}

	@Override
	public void setUp() throws Exception {
		defaultLocalDBConnector = defaultLocalGenericDBConnector;
		super.setUp();
	}
}