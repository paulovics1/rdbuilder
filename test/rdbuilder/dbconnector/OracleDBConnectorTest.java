package rdbuilder.dbconnector;

import rdbuilder.dbconnector.external.OracleDBConnector;

public class OracleDBConnectorTest extends GenericDBConnectorTest {
	static final String LOCAL_USERNAME = "system";
	static final String LOCAL_PASSWORD = "bcpraca";

	public OracleDBConnectorTest() {
		super(LOCAL_USERNAME, LOCAL_PASSWORD);
	}

	@Override
	public void setUp() throws Exception {
		defaultLocalGenericDBConnector = new OracleDBConnector("localhost",
				OracleDBConnector.DEFAULT_PORT,
				LOCAL_USERNAME,
				LOCAL_PASSWORD);
		defaultLocalGenericDBConnector.setDatabase(OracleDBConnector.DEFAULT_DATABASES[0]);
		super.setUp();
	}
}