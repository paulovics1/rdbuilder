package rdbuilder.dbconnector;

import rdbuilder.dbconnector.external.MicrosoftSQLServerDBConnector;

public class MicrosoftSQLServerDBConnectorTest extends GenericDBConnectorTest {
	static final String LOCAL_USERNAME = "sa";
	static final String LOCAL_PASSWORD = "bcpraca";

	public MicrosoftSQLServerDBConnectorTest() {
		super(LOCAL_USERNAME, LOCAL_PASSWORD);
	}

	@Override
	public void setUp() throws Exception {
		defaultLocalGenericDBConnector = new MicrosoftSQLServerDBConnector("localhost",
				MicrosoftSQLServerDBConnector.DEFAULT_PORT,
				LOCAL_USERNAME,
				LOCAL_PASSWORD);
		defaultLocalGenericDBConnector.setDatabase(MicrosoftSQLServerDBConnector.DEFAULT_DATABASES[0]);
		super.setUp();
	}
}